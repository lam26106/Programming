#include "postfixUtility.h"
int priority(char c)
{
	switch (c)
	{
	case '(':
		return 0;
		break;
	case '+': case '-':
		return 1;
		break;
	case '*': case '/':
		return 2;
		break;
	}
}

vector<string> Split(string str)
{

	vector<string> result;
	string t = "()/+-*) ";
	string temp = "";
	for (int i = 0; i < str.length(); ++i)
	{
		if (str[i] != ' '){
			if (t.find(str[i]) != string::npos)
			{
				temp += str[i];
				result.push_back(temp);
				temp = "";
			}
			else
			{
				temp = temp + str[i];
				if (t.find(str[i + 1]) != string::npos){
					result.push_back(temp);
					temp = "";
				}
			}
		}
		else
			temp = "";
	}
	if (temp != "")
		result.push_back(temp);
	return result;
}
string getPostfix(string str)
{
	string res = "";
	genericLinkedListStack<string> stack;
	vector<string> a = Split(str);
	string t = "()/+-*)";
	string top;
	for (int i = 0; i < a.size(); ++i)
	{
		if (!stack.empty())
			top = stack.top();
		else
			top = "";
		if (a[i] == "(")
			stack.push(a[i]);
		else if (a[i] == ")")
		{
			string temp = stack.top();
			while (temp != "(")
			{
				res = res + " " + temp;
				stack.pop();
				if (stack.empty())
					break;
				temp = stack.top();
			}
			if (stack.top() == "(")
				stack.pop();

		}
		else if (top != "")
		{
			if (t.find(a[i]) != string::npos){
				if (priority(a[i][0]) > priority(top[0]))
					stack.push(a[i]);
				else
				{
					res = res + " " + top;
					stack.pop();
					stack.push(a[i]);
				}
			}
			else
				res = res + " " + a[i];
		}
		else
			stack.push(a[i]);

	}
	while (!stack.empty() && stack.top() != "(")
	{
		res = res + " " + stack.top();
		stack.pop();
	}
	res.erase(res.begin());
	return res;

}

float evaluatePostfix(string pexp)
{
	string t = "+-/*";
	vector<string> a = Split(pexp);
	genericLinkedListStack<float> stack;
	for (int i = 0; i < a.size(); ++i)
	{
		if (t.find(a[i]) != string::npos)
		{
			float num1 = stack.top();
			stack.pop();
			float num2 = stack.top();
			stack.pop();
			if (a[i] == "+")
				num1 = num1 + num2;
			else if (a[i] == "-")
				num1 = num2 - num1;
			else if (a[i] == "*")
				num1 = num1 * num2;
			else
				num1 = num2 / num1;
			stack.push(num1);

		}
		else
			stack.push(atoi(a[i].c_str()));
	}
	float res = stack.top();
	stack.~genericLinkedListStack();
	return res;

}
