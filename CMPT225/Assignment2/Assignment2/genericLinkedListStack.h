#pragma once
#include<iostream>
#include<string>
#include<vector>
using namespace std;

template<typename T>
class Node
{
private:
	Node<T>* link;
	T data;
public:
	Node()
	{
		link = NULL;
	}
	Node(const Node<T>& link, const T& DATA)
	{
		link = link;
		data = DATA;
	}
	Node<T>* getLink() const
	{
		return link;
	}
	const T& getData() const
	{
		return data;
	}
	void setLink(Node<T>* _link)
	{
		link = _link;
	}
	void setData(const T& _data)
	{
		data = _data;
	}
};
template<typename T>
class LinkedList
{
private:
	Node<T> *head, *tail;
	int SIZE;
public:
	LinkedList()
	{
		head = tail = NULL;
		SIZE = 0;
	}
	LinkedList(const LinkedList<T>& t)
	{
		this->head = this->tail = NULL;
		SIZE = 0;
		if (t.head != NULL)
		{
			Node<T>* temp = t.head;
			while (temp != NULL)
			{
				this->addBack(temp->getData());
				temp = temp->getLink();
			}
		}
	}
	int size()
	{
		return this->SIZE;
	}
	bool empty()
	{
		return head == NULL;
	}
	const T& front() const
	{
		return this->head->getData();
	}
	void addBack(const T& s)
	{

		Node<T>*temp = new Node<T>;
		temp->setData(s);
		temp->setLink(NULL);
		if (head == NULL)
			head = tail = temp;
		else{
			tail->setLink(temp);
			tail = temp;
		}
		SIZE++;
	}
	void addFront(const T& s)
	{
		Node<T> *temp = new Node<T>;
		temp->setData(s);
		temp->setLink(head);
		head = temp;
		if (head->getLink() == NULL)
			tail = head;
		SIZE++;
	}
	void removeFront()
	{
		if (head != NULL)
		{
			Node<T>* temp = head;
			head = head->getLink();
			delete temp;
			SIZE--;
		}
	}
	void print()
	{
		for (Node<T>* temp = head; temp != NULL; temp = temp->getLink())
		{
			cout << temp->getData() << endl;
		}
	}
	~LinkedList()
	{
		while (!empty()){ removeFront(); }
	}

};



template<class T>
class genericLinkedListStack
{
private:
	LinkedList<T> stack;
public:
	genericLinkedListStack() 
	{
	}
	genericLinkedListStack(const genericLinkedListStack<T>& t)
	{
		stack = new LinkedList<T>(t);
	}
	void size()
	{
		return stack.size();
	}
	bool empty()
	{
		return stack.empty();
	}
	const T& top()
	{
		return stack.front();
	}
	void push(const T& t)
	{
		stack.addFront(t);
	}
	void pop()
	{
		stack.removeFront();
	}
	void print()
	{
		stack.print();
	}
	~genericLinkedListStack()
	{
		stack.~LinkedList();
	}
};

