#pragma once
#include "binaryTree.h"
template <typename E>
class binarySearchTree : public binaryTree<E>
{
protected:
	vector<int> emptyNode;
	bool checkEmptyNode(int i)
	{
		for (int j = 0; j < emptyNode.size(); ++j)
			if (i == emptyNode[j])
				return true;
		return false;
	}
	void pre_order(int i = 1)
	{
		if (i < this->_tree.size() && !checkEmptyNode(i))
		{
			cout << this->_tree[i] << " ";
			pre_order(i * 2);
			pre_order(i * 2 + 1);
		}
	}
	void post_order(int i = 1)
	{
		if (i < this->_tree.size() && !checkEmptyNode(i))
		{
			post_order(i * 2);
			post_order(i * 2 + 1);
			cout << this->_tree[i] << " ";
		}
	}
	void in_order(int i = 1 )
	{
		if (i < this->_tree.size() && !checkEmptyNode(i))
		{
			in_order(i * 2);
			cout << this->_tree[i] << " ";
			in_order(i * 2 + 1);
		}
	}  
	int leftMost(int i)
	{
		if (i < this->_tree.size() && !checkEmptyNode(i)){
			while (i * 2 < this->_tree.size() && !checkEmptyNode(i * 2))
			{
				i = i * 2;
			}
			return i;
		}
		return -1;
	}
	void shift(vector<int> parent, vector<int> children, vector<E>& tmp)
	{
		//for (int i = 0; i < parent.size(); ++i) cout << parent[i] << " ";
		//cout << endl;
		//for (int i = 0; i < children.size(); ++i) cout << children[i] << " ";
		//cout << endl;
		//cout << this->_tree.size() << endl;
		bool check = true;
		if (parent.size() > 0){
			if (parent[0] < this->_tree.size())
			{
				for (int i = 0; i < parent.size(); ++i)
					emptyNode.erase(remove(emptyNode.begin(), emptyNode.end(), parent[i]), emptyNode.end());
				vector<int> a;
				vector<int> b;
				for (int i = 0; i < children.size(); ++i)
				{
					if (children[i] * 2 >= this->_tree.size())
					{
						//if (children[i] == 58)
						emptyNode.erase(remove(emptyNode.begin(), emptyNode.end(), children[i]), emptyNode.end());
						emptyNode.push_back(children[i]);
						check = false;
					}
					else{
						if (children[i] * 2 < this->_tree.size() && !checkEmptyNode(children[i] * 2)){
							b.push_back(children[i] * 2);
							a.push_back(parent[i] * 2);
							tmp[parent[i] * 2] = this->_tree[children[i] * 2];
							
							emptyNode.erase(remove(emptyNode.begin(), emptyNode.end(), parent[i] * 2), emptyNode.end());
							
						}
						else{
							//if (parent[i] * 2 == 48) cout << children[i] << endl;
							emptyNode.erase(remove(emptyNode.begin(), emptyNode.end(), parent[i] * 2), emptyNode.end());
							emptyNode.push_back(parent[i] * 2);
						}
						if (children[i] * 2 + 1 < this->_tree.size() && !checkEmptyNode(children[i] * 2 + 1)){
							b.push_back(children[i] * 2 + 1);
							a.push_back(parent[i] * 2 + 1);
							tmp[parent[i] * 2 + 1] = this->_tree[children[i] * 2 + 1];
							
							emptyNode.erase(remove(emptyNode.begin(), emptyNode.end(), parent[i] * 2 + 1), emptyNode.end());
						}
						else{
							emptyNode.erase(remove(emptyNode.begin(), emptyNode.end(), parent[i] * 2 + 1), emptyNode.end());
							emptyNode.push_back(parent[i] * 2 + 1);
						}
						emptyNode.push_back(children[i]);
					}

					
				}
				//cout << endl << endl;
				shift(a, b, tmp);
			}
		}
	}
	void removeNodeHasOneChild(int idx)
	{
		int i, j;
		vector<int> parent;
		vector<int> children; 
		int _tmp;
		if (!checkEmptyNode(idx * 2 + 1) && idx * 2 + 1 < this->_tree.size())
		{
			this->_tree[idx] = this->_tree[idx * 2 + 1];
			_tmp = idx * 2 + 1;
			//cout << idx * 2 + 1 << endl;
			//i = idx * 2 + 1;
		}
		else if (!checkEmptyNode(idx * 2) && idx*2 < this->_tree.size())
		{
			this->_tree[idx] = this->_tree[idx * 2];
			_tmp = idx * 2;
			//i = idx * 2;
		}
		emptyNode.erase(remove(emptyNode.begin(), emptyNode.end(), idx), emptyNode.end());
		i = idx * 2;
		idx *= 2;
		if ((!checkEmptyNode(_tmp * 2) && _tmp * 2 < this->_tree.size()) || (!checkEmptyNode(_tmp * 2 + 1) && _tmp * 2 + 1 < this->_tree.size())){
			if (!checkEmptyNode(_tmp * 2) && _tmp * 2 < this->_tree.size())
			{
				this->_tree[idx] = this->_tree[_tmp * 2];
				emptyNode.erase(remove(emptyNode.begin(), emptyNode.end(), idx), emptyNode.end());
				parent.push_back(idx);
				children.push_back(_tmp * 2);
			}
			else
				emptyNode.push_back(idx);
			if (!checkEmptyNode(_tmp * 2 + 1) && _tmp * 2 + 1 < this->_tree.size())
			{
				this->_tree[idx + 1] = this->_tree[_tmp * 2 + 1];
				emptyNode.erase(remove(emptyNode.begin(), emptyNode.end(), idx + 1), emptyNode.end());
				parent.push_back(idx + 1);
				children.push_back(_tmp * 2 + 1);
			}
			else
				emptyNode.push_back(idx + 1);
		}
		else{
			//cout << "abubu" << " " <<  _tmp << endl; 
			emptyNode.push_back(_tmp);
			return;
		}
		//}
		//for (int i = 0; i < parent.size(); ++i) cout << parent[i] << " ";
		//cout << endl;
		//for (int i = 0; i < children.size(); ++i) cout << children[i] << " ";
		//cout << endl;
		vector<E> tmp = this->_tree;
		shift(parent, children, tmp);
		this->_tree = tmp;
	}
	bool removeOne(int idx)
	{
		if (idx == -1)
			return false;
		if (idx * 2 >= this->_tree.size())
			emptyNode.push_back(idx);
		else if (checkEmptyNode(idx * 2) && checkEmptyNode(idx * 2 + 1))
			emptyNode.push_back(idx);
		else if (!checkEmptyNode(idx * 2) && !checkEmptyNode(idx * 2 + 1)  && idx*2 < this->_tree.size() && idx * 2 + 1 < this->_tree.size())
		{
			int idx1 = leftMost(idx * 2 + 1);
			//cout << idx1 << endl;
			//cout << idx << " " << idx1 << endl;
				//cout << idx1 << endl;
				E temp = this->_tree[idx1];
				this->_tree[idx1] = this->_tree[idx];
				this->_tree[idx] = temp;
				//removeOne(idx1);
				if (!checkEmptyNode(idx1 * 2 + 1) && idx1 * 2 + 1 < this->_tree.size()){
					//cout << "Lan 1 " << " " << idx1 << " ";
					removeNodeHasOneChild(idx1);
				}
				else{
					//cout << idx1 << " ahiihihih ";
					emptyNode.push_back(idx1);
				}

			//return true;
		}
		else {
			//removeNodeHasOneChild(idx);
			//cout << idx << endl;
			removeNodeHasOneChild(idx);
		}
		sort(emptyNode.begin(), emptyNode.end());
		for (int i = this->_tree.size() - 1; i >= 1; --i)
		{
			if (!checkEmptyNode(i)){
				//cout << idx << " " << this->_tree.size() - 1 << " " << i << endl;
				//cout << "aihihih   " << this->_tree.size() << endl;
				//cout << "ahihiih   " << i << endl;
				this->_tree.resize(i + 1);
				break;
			}
			emptyNode.erase(remove(emptyNode.begin(), emptyNode.end(), i), emptyNode.end());
		}
		return true;
	}
public:
	binarySearchTree() : binaryTree(){
	}
	int find(E itm, int i = 1)
	{
		if (i >= this->_tree.size() || checkEmptyNode(i))
			return -1;
		if (itm == this->_tree[i])
			return i;
		else if (itm < this->_tree[i])
			return find(itm, i * 2);
		return find(itm, i * 2 + 1);
	}
	void addNode(E itm)
	{
		int i = 1;
		while (i < this->_tree.size())
		{
			if (checkEmptyNode(i))
			{
				emptyNode.erase(remove(emptyNode.begin(), emptyNode.end(), i), emptyNode.end());
				this->_tree[i] = itm;
				return;
			}
			else if (itm < this->_tree[i])
				i = i * 2;
			else
				i = i * 2 + 1;
		}
		for (int j = this->_tree.size() == 0 ? 1 : this->_tree.size(); j < i; ++j)
			emptyNode.push_back(j);
		this->_tree.resize(i + 1);
		this->_tree[i] = itm;
		
	}
	void remove_all(E itm)
	{
		
		int idx = find(itm);
		while (idx != -1)
		{
			removeOne(idx);

			idx = find(itm);
		}
	}
	bool remove_one(E itm)
	{
		
		int idx = find(itm);
		if (idx == -1)
			return false;
		removeOne(idx);

		return true;

	}
	void preOrderPrint()
	{
		pre_order();
	}

	void postOrderPrint()
	{
		post_order();
	}

	void inOrderPrint()
	{
		in_order();
	}
	void printEmptyNode()
	{
		for (int i = 0; i < emptyNode.size(); ++i) cout << emptyNode[i] << " ";
		cout << endl;
	}
	~binarySearchTree(){}
};

