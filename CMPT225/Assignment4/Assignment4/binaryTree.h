#pragma once
#include "binaryTreeInterface.h"


template<class E>
class binaryTree : public binaryTreeInterface<E>
{
protected:
	vector<E> _tree;
	void pre_order(int i = 1)
	{
		if (i < _tree.size())
		{
			cout << _tree[i] << " ";
			pre_order(i * 2);
			pre_order(i * 2 + 1);
		}
	}
	void post_order(int i = 1)
	{
		if (i < _tree.size())
		{
			post_order(i * 2);
			post_order(i * 2 + 1);
			cout << _tree[i] << " ";
		}
	}
	void in_order(int i = 1)
	{
		if (i < _tree.size())
		{
			in_order(i * 2);
			cout << _tree[i] << " ";
			in_order(i * 2 + 1);
		}
	}
public:
	binaryTree() : _tree(1){}
	void addNode(E itm)
	{
		this->_tree.push_back(itm);
	}

	void remove_all(E itm)
	{
		for (int i = 0; i < _tree.size(); ++i)
		{
			if (_tree[i] == itm){
				_tree.erase(_tree.begin() + i);
				i--;
			}
		}
	}

	// tries removing one instance of itm
	// if the tree doesn't contain itm returns false
	bool remove_one(E itm)
	{
		for (int i = 0; i < _tree.size(); ++i)
		{
			if (_tree[i] == itm)
			{
				_tree.erase(_tree.begin() + i);
				return true;
			}

		}
		return false;
	}

	bool isEmpty()
	{
		return _tree.size() <= 1;
	}

	int size()
	{
		return _tree.size() - 1;
	}

	void preOrderPrint()
	{
		pre_order();
	}

	void postOrderPrint()
	{
		post_order();
	}

	//void inOrderPrint()
	//{
	//	in_order();
	//}
	void inOrderPrint(int i = 1)
	{
		if (i < _tree.size())
		{
			inOrderPrint(i * 2);
			cout << _tree[i] << " ";
			inOrderPrint(i * 2 + 1);
		}
	}
	int height()
	{
		return log2(_tree.size() - 1);
	}
	void normalprint()
	{
		for (int i = 1; i < _tree.size(); ++i)
			cout << _tree[i] << " ";
		cout << endl;
	}
	~binaryTree()
	{
		_tree.clear();
	}
};

