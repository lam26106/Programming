/** @file ListInterface.h
Defines ADT List.
*/
#include <iostream>
using namespace std;
template <typename E>
class ListInterface{
public:
	/** Adds a new entry to this list.
	@post If successful, newEntry is stored in the list and
	the count of items in the bag has increased by 1.
	@param newEntry The object to be added as a new entry.
	@return True if addition was successful, or false if not. */
	virtual bool add(const E& newEntry) = 0;

	/** Removes the first instance of elem from this list.
	@post If successful, the count of items in the list has decreased by 1.
	@param elem The object to be removed.
	@return True if removal was successful, or false if not. */
	virtual bool removeFirst(const E& elem) = 0;

	/** Determines if the list is empty.
	@return True if the list contains no element, or false if not. */
	virtual bool empty() const = 0;

	/** @return The number of elements in the list.*/
	virtual int count() const = 0;

	/** Destructs the object.
	@post all allocated memory is released.*/
	virtual ~ListInterface() {};

	/** prints the list to standard output.*/
	virtual void print() const = 0;
};

//using namespace std;
template <typename E>
class ArrayList : public ListInterface<E>{
public:

	//I added default constructor
	ArrayList()
	{
		listArray_ = NULL;
		n_ = 0;
		capacity_ = 0;
	}
	ArrayList(int capacity){
		listArray_ = new E[capacity];
		n_ = 0;
		capacity_ = capacity;
	}

	bool empty() const{ return n_ == 0; };

	int count() const{ return n_; }

	void print() const{
		for (int i = 0; i<n_; i++)
			std::cout << listArray_[i] << " ";
		std::cout << std::endl;
	}

	bool removeFirst(const E& elem){
		int index = findFirst(elem);
		if (index == -1){
			std::cout << "removing " << elem << " failed!\n";
			return false;
		}
		for (int i = index; i<n_ - 1; i++)
			listArray_[i] = listArray_[i + 1];
		n_--;
		return true;
	}

	bool add(const E& new_entry){
		if (n_ + 1 > capacity_){ // array is full
			std::cout << "adding " << new_entry << " failed\n";
			return false;
		}
		listArray_[n_] = new_entry;
		n_++;
		return true;
	}

	~ArrayList() {
		delete[] listArray_;
	}

protected:
	/** Finds the first time elem apears in the list.
	@param elem The object to be found.
	@return -1 if the list does not contain elem and index of elem in listArray_ otherwise. */
	int findFirst(const E& elem) const{
		for (int i = 0; i<n_; i++)
			if (listArray_[i] == elem)
				return i;
		return -1;
	}

	E* listArray_;
	int n_, capacity_;
};


template <typename E>
class SortedArrayList : public ArrayList<E>{
public:

	//I added default constructor
	SortedArrayList() : ArrayList() {}
	SortedArrayList(int capacity) : ArrayList<E>(capacity) {
	}
	SortedArrayList(const SortedArrayList<E>& s)
	{
		if (this->n_ > 0)
			delete[] this->listArray_;

		this->listArray_ = new E[s.capacity_];
		this->capacity_ = s.capacity_;
		for (int i = 0; i < s.n_; ++i)
		{
			this->listArray_[i] = s.listArray_[i];
		}
		this->n_ = s.n_;
	}
	//I added resize method
	void resize(const int& num)
	{
		if (this->n_ > 0)
			delete[] this->listArray_;
		this->capacity_ = num;
		this->n_ = 0;
		this->listArray_ = new E[num];
	}
	bool add(const E& elem){
		if (this->n_ + 1> this->capacity_){
			std::cout << "adding " << elem << " failed!" << std::endl;
			return false;
		}

		int right_place = getRightPlace(elem);
		// shift everything right
		for (int i = this->n_ - 1; i >= right_place; i--)
			this->listArray_[i + 1] = this->listArray_[i];

		this->listArray_[right_place] = elem;
		this->n_++;

		return true;
	}

	bool fastSearch(const E& elem){
		return binarySearch(elem, 0, this->n_ - 1);
	}


	~SortedArrayList(){
	}

protected:
	int getRightPlace(const E& elem){
		for (int i = 0; i<this->n_; i++)
			if (this->listArray_[i] >= elem)
				return i;
		return this->n_;
	}

	bool binarySearch(const E& elem, int start_index, int end_index){
		if (start_index > end_index)
			return false;
		int middle_index = (start_index + end_index) / 2;
		if (this->listArray_[middle_index]>elem)
			return binarySearch(elem, start_index, middle_index - 1);
		else if (this->listArray_[middle_index]<elem)
			return binarySearch(elem, middle_index + 1, end_index);
		else // this->listArray_[middle_index]==elem
			return true;
	}
};

class test
{
public:
	SortedArrayList<int> a;
public:
	test()
	{
		a.resize(10);
	}
	SortedArrayList<int> getA()
	{
		return this->a;
	}
	~test()
	{
	}
};
int main()
{	
	test Test;
	int b = 10;
	Test.a.add(b);
	Test.getA();
	system("pause");
	return 0;
}