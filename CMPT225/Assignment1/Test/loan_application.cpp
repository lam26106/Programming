#include"loan_application.h"
#include<string>

loan_application::loan_application()
{
	applicant_full_name = " ";
	year_of_relevant_experience = 0;
	year_of_relevant_education = 0;
	loan_amount = 0;
	estimated_yearly_profit = NULL;
}
loan_application::loan_application(string name, int year_exp, int edu_exp, long long int loan_amount, long long int* estimated_yearly_profit)
{
	applicant_full_name = name;
	year_of_relevant_experience = year_exp;
	year_of_relevant_education = edu_exp;
	this->loan_amount = loan_amount;
	this->estimated_yearly_profit = estimated_yearly_profit;
}
loan_application::~loan_application()
{
	delete[]estimated_yearly_profit;
}
string loan_application::getname()
{
	return applicant_full_name;
}
int loan_application::getyear_exp()
{
	return year_of_relevant_experience;
}
int loan_application::getedu_exp()
{
	return  year_of_relevant_education;
}
long long int loan_application::getloan_amount()
{
	return loan_amount;
}
void loan_application::setname(string name)
{
	applicant_full_name = name;

}
void loan_application::setyear_exp(int exp)
{
	year_of_relevant_experience = exp;

}
void loan_application::setedu_exp(int exp)
{
	year_of_relevant_education = exp;
}
void loan_application::setloan_amount(long long int amount)
{
	loan_amount = amount;
}
double loan_application::priority_applicant()
{
	double priority = 0.0;
	for (int i = 0; i < 30; i++)
		priority += 1 / (i + 1)*estimated_yearly_profit[i];
	return priority;
}
bool loan_application:: operator>=(loan_application rhs)
{
	if (this->priority_applicant() >= rhs.priority_applicant())
		return true;
	return false;

}
bool loan_application:: operator<=(loan_application rhs)
{
	if (this->priority_applicant() <= rhs.priority_applicant())
		return true;
	return false;

}
bool loan_application:: operator>(loan_application rhs)
{
	if (this->priority_applicant() > rhs.priority_applicant())
		return true;
	return false;
}

bool loan_application:: operator<(loan_application rhs)
{
	if (this->priority_applicant() < rhs.priority_applicant())
		return true;
	return false;
}

bool loan_application::  operator==(loan_application rhs)
{
	if (this->priority_applicant() == rhs.priority_applicant())
		return true;
	return false;
}

ostream& operator<<(ostream & output, loan_application rhs)
{
	output << rhs.applicant_full_name << endl;
	output << rhs.year_of_relevant_experience << endl;
	output << rhs.year_of_relevant_education << endl;
	output << rhs.loan_amount << ends;

	return output;
}
/*bool denied_application()
{

if(year_of_relevant_experience +year_of_relevant_education < 10)
return true;
return false;
}
*/
