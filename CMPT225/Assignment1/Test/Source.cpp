#include<iostream>
#include<vector>
#include<fstream>
#include"loan_application.h"
#include <boost/lexical_cast.hpp>
#include<string>
using namespace std;
template <typename E>
class ListInterface{
public:
	/** Adds a new entry to this list.
	@post If successful, newEntry is stored in the list and
	the count of items in the bag has increased by 1.
	@param newEntry The object to be added as a new entry.
	@return True if addition was successful, or false if not. */
	virtual bool add(const E& newEntry) = 0;

	/** Removes the first instance of elem from this list.
	@post If successful, the count of items in the list has decreased by 1.
	@param elem The object to be removed.
	@return True if removal was successful, or false if not. */
	virtual bool removeFirst(const E& elem) = 0;

	/** Determines if the list is empty.
	@return True if the list contains no element, or false if not. */
	virtual bool empty() const = 0;

	/** @return The number of elements in the list.*/
	virtual int count() const = 0;

	/** Destructs the object.
	@post all allocated memory is released.*/
	virtual ~ListInterface() {};

	/** prints the list to standard output.*/
	virtual void print() const = 0;
};
template <typename E>
class ArrayList : public ListInterface<E>{
public:
	ArrayList(int capacity){
		listArray_ = new E[capacity];
		n_ = 0;
		capacity_ = capacity;
	}

	bool empty() const{ return n_ == 0; };

	int count() const{ return n_; }

	void print() const{
		for (int i = 0; i<n_; i++)
			std::cout << listArray_[i] << " ";
		std::cout << std::endl;
	}

	bool removeFirst(const E& elem){
		int index = findFirst(elem);
		if (index == -1){
			std::cout << "removing " << elem << " failed!\n";
			return false;
		}
		for (int i = index; i<n_ - 1; i++)
			listArray_[i] = listArray_[i + 1];
		n_--;
		return true;
	}

	bool add(const E& new_entry){
		if (n_ + 1 > capacity_){ // array is full
			std::cout << "adding " << new_entry << " failed\n";
			return false;
		}
		listArray_[n_] = new_entry;
		n_++;
		return true;
	}

	~ArrayList() {
		delete[] listArray_;
	}

protected:
	/** Finds the first time elem apears in the list.
	@param elem The object to be found.
	@return -1 if the list does not contain elem and index of elem in listArray_ otherwise. */
	int findFirst(const E& elem) const{
		for (int i = 0; i<n_; i++)
			if (listArray_[i] == elem)
				return i;
		return -1;
	}

	E* listArray_;
	int n_, capacity_;
};
template <typename E>
class SortedArrayList : public ArrayList<E>{
public:
	SortedArrayList(int capacity) : ArrayList<E>(capacity) {
	}

	bool add(const E& elem){
		if (this->n_ + 1> this->capacity_){
			std::cout << "adding " << elem << " failed!" << std::endl;
			return false;
		}

		int right_place = getRightPlace(elem);
		// shift everything right
		for (int i = this->n_ - 1; i >= right_place; i--)
			this->listArray_[i + 1] = this->listArray_[i];

		this->listArray_[right_place] = elem;
		this->n_++;

		return true;
	}

	bool fastSearch(const E& elem){
		return binarySearch(elem, 0, this->n_ - 1);
	}

	~SortedArrayList(){
	}

protected:
	int getRightPlace(const E& elem){
		for (int i = 0; i<this->n_; i++)
			if (this->listArray_[i] >= elem)
				return i;
		return this->n_;
	}

	bool binarySearch(const E& elem, int start_index, int end_index){
		if (start_index > end_index)
			return false;
		int middle_index = (start_index + end_index) / 2;
		if (this->listArray_[middle_index]>elem)
			return binarySearch(elem, start_index, middle_index - 1);
		else if (this->listArray_[middle_index]<elem)
			return binarySearch(elem, middle_index + 1, end_index);
		else // this->listArray_[middle_index]==elem
			return true;
	}
};

void Tokenize(string line, vector<string> & tokens, string delimiters = "\t "){
	string token = "";
	string OneCharString = " ";
	for (int i = 0; i<line.size(); i++)
		if (find(delimiters.begin(), delimiters.end(), line[i]) != delimiters.end()) // line[i] is one of the delimiter characters
		{
			if (token.size()>0)
				tokens.push_back(token);
			token = "";
		}
		else
		{
			OneCharString[0] = line[i];
			token += OneCharString;
		}

	if (token != "")
		tokens.push_back(token);
}
bool denied_application()
{
	if (this->years_of_relevant_education + this->years_of_relevant_experience < 10)
		return true;
	return false;


}
int main(int argc, char ** argv)
{
	ifstream read;
	read.open("commands.txt");

	SortedArrayList<loan_application> active_application(5000);
	SortedArrayList<loan_application> approved_application(5000);
	SortedArrayList<loan_application> denied_application(5000);
	int budget;
	int x = 0;
	vector<string> tokens;
	string date;
	string * data = new string[3];
	while (x == 0) // to read all from the txt file
	{
		int size = tokens.size();
		string line;
		getline(read, line);
		Tokenize(line, tokens, "\t");
		if (tokens[0] == "save_application")

		{
			string applicant_full_name = tokens[1];
			int years_of_relevant_education = lexical_cast<int>tokens[2];
			int  years_of_relevant_experience = boost::lexical_cast<int>tokens[3];
			int loan_amount = boost::lexical_cast<int>tokens[4];
			size -= 5;
			int * estimated_yearly_profits = new int[size];
			active_applicant.add(loan_application(applicant_full_name, years_of_relevant_experience, years_of_relevant_education, loan_amount));
			++active_applicant.count();
			for (int i = 0; i < size; i++)
			{
				estimated_yearly_profits[i] = boost::lexical_cast<int>(tokens[i + 5]);
			}

		}
		else if (tokens[0] == "make_decision")
		{


			date = tokens[1];
			budget = boost::lexical_cast<int>tokens[2];
			for (int i = 0; i < 5000; i++)
			{
				if (active_application[i].denied_application() == true)
				{
					denied_application.add(active_application[i]);
					++denied_application.count();
					active_application.removefirst(active_application[i]);
					--active_application.count();

				}
				else if (budget > active_application[i].getloan_amount())
				{
					approved_application.add(active_application[i]);
					++approved_application.count();
					active_application.removeFirst(active_application[i]);
					--active_application.count();
					budget -= active_application[i].getloan_amount();


				}
			}
		else if (tokens[0] == "print")
		{
			for (int i = 0; i < active_application.count(); i++)
			{
				cout << "active_application" << "\t" << "(" << active_application.getname() << "<" << active_application.getloan_amount() << ")";

			}
			for (int i = 0; i < approved_application.count(); i++)
			{
				cout << "approved_application" << "\t" << "(" << active_application.getname() << "<" << active_application.getloan_amount() << "," << date << ")";


			}
			for (int i = 0; i < denied_application.count(); i++)
			{
				cout << "denied_application" << "\t" << "(" << denied_application.getname() << "<" << denied_application.getloan_amount() << "," << date << ")";


			}










		}
		else
			bresk;







		}
		system("pause");
		return 0;





	}










}

