#include<iostream>
#include<string>
using namespace std;
class loan_application
{

private:
	string applicant_full_name;
	int year_of_relevant_experience;
	int year_of_relevant_education;
	long long int loan_amount;
	long long int* estimated_yearly_profit;

public:
	loan_application();
	loan_application(string name, int year_exp, int edu_exp, long long int loan_amount, long long int* estimated_yearly_profit);
	double priority_applicant();
	~loan_application();
	string getname();
	int getyear_exp();
	int getedu_exp();
	long long int getloan_amount();
	void setname(string name);
	void setyear_exp(int exp);
	void setedu_exp(int exp);
	void setloan_amount(long long int amount);
	bool operator>=(loan_application rhs);
	bool operator<=(loan_application rhs);
	bool operator>(loan_application rhs);
	bool operator<(loan_application rhs);
	bool operator==(loan_application rhs);
	friend ostream& operator<<(ostream & output, loan_application rhs);
};
