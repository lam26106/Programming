#include"loan_application.h"

#include<fstream>
#pragma warning(disable:4996)

long long int myAtoi(const string& t)
{
	long long int res = 0;
	for (int i = 0; i < t.length(); ++i)
		res = res * 10 + t[i] - 48;
	//cout << t << "\t" << res << endl;
	return res;
}

/** @file ListInterface.h
Defines ADT List.
*/

template <typename E>
class ListInterface{
public:
	/** Adds a new entry to this list.
	@post If successful, newEntry is stored in the list and
	the count of items in the bag has increased by 1.
	@param newEntry The object to be added as a new entry.
	@return True if addition was successful, or false if not. */
	virtual bool add(const E& newEntry) = 0;

	/** Removes the first instance of elem from this list.
	@post If successful, the count of items in the list has decreased by 1.
	@param elem The object to be removed.
	@return True if removal was successful, or false if not. */
	virtual bool removeFirst(const E& elem) = 0;

	/** Determines if the list is empty.
	@return True if the list contains no element, or false if not. */
	virtual bool empty() const = 0;

	/** @return The number of elements in the list.*/
	virtual int count() const = 0;

	/** Destructs the object.
	@post all allocated memory is released.*/
	virtual ~ListInterface() {};

	/** prints the list to standard output.*/
	virtual void print() const = 0;
};

template <typename E>
class ArrayList : public ListInterface<E>{
public:
	ArrayList()
	{
		this->listArray_ = NULL;
		this->n_ = 0;
		this->capacity_ = 0;
	}
	ArrayList(int capacity){
		listArray_ = new E[capacity];
		n_ = 0;
		capacity_ = capacity;
	}

	bool empty() const{ return n_ == 0; };

	int count() const{ return n_; }

	void print() const{
		for (int i = 0; i<n_; i++)
			std::cout << listArray_[i] << " ";
		std::cout << std::endl;
	}

	bool removeFirst(const E& elem){
		int index = findFirst(elem);
		if (index == -1){
			std::cout << "removing " << elem << " failed!\n";
			return false;
		}
		for (int i = index; i<n_ - 1; i++)
			listArray_[i] = listArray_[i + 1];
		n_--;
		return true;
	}


	bool add(const E& new_entry){
		if (n_ + 1 > capacity_){ // array is full
			std::cout << "adding " << new_entry << " failed\n";
			return false;
		}
		listArray_[n_] = new_entry;
		n_++;
		return true;
	}

	~ArrayList() {
		if (this->n_ > 0)
			delete[] listArray_;
	}

protected:
	/** Finds the first time elem apears in the list.
	@param elem The object to be found.
	@return -1 if the list does not contain elem and index of elem in listArray_ otherwise. */
	int findFirst(const E& elem) const{
		for (int i = 0; i<n_; i++)
			if (listArray_[i] == elem)
				return i;
		return -1;
	}

	E* listArray_;
	int n_, capacity_;
};

template <typename E>
class SortedArrayList : public ArrayList<E>{
public:
	SortedArrayList<E>() : ArrayList<E>()
	{}
	SortedArrayList<E>(int capacity) : ArrayList<E>(capacity) {
	}
	bool removeAt(const int& index)
	{
		if (this->n_ > 0)
		{
			if (index < 0 || index >= this->n_)
				return false;
			else
			{
				for (int i = index; i < this->n_ - 1; ++i)
					this->listArray_[i] = this->listArray_[i + 1];
				this->n_--;
				return true;
			}
		}
		else
			return false;
	}
	void resize(const int& num)
	{
		if (this->n_ > 0)
			delete[] this->listArray_;
		this->capacity_ = num;
		this->n_ = 0;
		this->listArray_ = new E[num];
	}
	int getCapacity() const
	{
		return this->capacity_;
	}
	bool add_increasing_order(const E& elem){
		if (this->n_ + 1> this->capacity_){
			std::cout << "adding " << elem << " failed!" << std::endl;
			return false;
		}

		int right_place = getRightPlace(elem);
		// shift everything right
		RightShiftElements(elem, right_place);
		return true;

	}

	bool add_decreasing_order(const E& elem)
	{
		if (this->n_ + 1 > this->capacity_){
			std::cout << "adding " << elem << " failed!" << std::endl;
			return false;
		}
		int left_place = getLeftPlace(elem);
		//RightShiftElements(elem, left_place); 
		for (int i = this->n_ - 1; i >= left_place; i--)
			this->listArray_[i + 1] = this->listArray_[i];
		this->listArray_[left_place] = elem;
		this->n_++;
		return true;

	}
	bool fastSearch(const E& elem){
		return binarySearch(elem, 0, this->n_ - 1);
	}
	SortedArrayList<E>& operator = (const SortedArrayList<E>& s)
	{
		if (this->n_ > 0)
			delete[] this->listArray_;
		this->listArray_ = new E[s.capacity_];
		this->capacity_ = s.capacity_;
		for (int i = 0; i < s.n_; ++i)
		{
			this->listArray_[i] = s.listArray_[i];
		}
		this->n_ = s.n_;
		return *this;
	}
	E& operator [] (const int& index)
	{
		return this->listArray_[index];
	}
	~SortedArrayList<E>(){
	}

protected:
	int getRightPlace(const E& elem){
		for (int i = 0; i<this->n_; i++)
			if (this->listArray_[i] >= elem)
				return i;
		return this->n_;
	}
	int getLeftPlace(const E& elem)
	{
		for (int i = 0; i < this->n_; ++i)
			if (this->listArray_[i] <= elem)
				return i;
		return this->n_;
	}
	bool binarySearch(const E& elem, int start_index, int end_index){
		if (start_index > end_index)
			return false;
		int middle_index = (start_index + end_index) / 2;
		if (this->listArray_[middle_index]>elem)
			return binarySearch(elem, start_index, middle_index - 1);
		else if (this->listArray_[middle_index]<elem)
			return binarySearch(elem, middle_index + 1, end_index);
		else // this->listArray_[middle_index]==elem
			return true;
	}
	bool RightShiftElements(const E& elem, const int& endpoint)
	{
		for (int i = this->n_ - 1; i >= endpoint; i--)
			this->listArray_[i + 1] = this->listArray_[i];
		this->listArray_[endpoint] = elem;
		this->n_++;
		return true;
	}
};
SortedArrayList<loan_application> in_process_applications(100);
SortedArrayList<loan_application> active_applications(100);
SortedArrayList<loan_application> approved_applications(100);
SortedArrayList<loan_application> denied_applications(100);
void setDcisionDate(const string& date)
{
	for (int i = 0; i < in_process_applications.count(); ++i)
		in_process_applications[i].setDecisionDate(date);
}

void  ProcessApprovedList(int budget)
{
	for (int i = 0; i < active_applications.count(); ++i)
	{
		loan_application tmp = active_applications[i];
		if (tmp.getLoan() <= budget)
		{
			in_process_applications.removeAt(i);
			approved_applications.add_decreasing_order(tmp);
			active_applications.removeAt(i);
			budget -= tmp.getLoan();
			i--;
		}
	}
}
void ProcessDeniedList(int budget)
{
	for (int i = 0; i < active_applications.count(); ++i)
	{
		loan_application tmp = active_applications[i];
		if (tmp.getYearsOfEducation() + tmp.getYearsOfExperience() < 10)
		{
			
			in_process_applications.removeAt(i);
			active_applications.removeAt(i);
			denied_applications.add_decreasing_order(tmp);
			i--;
		}
	}
}
void make_decision(int budget)
{
	ProcessDeniedList(budget);
	ProcessApprovedList(budget);

}
void print()
{
	cout << "active_applications\t";
	for (int i = 0; i < active_applications.count(); ++i)
		cout << "(" << active_applications[i] << ")" << "\t";
	cout << endl << "approved_applications\t";
	for (int i = 0; i < approved_applications.count(); ++i)
		cout << "(" << approved_applications[i] << ",\"" << approved_applications[i].getDecisionDate() << "\")\t";
	cout << endl << "denied_applications\t";
	for (int i = 0; i < denied_applications.count(); ++i)
		cout << "(" << denied_applications[i] << ",\"" << denied_applications[i].getDecisionDate() << "\")\t";
	cout << endl;
}
void standarize(string &s)
{

	for (int i = 0; i < s.length(); ++i)
	{
		if (s[i] == ' ' && (s[i + 1] == ' ' || s[i + 1] == '\t')){
			s.erase(s.begin() + i);
			i--;
		}
		else if (s[i] == '\t' && s[i+1] != '\t' && s[i+1] != ' ')
			s[i] = ' ';
	}
	if (s[0] == ' ')
		s.erase(s.begin());
}
vector<string> Split(string s)
{

	vector<string> vecStr;
	string tmp = "";
	int i = 0;
	while (i < s.length())
	{
		if (s[i] == '\"' && s[i + 1] != ' ')
		{
			
			tmp = s.substr(i + 1, s.find('\"', i + 1) - i - 1);
			i = s.find('\"', i + 1) + 1;
		}
		else if ((s[i] != ' ' && s[i + 1] == '\0') || s[i] == ' ')
		{
			if (s[i] != ' ')
				tmp += s[i];
			vecStr.push_back(tmp);
			tmp = "";
			++i;
		}
		else 
		{
			tmp = tmp + s[i++];
		}
	}
	return vecStr;

}	

const int MAXAPPLICATION = 5000;
int main(int argv, char** argc)
{


	vector<long long int> profits;
	ifstream read;
	read.open("commands.txt");
	int i = 0;
	while (!read.eof())
	{
		string tmp;
		vector<string> info;
		getline(read, tmp);
		standarize(tmp);
		info = Split(tmp);
		int size = info.size();
		if (size > 0){
			if (info[0] == "save_application")
			{
				loan_application temp;
				temp.setFullName(info[1]);
				temp.setYearsOfEducation(myAtoi(info[2]));
				temp.setYearsOfExperience(myAtoi(info[3]));
				temp.setLoan(myAtoi(info[4]));
				int i = 5;
				while (i < info.size() - 1)
				{
					profits.push_back(myAtoi(info[i++]));
				}
				temp.setYearlyProfits(profits);
				//cout << temp.getFullName() << endl;
				in_process_applications.add_decreasing_order(temp);

				cout << endl;
			}
			else if (info[0] == "make_decisions")
			{
				profits.clear();
				setDcisionDate(info[1]);
				active_applications = in_process_applications;

				int budget = myAtoi(info[2]);
				make_decision(budget);

			}
			else if (info[0] == "print")
			{
				print();
			}
		}

	}
	//cout << i;
		system("pause"); 
	return 0;
}