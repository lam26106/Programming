#pragma once
#include<iostream>
#include<vector>
#include<string>
using namespace std;
class loan_application
{
private:
	string applicant_full_name;
	int	years_of_relevant_education;
	int	years_of_relevant_experience;
	long long int	loan_amount;
	vector<long long int> estimated_yearly_profits;
	string decision_date;
public:
	loan_application();
	loan_application(string, int, int, long long int, vector<long long int>);
	string getFullName() const;
	int getYearsOfEducation() const;
	int getYearsOfExperience() const;
	long long int getLoan() const;
	vector<long long int> getYearlyProfits() const;
	long long int getPriority() const;
	string getDecisionDate() const;

	void setFullName(string);
	void setYearsOfEducation(int);
	void setYearsOfExperience(int);
	void setLoan(long long int);
	void setYearlyProfits(vector<long long int>);
	void setDecisionDate(const string& s);

	bool operator < (const loan_application&) const;
	bool operator == (const loan_application&) const;
	bool operator <= (const loan_application&) const;
	bool operator > (const loan_application&) const;
	bool operator >= (const loan_application&) const;

	loan_application& operator = (const loan_application&);

	friend ostream& operator << (ostream&, const loan_application&);

	~loan_application();
};


