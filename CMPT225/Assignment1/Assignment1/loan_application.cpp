#include "loan_application.h"


loan_application::loan_application()
{
	this->applicant_full_name = "";
	this->years_of_relevant_education = 0;
	this->years_of_relevant_experience = 0;
	this->loan_amount = 0;
}

loan_application::loan_application(string name, int yearsOfedu, int yearsOfexp, long long int loan, vector<long long int> profits)
{
	this->applicant_full_name = name;
	this->years_of_relevant_education = yearsOfedu;
	this->years_of_relevant_experience = yearsOfexp;
	this->loan_amount = loan;
	for (int i = 0; i < profits.size(); ++i)
	{
		int p = profits[i];
		this->estimated_yearly_profits.push_back(p);
	}

}
string loan_application::getFullName() const
{
	return this->applicant_full_name;
}
int loan_application::getYearsOfEducation() const
{
	return this->years_of_relevant_education;
}
int loan_application::getYearsOfExperience() const
{
	return this->years_of_relevant_experience;
}
long long int loan_application::getLoan() const
{
	return this->loan_amount;
}
vector<long long int> loan_application::getYearlyProfits() const
{
	return this->estimated_yearly_profits;
}
long long int loan_application::getPriority() const
{
	long long int priority = 0;
	for (int i = 0; i < this->estimated_yearly_profits.size(); ++i)
	{
		priority = priority + 1.0 / (i + 1) * this->estimated_yearly_profits[i];
	}
	return priority;
}
string loan_application::getDecisionDate() const
{
	return this->decision_date;
}


void loan_application::setFullName(string name)
{
	this->applicant_full_name = name;
}
void loan_application::setYearsOfEducation(int yearsOfedu)
{
	this->years_of_relevant_education = yearsOfedu;
}
void loan_application::setYearsOfExperience(int yearsOfexp)
{
	this->years_of_relevant_experience = yearsOfexp;
}
void loan_application::setLoan(long long int loan)
{
	this->loan_amount = loan;
}
void loan_application::setYearlyProfits(vector<long long int> profits)
{
	this->estimated_yearly_profits = profits;
}
void loan_application::setDecisionDate(const string& s)
{
	this->decision_date = s;
}

bool loan_application::operator < (const loan_application& s) const
{
	return this->getPriority() < s.getPriority();
}
bool loan_application::operator == (const loan_application& s) const
{
	return this->getPriority() == s.getPriority();
}
bool loan_application::operator <= (const loan_application& s) const
{
	return ((*this) < s || (*this) == s);
}
bool loan_application::operator > (const loan_application& s) const
{
	return !((*this) <= s);
}
bool loan_application::operator >= (const loan_application& s) const
{
	return !((*this) < s);
}
loan_application& loan_application::operator = (const loan_application& s)
{
	this->applicant_full_name = s.applicant_full_name;
	this->years_of_relevant_education = s.years_of_relevant_education;
	this->years_of_relevant_experience = s.years_of_relevant_experience;
	this->loan_amount = s.loan_amount;
	this->estimated_yearly_profits = s.estimated_yearly_profits;
	this->decision_date = s.decision_date;
	return *this;
}
ostream& operator << (ostream& os, const loan_application& s)
{
	os << "\"" << s.getFullName() << "\"," << s.getLoan();
	return os;
}
loan_application::~loan_application()
{
}