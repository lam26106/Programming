#include "tttboard.h"
#include <vector>
#include <iostream>

using namespace std;

tttboard::tttboard(int n){
	n_ = n;
	for (int i = 0; i<n*n; i++)
		b_.push_back(0);
}
tttboard::tttboard(int n, vector<int> a)
{
	n_ = n;
	b_ = a;
}


vector<tttboard> tttboard::possibleNextBoards(int turn) const{
	vector<tttboard> children;
	//ToDo for step 2
	for (int i = 0; i < b_.size(); ++i)
	{
		if (b_[i] == 0)
		{
			tttboard temp(this->n_, this->b_);
			temp.b_[i] = turn;
			children.push_back(temp);
		}
	}
	return children;
}
ostream& operator << (ostream& os, const tttboard& E)
{
	cout << "{";
	for (int i = 0; i < E.b_.size(); ++i)
		cout << E.b_[i] << ", ";
	cout << "\b\b" << "}";
	return os;
}

bool tttboard::operator == (const tttboard& E)
{
	return b_ == E.b_;
}




