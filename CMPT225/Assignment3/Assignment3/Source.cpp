#include "tttboard.h"
#include "gTree.h"

#include <vector>
#include <iostream>
#include <algorithm>
#define X 1
#define O -1
using namespace std;

void Tokenize(string line, vector<string> & tokens, string delimiters = "\t "){
	string token = "";
	string OneCharString = " ";
	for (int i = 0; i<line.size(); i++)
		if (find(delimiters.begin(), delimiters.end(), line[i]) != delimiters.end()) // line[i] is one of the delimiter characters
		{
			if (token != "")
				tokens.push_back(token);
			token = "";
		}
		else
		{
			OneCharString[0] = line[i];
			token += OneCharString;
		}

	if (token != "")
		tokens.push_back(token);
}

gTree<tttboard> tttTree;
void tic_tac_toe(gTree<tttboard>& tttTree, gTNode<tttboard>* t, int turn = 1)
{
	vector<tttboard> children = t->getData().possibleNextBoards(turn);
	if (!children.empty())
	{
		for (int i = 0; i < children.size(); ++i)
		{
			//tttTree.addNode(children[i], t);
			//t->addChild(children[i]);
			//gTNode<tttboard> *temp = findItem(t, children[i]);
			gTNode<tttboard>* temp = new gTNode<tttboard>(children[i], t);
			t->getChildren().push_back(temp);
			tic_tac_toe(tttTree, temp, turn * -1);
		}
	}
}
int checkW(gTNode<tttboard> *parent, int turn, bool check);
int checkL(gTNode<tttboard> *parent, int turn, bool check)
{
	if (parent->getData().WinOrLose(turn * -1)){
		return 1;
	}
	vector<gTNode<tttboard>*> subChildren = parent->getChildren();
	int checkAlmostwin = parent->getData().checkAlmostWin(turn*-1);
	if (checkAlmostwin != -1 && parent->getData().checkAlmostWin(turn) == -1)
	{
		tttboard temp = parent->getData();
		temp[checkAlmostwin] = turn;
		for (int i = 0; i < subChildren.size(); ++i)
		{
			if (subChildren[i]->getData() == temp)
			{
				int subCheck = 0;
				subCheck = checkW(subChildren[i], turn * -1, false);
				if (subCheck == 0)
					return 0;
				else if (subCheck == -1)
					return -1;
				return 1;
			}
		}

	}
	else{
		if (subChildren.size() != 0)
		{
			int subCheck = 0, flag = 0;
			for (int i = 0; i < subChildren.size(); ++i)
			{
				//if (subChildren[i]->getData().WinOrLose(turn * -1)) //If the other player already won
				//	return 1;
				subCheck = checkW(subChildren[i], turn * -1, false);
				if (subCheck == 0){
					return 0;
				}
				else if (subCheck == -1)
					return -1;
			}
			return 1;
		}
	}
	if (parent->getData().WinOrLose(turn)){
		return 0;
	}
	return -1;
	
}
int checkW(gTNode<tttboard> *parent, int turn, bool check)
{
	if (parent->getData().WinOrLose(turn))
		return 1;
	vector<gTNode<tttboard>*> subChildren = parent->getChildren();
	int checkAlmostwin = parent->getData().checkAlmostWin(turn*-1);
	if (checkAlmostwin != -1 && parent->getData().checkAlmostWin(turn) == -1)
	{
		//cout << "aihihh\n";
		tttboard temp = parent->getData();
		temp[checkAlmostwin] = turn;
		for (int i = 0; i < subChildren.size(); ++i)
		{
			if (subChildren[i]->getData() == temp)
			{
				int subCheck = 0;
				subCheck = checkL(subChildren[i], turn * -1, false);
				if (subCheck == 1)
					return 1;
				else if (subCheck == 0)
					return 0;
				return -1;
			}
		}

	}
	else{
		if (subChildren.size() != 0)
		{
			int subCheck = 0, flag = 0;
			for (int i = 0; i < subChildren.size(); ++i)
			{
				if (subChildren[i]->getData().WinOrLose(turn))
					return 1;
				subCheck = checkL(subChildren[i], turn * -1, false);
				if (subCheck == 1)
					return 1;
				else if (subCheck == -1){
					flag = 1;
				}
			}
			if (flag == 1){
				return -1;
			}
			return 0;
		}
	}
	if (parent->getData().WinOrLose(turn *-1))
		return 0;
	return -1;


}


int CurrentTurn(int s, vector<int> cur)
{
	int X_ = 0, O_ = 0;
	for (int i = 0; i < cur.size(); ++i)
	{
		if (cur[i] == 1)
			X_++;
		else if (cur[i] == -1)
			O_++;
	}
	if (X_ + O_ == s)
		return 0;
	if (X_ == O_)
		return X;
	else
		return O;
}

int main(int argc, char ** argv)
{
	//int n = 3;
	////vector<int> board(n*n, 0);
	//vector<int> board = { -1, 1, 1, 0, 0, 1, -1, -1, 1 };
	//tttboard root(n, board);
	//int turn = CurrentTurn(n*n, board);
	//tttTree.addNode(root, NULL);
	//tic_tac_toe(tttTree, tttTree.getRoot(), turn);
	//cout << "pre-order:\n";
	//preorderPrint(tttTree.getRoot());
	//cout << "\npost-order:\n";
	//postorderPrint(tttTree.getRoot());
	//cout << "\nsize: " << size(tttTree.getRoot()) << endl;
	//string str_board = argv[2];
	//vector<string> tokens;
	//Tokenize(str_board, tokens, " {,}");
	////vector<int> board;
	//for (int i = 0; i < tokens.size(); i++){
	//	board.push_back(atoi(tokens[i].c_str()));
	//}
	//if (n*n != board.size()){
	//	cout << "n and board are not consistent!\n";
	//	return 1;
	//}



	int n = 3;
	vector<int> board = { 0,0, 0, 0, 0, 0, 0, 0, 0 };
	tttboard cur(n, board);
	tttTree.addNode(cur, NULL);
	int turn = CurrentTurn(n*n, board);
	tic_tac_toe(tttTree, tttTree.getRoot(), turn);

	gTNode<tttboard> *temp = findItem(tttTree.getRoot(), cur);
	cout << turn << endl;
	if (temp->getData().WinOrLose(turn) || checkW(temp, turn, false) == 1)
		cout << "Winning board\n";
	else if (!temp->getData().WinOrLose(turn) && checkL(temp, turn, false) == 1)
		cout << "Losing board\n";
	else
		cout << "Not sure\n";

	//gTNode<int> *root = new gTNode<int>(10, NULL);
	//gTNode<int> *test;
	//root->addChild(2);
	//root->getChildren()[0]->addChild(1);
	//test = root->getChildren()[0];
	//root->~gTNode();
	//cout << test->getData();
	//root->getChildren

	//int n = 3;
	//vector<int> board = { -1, 1, -1, 1, 1, 1, -1, -1, 1 };
	//tttboard cur(n, board);
	//if (cur.WinOrLose(1))
	//	cout << "ahihihiih\n";
	system("pause");
	return 0;
}