#ifndef TTTBOARD_H
#define TTTBOARD_H

#include <iostream>
#include <vector>

using namespace std;

class tttboard{
public:

	tttboard(int n = 3);
	tttboard(int, vector<int>);
	vector<tttboard> possibleNextBoards(int turn) const;
	bool operator == (const tttboard& E);
	friend ostream& operator << (ostream& os, const tttboard& E);
	vector<int> getBoard()
	{
		return b_;
	}
	int& operator [](int index)
	{
		return b_[index];
	}
	int WinOrLose(int turn)
	{
		bool check;
		for (int i = 0; i < b_.size(); i += n_)
		{
			if (b_[i] == turn){
				check = true;
				for (int j = i + 1; j < i + n_; ++j)
				{
					if (b_[i] != b_[j]){
						check = false;
						break;
					}
				}
				if (check == true)
					return 1;
			}
		}
		for (int i = 0; i < n_; ++i)
		{
			if (b_[i] == turn){
				check = true;
				for (int j = i; j < b_.size(); j += n_)
				{
					if (b_[i] != b_[j])
					{
						check = false;
						break;
					}
				}
				if (check == true)
					return 1;
			}
		}
		if (b_[0] == turn){
			check = true;
			for (int i = 0 + n_ + 1; i < b_.size(); i = i + n_ + 1)
			{
				if (b_[i] != b_[0])
				{
					check = false;
					break;
				}
			}
			if (check == true)
				return 1;
		}
		if (b_[n_ - 1] == turn){
			for (int i = 2 * (n_ - 1); i < b_.size() - 1; i = i + n_ - 1)
			{
				if (b_[i] != b_[n_ - 1])
				{
					check = false;
					break;
				}
			}
			if (check == true)
				return 1;
		}
		return 0;
	}
	int checkAlmostWin(int turn)
	{
		int flag = -1;
		int count;
		bool check;
		for (int i = 0; i < b_.size(); i += n_)
		{
			count = 0;
			flag = -1;
				for (int j = i ; j < i + n_; ++j)
				{
					if (b_[j] == turn)
						count++;
					else if (b_[j] == 0)
						flag = j;
				}
				if (count == 2)
					return flag;
		}

		for (int i = 0; i < n_; ++i)
		{
			count = 0;
			flag = -1;
				for (int j = i; j < b_.size(); j += n_)
				{
					if (b_[j] == turn)
						count++;
					else if (b_[j] == 0)
						flag = j;

				}
				if (count == 2)
					return flag;

		}

		for (int i = 0; i < b_.size(); i = i + n_ + 1)
		{
			count = 0;
			flag = -1;
			if (b_[i] == turn)
				count++;
			else if (b_[i] == 0)
				flag = i;
		}
		if (count == 2)
			return flag;
		for (int i = n_ - 1; i < b_.size() - 1; i = i + n_ - 1)
		{
			count = 0;
			flag = -1;
			if (b_[i] == turn)
			{
				count++;
			}
			else if (b_[i] == 0)
				flag = i;
		}
		if (count == 2)
			return flag;
		return -1;
	}
	


private:
	vector<int> b_;
	int n_;
};

#endif 