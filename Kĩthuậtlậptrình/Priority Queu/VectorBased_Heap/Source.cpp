#include <iostream>
#include <vector>
#include <queue>
using namespace std;
template<class E>
class MinHeap
{
private:
	vector<E> heap;
	void _swap(E& a, E& b)
	{
		E c = a;
		a = b;
		b = c;
	}
public:
	MinHeap() : heap(1){}
	int size() { return heap.size() - 1; }
	int parent(int i) { return i / 2; }
	int left(int i) { return i * 2; }
	int right(int i) { return i * 2 + 1; }
	bool hasleft(int i) { return i * 2 < heap.size(); }
	bool hasright(int i) { return i * 2 + 1 < heap.size(); }
	bool isroot(int i) { return i == 1; }
	bool empty() { return heap.size() == 1; }
	const E& min() const
	{
		return heap[1];
	}
	void insert(const E& itm)
	{
		heap.push_back(itm);
		int i = heap.size() - 1;
		while (!isroot(i) && heap[i] < heap[parent(i)])
		{
			swap(heap[i], heap[parent(i)]);
			i = parent(i);
		}
	}
	void removeMin()
	{
		if (heap.size() == 2)
			heap.resize(1);
		else{
			swap(heap[1], heap[heap.size() - 1]);
			heap.resize(heap.size() - 1);
			int i = 1;
			while (hasleft(i))
			{
				int j = left(i);
				if (hasright(i) && heap[right(i)] < heap[j])
					j = right(i);
				if (heap[j] < heap[i])
				{
					_swap(heap[i], heap[j]);
					i = j;
				}
				else
					break;
			}
		}

	}
	void Minheapify(int i)
	{
		int ii = left(i);
		int jj = right(i);
		int smallest = i;
		if (ii < heap.size() && heap[ii] < heap[i]){
			smallest == ii;
			if (jj < heap.size() && heap[jj] < heap[smallest])
				smallest = jj;
			if (smallest != i)
			{
				_swap(heap[smallest], heap[i]);
				Minheapify(smallest);
			}
		}
	}
};
#include<string>
void newLine()
{
	char c;
	do{
		cin.get(c);
	} while (c != '\n');
}
void _swap(int& a, int& b){
	int c = a;
	a = b;
	b = c;
}
void heapify(vector<int>& t, int i, int s)
{
	int left = i * 2 + 1;
	int right = i * 2 + 2;
	if (left < s){
		int j = left;
		if (right < s && t[j] > t[right])
			j = right;
		if (t[j] < t[i])
		{
			_swap(t[j], t[i]);
			heapify(t, j, s);
		}
	}
}

void heapsort(vector<int>& t, int n)
{
	for (int i = n / 2 - 1; i >= 0; --i)
		heapify(t, i, n);
	for (int i = n - 1; i >= 0; --i)
	{
		_swap(t[0], t[i]);
		heapify(t, 0, i);
	}
}
bool compare(int a, int b){
	return a < b;
}
int main()
{

	cout << endl;
	system("pause");
	return 0;
}