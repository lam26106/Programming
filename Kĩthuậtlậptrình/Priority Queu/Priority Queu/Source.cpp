#include<iostream>
using namespace std;
#include"Node.h"
//Using user-defined list

class RunTimeExeption
{
private:
	string errorMsg;
public:
	RunTimeExeption(const string& msg)
	{
		errorMsg = msg;
	}
};

class QueuEmpty : public RunTimeExeption
{
public:
	QueuEmpty(const string& msg) : RunTimeExeption(msg){}
};

class QueuFull : public RunTimeExeption
{
public:
	QueuFull(const string& msg) : RunTimeExeption(msg){}
};
template<class T>
class SortedLinkedList
{
private:
	Node<T>*head, *tail;
	int n;
public:
	SortedLinkedList()
	{
		head = tail = NULL;
		n = 0;
	}
	SortedLinkedList<T>(const SortedLinkedList<T>& s)
	{
		Node<T>*temp = s.head;
		while (temp != NULL)
		{
			this->addBack(temp->data);
			temp = temp->link;
		}
	}
	Node<T>* getHead() const
	{
		return head;
	}
	int size() const
	{
		return n;
	}
	bool empty() const
	{
		return head == NULL;
	}
	void addFront(const T& elem)
	{
		Node<T>* temp = new Node<T>();
		temp->data = elem;
		temp->link = head;
		head = temp;
		n++;
	}
	void addBack(const T& elem)
	{
		Node<T>* temp = new Node<T>();
		temp->data = elem;
		temp->link = NULL;
		if (head == NULL)
		{
			head = tail = temp;
			return;
		}
		tail->link = temp;
		tail = temp;
		n++;
	}

	void add_increasing_order(const T& elem)
	{
		if (head == NULL)
			addFront(elem);
		else if (elem < head->data)
		{
			addFront(elem);
			return;
		}
		else{
			Node<T>*temp = head;
			while (temp->link != NULL && temp->data <= elem && temp->link->data >= elem)
			{
				temp = temp->link;
			}
			Node<T>* temp1 = new Node<T>();
			temp1->data = elem;
			temp1->link = temp->link;
			temp->link = temp1;
			n++;
		}
		
	}
	void add_decreasing_order(const T& elem)
	{
		if (head == NULL)
			addFront(elem);
		else if(elem >= head->data)
		{
			addFront(elem);
			return;
		}
		else
		{
			Node<T>*temp = head;
			while (temp->link != NULL && temp->data >= elem && temp->link->data <= elem)
			{
				temp = temp->link;
			}
			Node<T>* temp1 = new Node<T>();
			temp1->data = elem;
			temp1->link = temp->link;
			temp->link = temp1;
			n++;
		}

	}
	void removeFront()
	{
		if (empty()){
			cout << "Empty linked list. Can not remove.";
			return;
		}
		Node<T>* temp = head;
		head = head->link;
		n--;
		delete temp;
	}
	~SortedLinkedList()
	{
		while (!empty())
			removeFront();
	}
};
template<class T>
class PriorityQueu
{
private:
	SortedLinkedList<T> list;
public:
	PriorityQueu() : list()
	{

	}
	PriorityQueu(const PriorityQueu<T>& s)
	{
		Node<T>*temp = s.list.getHead();
		while (temp != NULL)
		{
			this->list.addBack(temp->data);
			temp = temp->link;
		}
	}
	int size()
	{
		return list.size();
	}
	bool empty()
	{
		return list.empty();
	}
	void insert(const T& elem)
	{
		list.add_decreasing_order(elem);
	}
	const T& top() const throw (QueuEmpty)
	{
		if (list.empty())
		{
			throw QueuEmpty("Queu is empty");
		}
		return list.getHead()->data;
	}
	void removeTop()
	{
		list.removeFront();
	}
};
int main()
{
	PriorityQueu<int> test;
	test.insert(12);
	cout << test.top() << endl;
	test.removeTop();
	test.insert(2);
	//test.removeTop();
	cout << test.top();
	//cout << test.top() << endl;
	system("pause");
	return 0;
}