#include<iostream>
#include<list>
#include<queue>
using namespace std;

class RunTimeExeption
{
private:
	string errorMsg;
public:
	RunTimeExeption(const string& msg)
	{
		errorMsg = msg;
	}
};

class QueuEmpty : public RunTimeExeption
{
public:
	QueuEmpty(const string& msg) : RunTimeExeption(msg){}
};

class QueuFull : public RunTimeExeption
{
public:
	QueuFull(const string& msg) : RunTimeExeption(msg){}
};
template<class T>
class LeftRight
{
public:
	bool operator()(const T&a, const T&b) const
	{
		return a < b;
	}
};
template<class T>
class BottomTop
{
public:
	bool operator()(const T&a, const T&b) const
	{
		return a > b;
	}
};
template <class T, class E>
class PriorityQueu
{
private:
	list<T> list;
	E isLess;
public:
	PriorityQueu()
	{
	}
	PriorityQueu(const PriorityQueu<T, E>& s)
	{
		
	}
	int size()
	{
		return list.size();
	}
	bool empty()
	{
		return list.empty();
	}
	void insert(const T& elem)
	{
		
	}
	const T& top() const throw (QueuEmpty)
	{
		if (list.empty())
		{
			throw QueuEmpty("Queu is empty");
		}
	}
	void removeTop()
	{
	}
};
int main()
{
	priority_queue<int> test;
	for (int i = 0; i <= 10; ++i) test.push(i);
	cout << test.top();
	system("pause");
	return 0;
}