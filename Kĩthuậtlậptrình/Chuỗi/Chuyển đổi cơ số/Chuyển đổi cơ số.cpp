#include <iostream>
using namespace std;

void he16(int num)
{
	char heso16[] = "0123456789ABCDEF";
	if (num < 16)
	{
		cout << heso16[num];
		return;
	}
	else 
	{
		he16(num / 16);
		cout << heso16[num % 16];
	}
}
void he2_8_10(int num, int heso)
{
	if (num < heso)
	{
		cout << num % heso;
		return;
	}
	else
	{
		he2_8_10(num / heso, heso);
		cout << num % heso;
	}
}
int main()
{
	int num = 250;
	he2_8_10(num, 10);
	system("pause");
	return 0;
}