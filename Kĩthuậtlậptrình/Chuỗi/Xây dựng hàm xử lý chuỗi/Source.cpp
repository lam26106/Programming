#include <iostream>
using namespace std;
int my_strlen(char *str)
{
	int count = 0;
	for (; str[count] != '\0'; ++count){}
	return count;
}
void my_strcpy(char*s1, char *s2)
{
	for (int i = 0; i <= my_strlen(s2); ++i)
	{
		s1[i] = s2[i];
	}
}
char* my_strdup(char*str)
{
	char *s = new char[my_strlen(str) + 1];
	my_strcpy(s, str);
	return s;
}
void my_strlwr(char *s)
{
	for (int i = 0; s[i] != '\0'; ++i)
	{
		if (s[i] >= 'A' && s[i] <= 'Z')
			s[i] += 32;
	}
}
void my_strupr(char *s)
{
	for (int i = 0; s[i] != '\0'; ++i)
	{
		if (s[i] >= 'a' && s[i] <= 'z')
			s[i] -= 32;
	}
}
void my_strrev(char *s)
{
	int maxIndex = my_strlen(s);
	for (int i = 0; i <= (maxIndex - 1 )/ 2; ++i)
	{
		int temp = s[i];
		s[i] = s[maxIndex - 1 - i];
		s[maxIndex - 1 - i] = temp;
	}
}
int my_stricmp(char *s1, char *s2)
{
	for (int i = 0; s1[i] != '\0' || s2[i] != '\0'; ++i)
	{
		if (s1[i] >= 'A' && s1[i] <= 'Z' && s2[i] >= 'a' && s2[i] <= 'z')
		{
			if ((int)s1[i] + 32 < (int)s2[i])
				return -1;
			else if ((int)s1[i] + 32 > (int)s2[i])
				return 1;
		}
		else if (s1[i] >= 'a' && s1[i] <= 'z' && s2[i] >= 'A' && s2[i] <= 'Z')
		{
			if ((int)s1[i] - 32 < (int)s2[i])
				return -1;
			else if ((int)s1[i] - 32 > (int)s2[i])
				return 1;
		}
		else if (s1[i] < s2[i])
			return -1;
		else if (s1[i] > s2[i])
			return 1;		
	}
	return 0;
}
char* my_strstr(char *s1, char *s2)
{
	int i = 0;
	while (s1[i] != '\0')
	{
		if (s1[i] == s2[0])
		{
			bool check = true;
			int index = i;
			for (int j = 1; s2[j] != '\0'; ++j)
			{
				index++;
				if (s2[j] != s1[index])
				{
					check = false;
					break;
				}
			}
			if (check == true) {
				char *p = new char[my_strlen(s1) - i];
				int indexOfp = 0;
				for (int j = i; j <= strlen(s1); ++j)
					p[indexOfp++] = s1[j];
				return p;
			}
		}
		++i;
	}
	return NULL;
}
void my_strcat(char *s1, char *s2)
{
	int j = 0;
	for (int i = my_strlen(s1); j <= my_strlen(s2); ++i)
	{
		s1[i] = s2[j];
		++j;
	}
}
int my_strstr2(char *str1, char *str2)
{
	int i = 0, count = 0;
	while (str1[i] != '\0')
	{
		if (str1[i] == str2[0])
		{
			int index = i;
			bool check = true;
			for (int j = 1; str2[j] != '\0'; ++j)
			{
				index++;
				if (str2[j] != str1[index])
				{
					check = false;
					break;
				}
			}
			if (check == true)
			{
				count++;
			}
		}
		++i;
	}
	return count;
}
int my_atoi(char *s)
{
	int num = 0;
	for (int i = 0; s[i] != '\0'; ++i)
	{
		if (s[i] >= '0' && s[i] <= '9')
		{
			num = num * 10 + int(s[i]) - 48;
		}
		else if (i > 0)
			break;
	}
	if (s[0] == '-')
		num *= -1;
	return num;
}
char* my_itoa( int num, int coso)
{
	char *s  = new char[15];
	char hexa[] = "0123456789ABCDEF";
	int j = 0;
	while (num != 0)
	{
		int ketqua = num % coso;
		if (ketqua < 10)
			s[j++] = ketqua + 48;
		else
			s[j++] = hexa[ketqua];
		num /= coso;
	}
	s[j] = '\0';
	for (int i = 0; i <= (j - 1) / 2; ++i)
	{
		char temp = s[i];
		s[i] = s[j - 1 - i];
		s[j - 1 - i] = temp;
	}
	return s;
}
int index = -1;
char* my_strtok(char*s1, char*s2)
{
	char *s = new char[my_strlen(s1) + 1];
	for (int i = 0; i <= my_strlen(s1); ++i)
	{
		s[i] = s1[i];
	}
	if (s1 != NULL && index == -1)
	{
		for (int i = 0; s1[i] != '\0'; ++i)
		{
			if (s1[i] == s2[0])
			{
				int j = i;
				bool check = true;
				for (int k = 1; s2[k] != '\0'; ++k)
				{
					j++;
					if (s2[k] != s1[j]) {
						check = false;
						break;
					}
				}
				if (check == true)
				{
					index = j+1;
					char *s3 = new char[i];
					for (int f = 0; f < i; ++f)
						s3[f] = s1[f];
					s3[i] = '\0';
					return s3;
				}
			}
		}
	}
	else if (s1 == NULL && index != -1)
	{
		int index2 = index;
		for (int i = index; s[i] != '\0'; ++i)
		{
			if (s[i] == s2[0])
			{
				int j = i;
				bool check = true;
				for (int k = 1; s2[k] != '\0'; ++k)
				{
					j++;
					if (s2[k] != s[j]) {
						check = false;
						break;
					}
				}
				if (check == true)
				{
					index = j+1;
					char *s3 = new char[j-i];
					for (int f = 0; f < i; ++f)
					{
						s3[f] = s[index2];
						index2++;
					}
					s3[i] = '\0';
					return s3;
				}
			}
		}
	}
}
int main()
{
	char str1[] = "Lam";
	char str2[] = ", ";
	//cout << s3;
	//s3 = my_strtok(NULL, str2);
	//cout << c;

	
	system("pause");
	return 0;
}