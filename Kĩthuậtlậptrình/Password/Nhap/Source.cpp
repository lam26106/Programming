#include <iostream>
#include <string>
#include <fstream>
#include <vector>
using namespace std;
struct SinhVien
{
	string Ten, sdt, Mssv;
	int ngay, thang, nam;
	float dToan, dLy, dHoa;

};
typedef struct SinhVien SINHVIEN;
void Nhap1SinhVien(ifstream &FileIn, SINHVIEN &sv)
{
	getline(FileIn, sv.Mssv, '-');
	cout << sv.Mssv.length() << " ";
	sv.Mssv.erase(sv.Mssv.begin() + (sv.Mssv.length() - 1));
	FileIn.seekg(1, SEEK_CUR);
	getline(FileIn, sv.Ten, '-');
	cout << sv.Ten.length() << endl;
	sv.Ten.erase(sv.Ten.begin() + (sv.Ten.length() - 1));
	FileIn.seekg(1, SEEK_CUR);
	FileIn >> sv.sdt;
	FileIn.seekg(3, SEEK_CUR);
	FileIn >> sv.ngay;
	FileIn.seekg(1, SEEK_CUR);
	FileIn >> sv.thang;
	FileIn.seekg(1, SEEK_CUR);
	FileIn >> sv.nam;
	FileIn.seekg(2, SEEK_CUR);
	FileIn >> sv.dToan >> sv.dLy >> sv.dHoa;
	string temp;
	getline(FileIn, temp);
}
void Xuat1SinhVien(ofstream &FileOut, SINHVIEN sv)
{

	FileOut << " MSSV : " << sv.Mssv << endl;
	FileOut << " Ten : " << sv.Ten << endl;
	FileOut << " SDT : " << sv.sdt << endl;
	FileOut << " Sinh ngay " << sv.ngay << " thang " << sv.thang << " nam " << sv.nam << endl;
	FileOut << " Diem Toan :" << sv.dToan << endl;
	FileOut << " Diem Ly  :" << sv.dLy << endl;
	FileOut << " Diem Hoa :" << sv.dHoa << endl;
	FileOut << "----------------------------------------------------" << endl;

}
void NhapDanhSachSinhVien(ifstream &FileIn, vector<SINHVIEN> &arr)
{
	while (!FileIn.eof())
	{
		SINHVIEN sv;
		Nhap1SinhVien(FileIn, sv);
		arr.push_back(sv);
	}
}
void XuatDanhSachSinhVien(ofstream &FileOut, vector<SINHVIEN> arr)
{
	for (int i = 0; i < arr.size(); ++i)
	{
		Xuat1SinhVien(FileOut, arr[i]);
	}
}
int main()
{

	ifstream FileIn("INPUT.TXT");

	vector<SINHVIEN> arr;
	//SINHVIEN sv;
	NhapDanhSachSinhVien(FileIn, arr);
	//Nhap1SinhVien(FileIn, sv);
	FileIn.close();

	ofstream FileOut("OUTPUT.TXT");

	XuatDanhSachSinhVien(FileOut, arr);
	//	Xuat1SinhVien(FileOut, sv);
	FileOut.close();

	system("pause");
	return 0;
}