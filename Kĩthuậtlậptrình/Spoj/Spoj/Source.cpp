﻿#include <iostream>
using namespace std;

int my_strlen(char *s)
{
	int count = 0;
	for (int i = 0; s[i] != '\0'; ++i)
		count++;
	return count;
}
char* my_strdup(char *s)
{
	char*temp = new char[my_strlen(s) + 1];
	for (int i = 0; i <= my_strlen(s); ++i)
		temp[i] = s[i];
	return temp;
}
void my_strrev(char *s)
{
	int indexmax = my_strlen(s) - 1;
	for (int i = 0; i <= indexmax / 2; ++i)
	{
		char temp = s[i];
		s[i] = s[indexmax - i];
		s[indexmax - i] = temp;
	}
}
int my_strcmp(char *s1, char*s2)
{
	for (int i = 0; i <= my_strlen(s1) && i <= my_strlen(s2); ++i)
	{
		if (s1[i] < s2[i])
			return -1;
		else if (s1[i] > s2[i])
			return 1;
	}
	return 0;
}
bool checkPalindrome(char *s)
{
	char*temp = my_strdup(s);
	my_strrev(temp);
	if (my_strcmp(temp, s) == 0)
		return true;
	return false;
}

int main()
{
	int n;
	cin >> n;
	char**main = new char*[n];
	for (int i = 0; i < n; ++i)
	{
		main[i] = new char[1000002];
		cin >> main[i];
	}
	int numOfdigit;
	for (int i = 0; i < n; ++i)
	{
		if (my_strlen(main[i]) & 2 != 0)
		{
			int indexmax = my_strlen(main[i]) - 1;
			int mid = indexmax / 2;
			if (checkPalindrome(main[i]) == false)
			{
				bool check = false;
				for (int j = mid + 1; j <= indexmax; ++j)
				{
					if (main[i][j] < main[i][indexmax - j])
					{
						for (int k = j; k <= indexmax; ++k)
							main[i][k] = main[i][indexmax - k];
						check = true;
						break;
					}
				}
				if (check == true)
					continue;
				if (main[i][mid] != '9')
				{
					main[i][mid] += 1;
					for (int k = mid + 1; k <= indexmax; ++k)
						main[i][k] = main[i][indexmax - k];
				}
				else
				{
					for (int k = mid; k <= indexmax; ++k)
					{
						if (main[i][k] == '9' && main[i][indexmax - k] == '9')
						{
							main[i][k] = '0';
							main[i][indexmax - k] = '0';
						}
						else
						{
							main[i][indexmax - k]++;
							main[i][k] = main[i][indexmax - k];

							for (int p = k + 1; p <= indexmax; ++p)
								main[i][p] = main[i][indexmax - p];
							break;
						}
					}
				}
			}
			else
			{
				if (main[i][mid] != '9')
				{
					main[i][mid] ++;
				}
				else
				{
					for (int j = mid; j <= indexmax; ++j)
					{
						if (main[i][j] == '9')
						{
							main[i][j] = '0';
							main[i][indexmax - j] = '0';
							if (j == indexmax)
							{
								main[i][j + 1] = '1';
								main[i][j + 2] = '\0';
								main[i][0] = '1';
								break;
							}
						}
						else
						{
							main[i][indexmax - j]++;
							main[i][j] = main[i][indexmax - j];
							break;
						}
					}

				}
			}
		}
		else if (my_strlen(main[i]) % 2 == 0)
		{
			int indexmax = my_strlen(main[i]) - 1;
			int mid = indexmax / 2;
			if (checkPalindrome(main[i]) == true)
			{
				if (main[i][mid] != '9')
				{
					main[i][mid] += 1;
					main[i][mid + 1] += 1;
					continue;
				}
				else
				{
					for (int j = mid; j >= 0; --j)
					{
						if (main[i][j] == '9')
						{
							main[i][j] = main[i][indexmax - j] = '0';
						}
						else
						{
							main[i][j]++;
							main[i][indexmax - j]++;
							break;
						}
						if (j == 0)
						{
							main[i][j] = '1';
							main[i][indexmax + 1] = '1';
							main[i][indexmax + 2] = '\0';
							break;
						}

					}
					continue;
				}
			}
			else
			{
				if (main[i][mid] < main[i][mid + 1])
				{
					main[i][mid]++;
					main[i][mid + 1] = main[i][mid];
					for (int j = mid - 1; j >= 0; --j)
					{
						main[i][indexmax - j] = main[i][j];
					}
					continue;
				}
				else
				{
					main[i][mid + 1] = main[i][mid];
					for (int j = mid - 1; j >= 0; --j)
					{
						main[i][indexmax - j] = main[i][j];
					}
					continue;
				}
			}
		}
	}
	cout << endl;
	for (int i = 0; i < n; ++i)
	{
		cout << main[i] << endl;
	}
	system("pause");
	return 0;
}