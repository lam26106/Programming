#include <iostream>
#include <vector>
using namespace std;

void output(vector<char> str)
{
	for (int i = 0; i < str.size(); ++i)
		cout << str[i];
	cout << endl;
}
int my_strlen(char*str)
{
	int count = 0;
	for (int i = 0; str[i] != '\0'; ++i)
		count++;
	return count;
}
void addChar(vector<char>&str, vector <char> s)
{
	int sizeOFstr = str.size();
	int sizeOfs = s.size();
	int commonlength = sizeOFstr > sizeOfs ? sizeOFstr + 1 : sizeOfs + 1;
	for (int i = 0; i < commonlength - sizeOFstr; ++i)
		str.insert(str.begin(), '0');
	for (int i = 0; i < commonlength - sizeOfs; ++i)
		s.insert(s.begin(), '0');
	vector<char> temp(commonlength);
	int nho = 0;
	for (int i = commonlength - 1; i >= 0; --i)
	{
		int kq = str[i] - 48 + s[i] - 48 + nho;
		nho = kq / 10;
		temp[i] = kq % 10 + 48;
	}
	for (int i = 0;; ++i)
	{
		if (temp[i] == '0')
		{
			temp.erase(temp.begin());
			i--;
		}
		else
			break;
	}
	str = temp;
}
bool check(vector<char> str)
{
	vector<char> temp;
	int idxMax = str.size() - 1;
	for (int i = idxMax; i >= 0; --i)
	{
		char c = str[i];
		temp.push_back(c);
	}
	for (int i = 0; i <= idxMax; ++i)
	{
		if (temp[i] != str[i])
			return false;
	}
	return true;
}
int _3NPLUS1(int n)
{
	int count = 0;

	while (true)
	{
		count++;
		if (n == 1)
			return count;
		if (n % 2 != 0)
			n = 3 * n + 1;
		else
			n = n / 2;
	}
}
int main() {

	int n;
	cin >> n;
	int **a = new int*[n];
	for (int i = 0; i < n; ++i)
		a[i] = new int[2];
	for (int i = 0; i < n; ++i)
	{
		for (int j = 0; j < 2; ++j)
			cin >> a[i][j];
	}
	int count = 0;
	for (int i = 0; i < n ; ++i)
	{
		for (int k = i + 1; k < n; ++k)
		{
			if (a[i][0] == a[k][1])
				count++;
			if (a[i][1] == a[k][0])
				count++;
		}
	}
	cout << count;
	system("pause");
	return 0;
}