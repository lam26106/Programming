#include <iostream>
using namespace std;

struct NODE
{
	int Data;
	NODE *link;
};
struct LIST
{
	NODE *phead, *ptail;
};

void Init(LIST &l)
{
	l.phead = l.ptail = NULL;
}
NODE* getNode(int DATA)
{
	NODE *p = new NODE;
	p->Data = DATA;
	p->link = NULL;
	return p;
}

void head_insert(LIST &l, NODE *p)
{
	if (l.phead == NULL)
		l.phead = l.ptail = p;
	else
	{
		p->link = l.phead;
		l.phead = p;
	}
}
void tail_insert(LIST &l, NODE *p)
{
	if (l.phead == NULL)
		l.phead = l.ptail = p;
	else
	{
		l.ptail->link = p;
		l.ptail = p;
	}
}

void Input(LIST &l)
{
	Init(l);
	int n;
	cout << "Input number of element: ";
	cin >> n;
	for (int i = 1; i <= n; ++i)
	{
		int DATA;
		cout << "Input DATA: ";
		cin >> DATA;
		NODE *p;
		p = getNode(DATA);
		tail_insert(l, p);
	}
}
void Output(const LIST &l)
{
	for (NODE* p = l.phead; p != NULL; p = p->link)
	{
		cout << p->Data << " ";
	}
}
void deleteList(LIST &l)
{
	NODE *p;
	while (l.phead != NULL)
	{
		p = l.phead;
		l.phead = p->link;
		delete[]p;
	}
}
int main()
{

	system("pause");
	return 0;
}