#include <iostream>
#include <ctime>
using namespace std;

class NODE
{
private:
	int data;
	NODE *link;
public:
	//constructor
	NODE();
	NODE(const int&, NODE* );
	//getter
	int getData() const;
	NODE* getLink() const;

	//setter
	void setData(const int&);
	void setLink(NODE* );

};
//constructor
NODE::NODE() : data(0), link(nullptr)
{}

NODE::NODE(const int& DATA, NODE* p)
{
	data = DATA;
	link = p;
}

//getter
int NODE::getData() const
{
	return data;
}
NODE* NODE::getLink() const
{
	return link;
}

//setter
void NODE::setData(const int& temp)
{
	data = temp;
}
void NODE::setLink(NODE* p)
{
	link = p;
}
void Output(NODE *head)
{
	for (NODE*temp = head; temp != nullptr; temp = temp->getLink())
		cout << temp->getData() << " ";
}
void insert_head(NODE* &head, const int &DATA)
{
	NODE*temp = new NODE;
	temp->setData(DATA);
	temp->setLink(head);
	head = temp;
}
void insert_tail(NODE* &head, const int &DATA)
{
	NODE *temp = new NODE;
	temp->setData(DATA);
	temp->setLink(nullptr);
	NODE * tail = head;
	if (head == nullptr)
	{
		head = temp;
		return;
	}
	while (tail->getLink() != nullptr)
	{
		tail = tail->getLink();
	}
	tail->setLink(temp);
	tail = temp;
}
void insert_grouped(NODE* &head, const int &DATA)
{
	if (head == nullptr)
	{
		insert_head(head, DATA);
		return;
	}
	if (DATA % 2 == 0)
		insert_head(head, DATA);
	else
		insert_tail(head, DATA);
}
bool containDistintElements(NODE* head)
{
	for (NODE *temp = head; temp->getLink() != nullptr; temp = temp->getLink())
	{
		if (temp->getData() != temp->getLink()->getData())
			return false;
	}
	return true;
}
bool isIncreasing(NODE* &head)
{
	for (NODE* temp = head; temp ->getLink() != nullptr; temp = temp->getLink())
	{
		NODE*p = temp->getLink();
		if (temp->getData() > p->getData())
			return false;
	}
	return true;
}
bool checkPrime(int d)
{
	if (d < 2)
		return false;
	if (d % 2 == 0)
		return false;
	for (int i = 3; i < sqrt((double)d); i += 2)
	{
		if (d % i == 0)
			return false; 
	}
	return true;
}
bool containsPrime(NODE* &head)
{
	for (NODE* temp = head; temp != nullptr; temp = temp->getLink())
	{
		if (checkPrime(temp->getData()) == false)
			return false;
	}
	return true;
}
void insert_after(NODE *&p,int DATA)
{
	NODE*item = new NODE;
	item->setData(DATA);
	item->setLink(p->getLink());
	p->setLink(item);
	
}
void insert_increasing(NODE *&head, const int &DATA)
{
	if (head == nullptr)
	{
		insert_head(head, DATA);
		return;
	}
	if (DATA < head->getData())
	{
		insert_head(head, DATA);
		return;
	}
	NODE* temp;
	for (temp = head; temp->getLink() != nullptr; temp = temp->getLink())
	{
		NODE*p = temp->getLink(); 
		if (DATA >= temp->getData() && DATA <= p->getData())
		{
			insert_after(temp, DATA);
			temp = temp->getLink();
		}
	}
	if (DATA > temp->getData())
	{
		insert_after(temp, DATA);
	}
}
bool checkEvenandOdd(int data1, int data2)
{
	if (data1 % 2 == data2 % 2)
		return true;
	return false;
}

void insert_grouped_increasing(NODE *&head, int DATA)
{
	if (head == nullptr)
	{
		insert_head(head, DATA);
		Output(head);
		cout << endl;
		return;
	}
	NODE*border1 = nullptr, *tail = nullptr;
	NODE*border2 = nullptr;
	for (NODE *temp = head;; temp = temp->getLink())
	{

		if (temp->getLink() == nullptr)
		{
			tail = temp;
			break;
		}
		if (checkEvenandOdd(temp->getData(), temp->getLink()->getData()) == true)
		{
			border1 = temp;
			border2 = border1->getLink();
			continue;
		}

	}
	if (border1 == nullptr)
	{

		border1 = tail;
		tail = nullptr;
		border2 = nullptr;
		if (checkEvenandOdd(DATA, border1->getData()) == false)
		{
			insert_after(border1, DATA);
			tail = border1->getLink();
			border2 = tail;
			Output(head);
			cout << endl;
			return;
		}
		else
		{
			if (DATA > head->getData())
			{
				insert_head(head, DATA);
				Output(head);
				cout << endl;
				return;
			}
			else if (DATA < border1->getData())
			{
				insert_after(border1, DATA);
				border1 = border1->getLink();
				Output(head);
				cout << endl;
				return;
			}
			else
			{
				for (NODE*temp = head; temp->getLink() != border1; temp = temp->getLink())
				{
					if (DATA <= temp->getData() &&  DATA >= temp->getLink()->getData())
					{
						insert_after(temp, DATA);
					}
				}
				Output(head);
				cout << endl;
			}
		}
		return;

	}
	if (DATA > head->getData() && checkEvenandOdd(DATA, head->getData()) == true)
	{
		insert_head(head, DATA);
		Output(head);
		cout << endl;
		return;
	}
	else if (DATA < border1->getData() && checkEvenandOdd(DATA, border1->getData()) == true)
	{
		insert_after(border1, DATA);
		border1 = border1->getLink();
		Output(head);
		return;
	}
	else if (DATA > border2->getData() && checkEvenandOdd(DATA, border2->getData()) == true)
	{
		insert_after(border1, DATA);
		border2 = border1->getLink();
		Output(head);
		cout << endl;
		return;
	}
	else if (DATA < tail->getData() && checkEvenandOdd(DATA, tail->getData()) == true)
	{
		insert_after(tail, DATA);
		tail = tail->getLink();
		Output(head);
		cout << "hi" << endl;
		return;
	}
	else
	{
		for (NODE*temp = head; temp->getLink() != tail; temp = temp->getLink())
		{
			if (DATA <= temp->getData() && DATA >= temp->getLink()->getData() && checkEvenandOdd(DATA, temp->getData()) == true)
			{
				insert_after(temp, DATA);
				return;
			}
		}
		Output(head);
		cout << "hello" << endl;
	}
}
void Input(NODE *&head)
{
	int n;
	cout << "Input number of element: ";
	cin >> n;
	for (int i = 1; i <= n; ++i)
	{
		int DATA;
		cout << "Inpute Data: ";
		cin >> DATA;
		insert_head(head, DATA);
	}
}

int main()
{
	NODE *head = nullptr;
	////Input(head);
	////srand(time(NULL));
	//for (int k = 1; k <= 10; k++)
	//{
	//	int temp = rand() % 25;	
	//	insert_grouped_increasing(head, temp);
	//}
	//Output(head);
	if (head == nullptr || head->getLink() == nullptr)
		cout << "ahihihi\n";
	system("pause");
	return 0;
}