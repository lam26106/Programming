#include <iostream>
#include <string>
using namespace std;

struct Student
{
	string name;
	int math, physics, english;
};
istream& operator >>  (istream&is, Student& s)
{
	cout << "Input name of the student: ";
	is >> s.name;
	cout << "Input the mark for each course: ";
	is >> s.math >> s.physics >> s.english;
	return is;
}
ostream& operator << (ostream& os, Student& s)
{
	os << "The student name is " << s.name << endl;
	os << "The students marks for math, physics and english course are: " << s.math << " " << s.physics << " " << s.english;
	return os;
}
struct NODE
{
	Student stu;
	NODE *link;
};
struct List
{
	NODE *pHead, *pTail;
};

void Init(List &l)
{
	l.pHead = l.pTail = NULL;
}

NODE* getNode(Student s)
{
	NODE * temp = new NODE;
	temp->stu = s;
	temp->link = NULL;
	return temp;
}

void addHead(List &l, Student s)
{
	NODE *temp = getNode(s);
	if (l.pHead == NULL)
		l.pHead = l.pTail = temp; 
	else
	{
		temp->link = l.pHead;
		l.pHead = temp;
	}
}
void addTail(List &l, Student s)
{
	NODE *temp = getNode(s);
	if (l.pHead == NULL)
		l.pHead = l.pTail = temp;
	else
	{
		l.pTail->link = temp;
		l.pTail = temp;
	}
}

void INPUT(List &l)
{
	int numOfStudent;
	cout << "Input the number of students: ";
	cin >> numOfStudent;
	for (int i = 1; i <= numOfStudent; ++i)
	{
		Student s;
		cin >> s;
		addTail(l, s);
	}
}
void OUTPUT(const List & l)
{
	for (NODE *temp = l.pHead; temp != NULL; temp = temp->link)
	{
		cout << temp->stu << endl;
	}
}


int main()
{
	List l;
	Init(l);
	INPUT(l);
	OUTPUT(l);
	system("pause");
	return 0;
}