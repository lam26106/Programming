﻿#include <iostream>
#include <string>
using namespace std;

// 7 buoc can ban thao tac voi linked list
//B1: Khai bao cau truc du lieu
struct NODE
{
	int Data;
	struct NODE *pNext;
};
struct List
{
	NODE *pHead, *pTail;
};
//B2: Khoi tao danh sach lien ket
void Init(List &l)
{
	l.pHead = l.pTail = NULL;
}
//B3: Tao node trong DSLK
NODE* GetNode(int DATA)
{
	NODE *p = new NODE;
	if (p == NULL)
	{
		cout << "Not enough memory";
		system("pause");
		exit(0);
	}
	p->Data = DATA;
	p->pNext = NULL;
	return p;
}
//B4: Thêm đầu thêm cuối
void AddHead(List &l, NODE *p)
{
	if (l.pHead == NULL)
		l.pHead = l.pTail = p;
	else
	{
		p->pNext = l.pHead;
		l.pHead = p;
	}
}
void AddTail(List &l, NODE *p)
{
	if (l.pHead == NULL)
	{
		l.pHead = l.pTail = p;
	}
	else
	{
		l.pTail->pNext = p;
		l.pTail = p;
	}
}

//B5:Input/Output
void Input(List &l)
{
	Init(l);
	int n;
	cout << "Input the number of element";
	cin >> n;
	for (int i = 1; i <= n; ++i)
	{
		int data;
		cout << "Input data: ";
		cin >> data;
		NODE *p;
		p = GetNode(data);
		AddTail(l, p);
	}
}
void Output(List l)
{
	for (NODE *p = l.pHead; p != NULL; p = p->pNext)
	{
		cout << p->Data << " ";
	}
}

//B6:Xu ly nhung yeu cau can thiet
void suff_add(List &l, int DATA, NODE *p) // Suppose we already have access to the NOPE q in the list
{
	NODE *x = GetNode(DATA);
	x->pNext = p->pNext;
	p->pNext = x;
}
void pre_add(List &l, int DATA, NODE *p)
{
	if (p == l.pHead)
	{
		NODE *x = GetNode(DATA);
		AddHead(l, x);
		return;
	}
	for (NODE*temp = l.pHead; temp != NULL; temp = temp->pNext)
	{
		//cout << "y" << endl;
		if (temp->pNext->Data == p->Data)
		{
			suff_add(l, DATA, temp);
			p = temp;
			return;
		}
	}
}
// Add NODE before every element which has data as even number
void multiple_pre_add(List &l, int DATA)
{
	NODE *temp1 = l.pHead;
	if (l.pHead->Data % 2 == 0)
	{
		NODE *x = GetNode(DATA);
		AddHead(l, x);
		temp1 = l.pHead->pNext;
	}

	for (NODE*temp = temp1; temp->pNext != NULL; temp = temp->pNext)
	{
		if (temp->pNext->Data % 2 == 0)
		{
			suff_add(l, DATA, temp);
			temp = temp->pNext;
		}
	}
}
//B7: Giai phong con tro
void Delete(List &l)
{
	NODE *p;
	while (l.pHead != NULL)
	{
		p = l.pHead;
		l.pHead = p->pNext;
		delete p;
	}
}

//Xoa Node dau tien
void delete_head(List &l)
{
	if (l.pHead != NULL){
		NODE *temp = l.pHead;
		l.pHead = l.pHead->pNext;
		delete temp;
	}
}
//Xoa Node sau 1 Node
void deleteANode(List &l, NODE *p) // Xoa not p
{
	if (p->Data == l.pHead->Data)
	{
		delete_head(l);
	}
	for (NODE *temp = l.pHead; temp->pNext != NULL;)
	{
		if (temp->pNext->Data == p->Data)
		{
			NODE *k = temp->pNext;
			temp->pNext = k->pNext;
			delete k;
		}
		else
		{
			temp = temp->pNext;
		}
	}
}
int main()
{
	string t = "ahihi";
	int k = string::npos;
	cout << k;
	system("pause");
	return 0;
}