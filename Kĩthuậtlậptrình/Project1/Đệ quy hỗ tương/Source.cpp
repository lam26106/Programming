#include <iostream>
using namespace std;


//de quy ho tuong
// x(0) = 1, y(0) = 0
// x(n) = x(n-1) + y (n-1)
// y(n) = 3 * x(n-1) + 2 * y(n-1)
int x_n(int);
int y_n(int);

int x_n(int n)
{
	if (n == 0)
		return 1;
	return x_n(n - 1) + y_n(n - 1);
}
int y_n(int n)
{
	if (n == 0)
		return 0;
	return 3 * x_n(n - 1) + 2 * y_n(n - 1);
}
int main()
{
	cout << x_n(1);
	//cout << y_n(0);
	system("pause");
	return 0;
}