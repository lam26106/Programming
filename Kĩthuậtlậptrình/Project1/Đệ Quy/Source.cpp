#include <iostream>
using namespace std;
//S = 1 + 2 + 3 + ... + n
//de quy thuong
int sum(int n)
{
	if (n == 1)
		return 1;
	else{
		int c = sum(n - 1) + n;
		return c;
		//return sum(n-1) + n;
	}
}
//de quy duoi
int sum2(int n, int x)
{
	if (n == 1)
		return n + x;
	return sum2(n - 1, n + x);
}

//De quy ap dung cho day fibonacci
int fibo(int n)
{
	if (n == 0 || n == 1)
		return 1; 
	return fibo(n - 1) + fibo(n - 2);
}
int main()
{
	cout << sum(10);
	system("pause");
	return 0;
}