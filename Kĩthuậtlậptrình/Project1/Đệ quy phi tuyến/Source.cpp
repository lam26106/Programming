#include <iostream>
using namespace std;


//de quy phi tuyen
// base: x(0) = 1;
// cong thuc: x(n) = n ^ 2 * x(0) + (n-1)*2*x(1) + (n-2)*2*x(2) + ... + 2^2*x(n-2) + 1^2*x(n - 1);

int abc(int n)
{
	
	if (n == 0)
		return 1;
	int sum = 0;
	for (int i = 1; i <= n; ++i)
	{
		sum = sum + i * i * abc(n - i);
		//return sum;
	}
	return sum;
}
int main()
{
	cout << abc(5);
	system("pause");
	return 0;
}