
#pragma once
#include <iostream>
#include <vector>
#include <string>
#include <fstream>
using namespace std;

struct MaSo
{
	char m_Code[10];
};
typedef struct MaSo MASO;
struct VanDongVien
{
	int vitrixp;
	int khoihanh;
	MASO ms;
	int m_Speed;
	int m_Equipment;
	float thanhTich;
};
typedef struct VanDongVien VDV;

void NhapThongTin(VDV &vdv)
{
	//	cout << "\nNhap vao toc do cua van dong vien: ";
	//	cin >> vdv.m_Speed;
	//	do{
	//		cout << "\nNeu vdv co trang bi thi nhap vao 1, neu khong trang bi nhap vao 0";
	//		cout << "\nVan dong vien co trang bi hay khong: ";
	//		cin >> vdv.m_Equipment;
	//	} while (vdv.m_Equipment != 1 && vdv.m_Equipment != 0);
	ifstream FileIn("INPUT.txt");
	FileIn >> vdv.ms.m_Code;
	FileIn >> vdv.m_Speed >> vdv.m_Equipment;
	FileIn.close();

}
void XuatThongTin(VDV vdv)
{
	cout << vdv.ms.m_Code << " " << vdv.m_Speed << " " << vdv.m_Equipment << endl;
}

int main()
{
	VDV vdv;
	NhapThongTin(vdv);
	XuatThongTin(vdv);
	system("pause");
	return 0;
}
