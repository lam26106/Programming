#pragma once
#include <iostream>
#include <vector>
#include <string>
#include <fstream>
using namespace std;

struct MaSo
{
	char m_Code[10];
};
typedef struct MaSo MASO;
struct VanDongVien
{
	int vitrixp;
	int khoihanh;
	MASO ms;
	int m_Speed;
	int m_Equipment;
	float thanhTich;
};
typedef struct VanDongVien VDV;
void NhapThongTin(VDV &, ifstream&);
void XuatThongTin(ofstream&, VDV);