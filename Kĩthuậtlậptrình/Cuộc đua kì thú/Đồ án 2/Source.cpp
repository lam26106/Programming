#include "DanhSachVDV.h"
#pragma warning (disable:4996)


void UpdateSpeed(DANHSACH &ds)
{
	for (int i = 0; i < ds.soVDV; ++i)
	{
		if (ds.arr[i].m_Equipment == 1)
		{
			if (ds.arr[i].ms.m_Code[0] == 'L')
			{
				ds.arr[i].m_Speed *= 2;
			}
			else if (ds.arr[i].ms.m_Code[0] == 'R')
			{
				ds.arr[i].m_Speed += 30;
			}
			else
				ds.arr[i].m_Speed *= 5;
		}
	}
}
float TinhThoiGianVeDich(DANHSACH &ds, int i)
{
	int vitridich = 210;
	float time;
	time =( (vitridich - ds.arr[i].vitrixp) * 1.0) / ds.arr[i].m_Speed ;
	return time;
}
void HoanViVDV(VDV &vdv1, VDV &vdv2)
{
	VDV temp = vdv1;
	vdv1 = vdv2;
	vdv2 = temp;
}
void SapXepThanhTich(DANHSACH ds)
{
	for (int i = 0; i < ds.soVDV - 1; ++i)
	{
		for (int j = i + 1; j < ds.soVDV; ++j)
		{
			if (ds.arr[i].thanhTich > ds.arr[j].thanhTich)
			{
				HoanViVDV(ds.arr[i], ds.arr[j]);
			}
		}
	}
	for (int i = 0; i < ds.soVDV - 1; ++i)
	{
		if (ds.arr[i].thanhTich == ds.arr[i + 1].thanhTich)
		{
			if (strcmp(ds.arr[i].ms.m_Code, ds.arr[i + 1].ms.m_Code) == -1)
				HoanViVDV(ds.arr[i], ds.arr[i + 1]);
		}
	}
}

int main()
{
	ifstream FileIn("INPUT.txt");
	ofstream FileOut("OUTPUT.txt");
	if (!FileIn)
	{
		cout << "Khong tim thay tap tin";
		system("pause");
		return 0;
	}
	DANHSACH ds;
	NhapDanhSachVDV(FileIn, ds);
	UpdateSpeed(ds);
	for (int i = 0; i < ds.soVDV; ++i)
		ds.arr[i].thanhTich = TinhThoiGianVeDich(ds, i);
	
	SapXepThanhTich(ds);
	//cout << "\nBang thanh tich sap xep theo tong thoi gian tu be den lon la: " << endl;
	for (int i = 0; i < ds.soVDV; ++i)
	{
		if (ds.arr[i].thanhTich <= (12 - ds.arr[i].khoihanh))
		{
			XuatThongTin(FileOut, ds.arr[i]);
		}
	}

	//HoanViVDV(ds.arr[0], ds.arr[1]);
	FileIn.close();
	FileOut.close();

	system("pause");
	return 0;
}