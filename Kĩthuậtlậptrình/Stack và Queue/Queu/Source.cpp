#include <iostream>
using namespace std;

struct NODE
{
	int data;
	NODE *link;
};
struct QUEUE
{
	NODE *head, *tail;
};
void init(QUEUE &s)
{
	s.head = s.tail = nullptr;
}
bool isEmpty(QUEUE &s)
{
	if (s.head == nullptr)
		return true;
	return false;
}
void enqueue(QUEUE &s, const int &DATA)
{
	NODE*temp = new NODE;
	temp->data = DATA;
	temp->link = nullptr;
	if (isEmpty(s) == true)
	{
		s.head = s.tail = temp;
		return;
	}
	s.tail->link = temp;
	s.tail = temp;
}
void dequeue(QUEUE &s)
{
	if (isEmpty(s))
		return;
	NODE*temp = s.head;
	s.head = s.head->link;
	delete temp;
}
int main()
{
	system("pause");
	return 0;
}