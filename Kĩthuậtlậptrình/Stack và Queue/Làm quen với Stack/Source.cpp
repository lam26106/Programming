﻿#include <iostream>
using namespace std;

class RuntimeException {
private:
	string errorMsg;
public:
	RuntimeException(const string& err)
	{
		errorMsg = err;
	}
	string getMessage() const
	{
		return errorMsg;
	}
};
class StackEmpty : public RuntimeException
{
public:
	StackEmpty(const string& err) : RuntimeException(err) {}
};

template<class T>
class Node
{
private:
	Node<T> *link;
	T data;
public:
	Node();
	Node<T>* getLink() const
	{
		return link;
	}
	T getData() const
	{
		return data;
	}
	void setLink(Node<T> *temp)
	{
		link = temp;
	}
	void setData(const T& DATA)
	{
		data = DATA;
	}
	
};
template<class T>
Node<T>::Node() : link (nullptr)
{}
template<class T>
class Stack
{
public:
	Stack();
	int size() const;
	bool empty() const;
	const T& top() const;
	void push(const T& temp);
	void pop() throw(StackEmpty);
private:
	Node<T> *head;
};
template<class T>
Stack<T>::Stack()
{
	head = nullptr;
}
template<class T>
bool Stack<T>::empty() const
{
	return head == nullptr;
}
template<class T>
const T& Stack<T>::top() const 
{
	if (this->empty() == false)
	return *head;
}
template<class T>
int Stack<T>::size() const
{
	Node<T> *temp = head;
	int count = 0;
	while (temp->getLink() != nullptr)
		count++;
	return count;
}
template<class T>
void Stack<T>::push(const T& temp)
{
	Node<T> *tmp = new Node<T>();
	tmp->setData(temp);
	tmp->setLink(head->getLink());
	head = tmp;
}
template<class T>
void Stack<T>::pop() throw(StackEmpty)
{
	if (this->empty() == true){
		throw StackEmpty("Stack empty");
	}
	Node<T>* temp = head;
	head = head->getLink();
	delete temp;
		
}

int main()
{
	
	system("pause");
	return 0;
}