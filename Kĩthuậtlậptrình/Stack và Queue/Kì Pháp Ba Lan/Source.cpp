#include <iostream>
#include <string>
#include <fstream>
#include <cstring>
#pragma warning (disable:4996)
using namespace std;

struct NODE
{
	string data;
	NODE* pNext;
};
struct STACK
{
	NODE*top;
};
void init(STACK &s)
{
	s.top = nullptr;
}
NODE* getNode(string temp)
{
	NODE*p = new NODE();
	p->data = temp;
	p->pNext = nullptr;
	return p;
}
//Kiem tra stack rong
bool isEmpty(STACK s)
{
	if (s.top == nullptr)
		return true;
	return false;
}
void push(STACK &s, const string &temp)
{
	NODE*p = getNode(temp);
	if (s.top == nullptr)
	{
		s.top = p;
		return;
	}
	p->pNext = s.top;
	s.top = p;
}
void pop(STACK &s)
{
	if (isEmpty(s) == true)
		return;
	NODE *p = s.top;
	s.top = s.top->pNext;
	delete p;
}
bool peek(const STACK&s, string &temp)
{
	if (isEmpty(s) == true)
		return false;
	temp = s.top->data;
	return true;
}
int myAtoi(string s)
{
	int temp = 0;
	int i;
	i = s[0] == '-' ? 1 : 0;
	for (; i < s.length(); ++i)
	{
		temp = temp * 10 + (s[i] - 48);
	}
	if (s[0] == '-')
		temp *= -1;
	return temp;
}
void Input(STACK &s)
{
	ifstream fin("Input.txt");
	init(s);
	string p1, p2, p3;
	int result;
	while (!fin.eof())
	{
		char c;
		fin >> c;
		if (c != '(' && c!= ')' && c!= ' ')
		{
			fin.seekg(-1, SEEK_CUR);
			string temp;
			fin >> temp;
			push(s, temp);
			//cout << temp << endl;
		}
		else if (c == ')')
		{
			bool check1 = peek(s, p1);
			pop(s);
			bool check2 = peek(s, p2);
			pop(s);
			bool check3 = peek(s, p3);
			pop(s);
			if (check1 && check2 && check3){
				int num1 = atoi(p1.c_str()), num3 = atoi(p3.c_str());
				if (p2[0] == '+')
					result = num3 + num1;
				else if (p2[0] == '-')
					result = num3 - num1;
				else if (p2[0] == '*')
					result = num3 * num1;
				else
					result = (double)num3 / num1;
				char ch[20];
				itoa(result, ch, 10);
				push(s, (string)ch);
			}

		}
	}
	cout << result;

}
void Output(STACK s)
{
	while (isEmpty(s) == false)
	{ 
		string temp;
		peek(s, temp);
		cout << temp << endl;
		pop(s);
	}
}

int main()
{
	//STACK s;
	//Input(s);
	//Output(s);
	string s = "-32";
	int a = myAtoi(s);
	cout << a;
	system("pause");
	return 0;
}