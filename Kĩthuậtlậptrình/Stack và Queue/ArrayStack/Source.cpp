#include <iostream>
#include <string>
using namespace std;


class RunTimeException
{
private:
	string errMsg;
public:
	RunTimeException(const string& mess)
	{
		errMsg = mess;
	}
	string getErrMsg() const
	{
		return errMsg;
	}

};
class StackEmpty : public RunTimeException
{
public:
	StackEmpty(const string& err) : RunTimeException(err){}
};
class StackFull : public RunTimeException
{
public:
	StackFull(const string& err) : RunTimeException(err){}
};
template<class T>
class ArrayStack
{
	enum{ DEF_CAPACITY = 100 };
private:
	T* arrayS;
	int capacity;
	int t;
public:
	ArrayStack(int cap = DEF_CAPACITY);
	int size() const;
	bool empty() const;
	const T& top() const throw(StackEmpty);
	void push(const T& t) throw(StackFull);
	void pop() throw(StackEmpty);
};

template<class T>
ArrayStack<T>::ArrayStack(int cap)
{
	capacity = cap;
	t = -1;
	arrayS = new T[cap];
}
template<class T>
int ArrayStack<T>::size() const
{
	return t + 1;
}
template<class T>
bool ArrayStack<T>::empty() const
{
	return t + 1 == 0;
}
template<class T>
const T& ArrayStack<T>::top() const throw(StackEmpty)
{
	if (this->empty())
		throw StackEmpty("Stack Empty");
	else
	{
		return arrayS[t];
	}
}
template<class T>
void ArrayStack<T>::push(const T& temp) throw(StackFull)
{
	if (t + 1 == capacity)
		throw StackFull("Stack full");
	else
	{
		arrayS[t + 1] = temp;
		t += 1;
	}
}
template<class T>
void ArrayStack<T>::pop() throw(StackEmpty)
{
	if (t == -1)
		throw StackEmpty("Stack empty");
	else
		--t;
}
int main()
{
	ArrayStack<int> A; // A = [], size = 0 
	A.pop();
	
	

	system("pause");
	return 0;
}