#include<iostream>
#include<deque>
using namespace std;

class RunTimeException
{
private:
	string errMsg;
public:
	RunTimeException(const string& mess)
	{
		errMsg = mess;
	}
	string getErrMsg() const
	{
		return errMsg;
	}

};
class QueuEmpty : public RunTimeException
{
public:
	QueuEmpty(const string& err) : RunTimeException(err){}
};
class QueuFull : public RunTimeException
{
public:
	QueuFull(const string& err) : RunTimeException(err){}
};
template<class T>
class ArrayQueu
{
	enum{ DEF_CAPCITY = 100 };
private:
	T* arrayQ;
	int capacity;
	int numOfelements;
	int Front, Rear;
public:
	ArrayQueu(int cap = DEF_CAPCITY)
	{
		capacity = cap;
		arrayQ = new T[capacity];
		numOfelements = Front = Rear = 0;
	}
	int size()
	{
		return numOfelements;
	}
	bool Empty()
	{
		return numOfelements == 0;
	}
	void dequeu() throw(QueuEmpty)
	{
		if (Empty())
			throw QueuEmpty("Queu is empty");
		Front = (Front + 1) / capacity;
	}
	void enqueu(const T& elem) throw(QueuFull)
	{
		if (size() == capacity)
			throw QueuFull("Queu is full");
		ArrayQueu[Rear] = elem;
		Rear = (Rear + 1) / capacity;
	}
	const T& front() throw(QueuEmpty)
	{
		if (Empty())
			throw QueuEmpty("Queu is empty");
		//return ArrayQueu[Front];
	}
};
int main()
{
	deque<int> a;
	a.push_back(3);
	a.push_back(4);
	cout << a.front();
	system("pause");
	return 0;
}