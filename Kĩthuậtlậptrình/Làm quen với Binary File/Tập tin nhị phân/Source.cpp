#include <iostream>
#pragma warning (disable:4996)
using namespace std;

void NhapMang(int *a, int n)
{
	for (int i = 0; i < n; ++i)
	{
		cout << "Nhap vao a[" << i << "]: ";
		cin >> a[i];
	}
}
void XuatMang(int *a, int n)
{
	for (int i = 0; i < n; ++i)
	{
		cout << a[i] << " ";
	}
}
void ghiFile(FILE *&FileOut, int *a, int n)
{
	FileOut = fopen("TapTin.PPT", "wb");

	if (FileOut != NULL)
	{
		fwrite(a, sizeof(int), n, FileOut);
	}

	fclose(FileOut);
}
void docFile(FILE *&FileIn, int *&a, int &n)
{
	FileIn = fopen("TapTin.PPT", "rb");
	if (!FileIn)
	{
		cout << "Khong tim thay tap tin";
		system("pause");
		exit(0);
	}

}
int main()
{
	int n = 5;
	int *arr = new int[n];
	NhapMang(arr, n);
	FILE *FileOut;
	ghiFile(FileOut, arr, n);
	free(arr);
	system("pause");
	return 0;
}