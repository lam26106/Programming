#include <iostream>
using namespace std;

class A
{
//public:
//	int a;
//	int *arr;
public:
	A()
	{
	
	}

	~A()
	{
		cout << "hello\n";
		//delete arr;
	}
	virtual void display() { cout << "A display\n"; }
};
class B : public A
{
public:
	int *arr;
public:
	B() 
	{
		arr = new int(3);
	}
	//void print()
	//{
	//	cout << this->a;
	//}
	~B()
	{
		delete[] arr;
		cout << "hi\n";
	}
};
int main() // memory cleanup (deleting) is omitted in the following code
{
	//A *p = new B();
	//B *k = dynamic_cast<B*> (p);
	//B *k = new B();
	B k;
	//k.~B();
	//A *p = new A[2];
	//p->~A();
	//delete p;
	//cin.get();
	//p.~A();
	//delete p;
	k.~B();
	//k->~B();
	//delete k->arr;
	//delete k;
	//cout << k->arr[0];
	system("pause");
	return 0;
}
