#include <iostream>
using namespace std;

class PFArrayD // representing partially filled array of doubles
{
public:
	PFArrayD(); //Initializes with a capacity of 50.
	PFArrayD(int capacityValue);
	PFArrayD(const PFArrayD& pfaObject);
	PFArrayD& operator = (const PFArrayD& rightSide);
	~PFArrayD();
	void addElement(double element);
	//Precondition: The array is not full;
	//Postcondition: The element has been added.
	bool full() const; //Returns true if the array is full, false otherwise.
	int getCapacity() const;
	int getNumberUsed() const;
	void emptyArray();
	//Resets the number used to zero, effectively emptying the array.
	double& operator[](int index);
protected:
	double* arr; //for an array of doubles.
	int capacity; //for the size of the array.
	int used; //for the number of array positions currently in use.
};
PFArrayD::PFArrayD()
{
	arr = new double[50];
	capacity = 50;
}
PFArrayD::PFArrayD(int capacityValue)
{
	if (capacityValue > 0)
	{
		capacity = capacityValue;
		arr = new double[capacity];
	}
}
PFArrayD::PFArrayD(const PFArrayD& pfaObject)
{
	if (capacity > 0)
		delete arr;
	arr = pfaObject.arr;
	capacity = pfaObject.getCapacity();
	used = pfaObject.getNumberUsed();
}
PFArrayD& PFArrayD::operator = (const PFArrayD& rightSide)
{
	if (this == &rightSide)
		return *this;
	if (capacity > 0)
		delete[] arr;
	arr = rightSide.arr;
	capacity = rightSide.getCapacity();
	used = rightSide.getNumberUsed();
	return *this;
}
PFArrayD::~PFArrayD()
{
	if (capacity > 0)
		delete[] arr;
	capacity = 0;
	used = 0;
}
//Precondition: The array is not full;
//Postcondition: The element has been added.
void PFArrayD::addElement(double element)
{
	if (capacity > 0 && used < capacity)
	{
		arr[used + 1] = element;
		used++;
	}
}
// Returns true if the array is full, false otherwise.
bool PFArrayD::full() const
{
	if (used == capacity && capacity > 0)
		return true;
	return false;
}
int PFArrayD::getCapacity() const
{
	return capacity;
}
int PFArrayD::getNumberUsed() const
{
	return used;
}
//Resets the number used to zero, effectively emptying the array.
void PFArrayD::emptyArray()
{
	if (capacity > 0){
		delete[] arr;
		arr = new double[capacity];
		used = 0;
	}
}

double& PFArrayD::operator[](int index)
{
	if (index >= 0)
		return arr[index];
	else
	{
		cout << "Memory access invalid";
		system("pause");
		exit(0);
	}
}
// The following class PFArrayDBak is derived from PFArrayD. It has all the member
// functions of the PFArrayD and can be used just like an object of the class PFArrayD.
// But it has a member function called backup that can make a backup copy of all the
// data in the partially filled array; at any later time the programmer can use the
// member function restore to restore the partially filled array to the state it was
// in just before the last invocation of backup function.
// Please refer to the testing code for more details.
class PFArrayDBak : public PFArrayD
{
public:
	PFArrayDBak(); //Initializes with a capacity of 50.
	PFArrayDBak(int capacityValue);
	PFArrayDBak(const PFArrayDBak& Object);
	PFArrayDBak& operator = (const PFArrayDBak& rightSide);
	~PFArrayDBak();
	void backup(); //Makes a backup copy of the partially filled array.
	void restore();
	//Restores the partially filled array to the last saved version.
	//If backup has never been invoked, this empties the partially filled array.
private:
	double* arr_B; //for a backup of main array.
	int usedB; //backup for inherited member variable used.
};
PFArrayDBak::PFArrayDBak()
{
	capacity = 50;
}
//Initializes with a capacity of 50.
PFArrayDBak::PFArrayDBak(int capacityValue)
{
	if (capacityValue > 0)
	{
		PFArrayD temp(capacity);
		this->PFArrayD::operator = (temp);
		arr_B = new double[capacity];
		for (int i = 0; i < used; ++i)
			arr_B[i] = this->arr[i];
		usedB = used;
	}
}
PFArrayDBak::PFArrayDBak(const PFArrayDBak& Object)
{
	if (capacity > 0)
		delete[] arr_B;
	this->PFArrayD::operator=(Object);
	arr_B = new double[capacity];
	for (int i = 0; i < used; ++i)
		arr_B[i] = Object.arr[i];
	usedB = used;
}
PFArrayDBak& PFArrayDBak::operator = (const PFArrayDBak& rightSide)
{
	if (this == &rightSide)
		return *this;
	if (capacity > 0)
		delete[] arr_B;
	this->PFArrayD::operator=((PFArrayD)rightSide);
	for (int i = 0; i < used; ++i)
		arr_B[i] = rightSide.arr_B[i];
	usedB = rightSide.usedB;
	return *this;
}
PFArrayDBak::~PFArrayDBak()
{
	if (capacity > 0)
	{
		delete[] arr;
		delete[] arr_B;
		capacity = 0;
		used = usedB = 0;
	}
}
//Makes a backup copy of the partially filled array.
void PFArrayDBak::backup()
{
	if (capacity > 0 && used > 0)
	{
		for (int i = 0; i < used; ++i)
			arr_B[i] = arr[i];
		usedB = used;
	}
}
//Restores the partially filled array to the last saved version.
//If backup has never been invoked, this empties the partially filled array.
void PFArrayDBak::restore()
{

}


int main()
{
	system("pause");
	return 0;
}