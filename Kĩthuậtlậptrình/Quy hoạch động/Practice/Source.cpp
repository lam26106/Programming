#include <iostream>
#include <vector>
using namespace std;

int main()
{
	int a[13] = { INT_MIN, 1, 2, 3, 8, 9, 4, 5, 6, 20, 9, 10, INT_MAX };
	vector<int> L(13);
	vector<int> T(13);
	L[12] = 1;
	int i = 11, jmax;
	while (i >= 0){
		jmax = 12;
		int j = i + 1;
		while (j < 13){
			if (a[j] > a[i] && L[j] > L[jmax])
				jmax = j;
			j++;
		}
		L[i] = L[jmax] + 1;
		T[i] = jmax;
		i--;
	}

	cout << L[0] - 2 << endl;
	int j = T[0];
	while (j < 12){
		cout << a[j] << endl;
		j = T[j];
	}
	system("pause"); 
	return 0; 
}