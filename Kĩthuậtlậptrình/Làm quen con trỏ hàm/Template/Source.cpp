#include <iostream>
using namespace std;

template <class kutycoi>
void HoanVi(kutycoi &a, kutycoi &b)
{
	kutycoi temp = a;
	a = b;
	b = temp;
}
int main()
{
	int a = 3, b = 4;
	float c = 3.0, d = 4.5;
	string e = "Lam", k = "kutycoi";
	HoanVi<int>(a, b); // cach chuan
	// HoanVi(a, b); // Cach nhanh
	HoanVi<float>(c, d);
	HoanVi<string>(e, k);
	system("pause");
	return 0;
}