#include <iostream>
using namespace std;

void HamA()
{
	cout << "Day la ham A";
}
void HamB()
{
	cout << "Day la ham B";
}
int Tong(int a, int b)
{
	return a + b;
}
int Hieu(int a, int b)
{
	return a - b;
}

//Tra ve mot con tro
int(*TinhToan(char c))(int, int)
{
	if (c == '+')
		return Tong;
	if (c == '-')
		return Hieu;
}
int main()
{
	//void(*ptr)();
	//ptr = HamA;
	////ptr = &HamA;
	//ptr();

	//int(*ptr)(int, int);
	//ptr = Tong;
	//cout << ptr(3, 4);

	int(*ptr)(int, int);
	ptr = TinhToan('+');
	int kq = ptr(3, 4);
	cout << kq;
	system("pause");
	return 0;
}