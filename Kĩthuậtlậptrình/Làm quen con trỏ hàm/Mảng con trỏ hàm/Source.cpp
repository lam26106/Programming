#include <iostream>
using namespace std;

void NhapMang(int *a, int n)
{
	for (int i = 0; i < n; ++i)
	{
		cout << "\nNhao vao phan tu thu " << i << ": ";
		cin >> a[i];
	}
}
void XuatMang(int *a, int n)
{
	for (int i = 0; i < n; ++i)
	{
		cout << a[i];
	}
}

//Ung dung con tro ham:
bool SoSanhLon(int a, int b)
{
	return a > b;
}
bool SoSanhNho(int a, int b)
{
	return a < b;
}void SapXep(int *a, int n, bool(*SoSanh)(int, int))
{
	for (int i = 0; i < n - 1; ++i)
	{
		for (int j = i + 1; j < n; ++j)
		{
			if (SoSanh(a[i], a[j]) == true)
			{
				int temp = a[j];
				a[j] = a[i];
				a[i] = temp;
			}
		}
	}
}

int TimPhanTuMInOrMax(int *a, int n, bool(*SoSanh)(int, int))
{
	int result = a[0];
	for (int i = 1; i < n; ++i)
	{
		if (SoSanh(a[i], result) == true)
		{
			result = a[i];
		}
	}
	return result;
}
int main()
{
	int n = 5;
	int *a = new int[n];
	//tao mot mang con tro ham de luu cac ham Nhap vaf Xuat
	void(*pointer[2])(int*, int);
	pointer[0] = NhapMang;
	pointer[1] = XuatMang;
	for (int i = 0; i < 2; ++i)
		pointer[i](a, n);
	system("pause");
	return 0;
}