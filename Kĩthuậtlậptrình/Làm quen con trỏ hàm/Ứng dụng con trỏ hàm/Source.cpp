#include <iostream>
using namespace std;

void NhapMang(int *a, int n)
{
	for (int i = 0; i < n; ++i)
	{
		cout << "\nNhao vao phan tu thu " << i << ": ";
		cin >> a[i];
	}
}
void XuatMang(int *a, int n)
{
	for (int i = 0; i < n; ++i)
	{
		cout << a[i];
	}
}
void SapXemTang(int *a, int n)
{
	for (int i = 0; i < n - 1; ++i)
	{
		for (int j = i + 1; j < n; ++j)
		{
			if (a[j] < a[i])
			{
				int temp = a[j];
				a[j] = a[i];
				a[i] = temp;
			}
		}
	}
}
void SapXemGiam(int *a, int n)
{
	for (int i = 0; i < n - 1; ++i)
	{
		for (int j = i + 1; j < n; ++j)
		{
			if (a[j] > a[i])
			{
				int temp = a[j];
				a[j] = a[i];
				a[i] = temp;
			}
		}
	}
}
//Ung dung con tro ham:
bool SoSanhLon(int a, int b)
{
	return a > b;
}
bool SoSanhNho(int a, int b)
{
	return a < b;
}void SapXep(int *a, int n, bool(*SoSanh)(int, int))
{
	for (int i = 0; i < n - 1; ++i)
	{
		for (int j = i + 1; j < n; ++j)
		{
			if (SoSanh(a[i], a[j]) == true)
			{
				int temp = a[j];
				a[j] = a[i];
				a[i] = temp;
			}
		}
	}
}

int TimPhanTuMInOrMax(int *a, int n, bool (*SoSanh)(int, int))
{
	int result = a[0];
	for (int i = 1; i < n; ++i)
	{
		if (SoSanh(a[i], result) == true)
		{
			result = a[i];
		}
	}
	return result;
}
int TimMinMax2So(int a, int b, bool (*ptr)(int , int ))
{
	if (ptr(a, b) == true)
		return a;
	return b;
}
bool LonHon(int a, int b)
{
	return a > b;
}
bool NhoHon(int a, int b)
{
	return a < b;
}
int main()
{
	int n = 5;
	int *a = new int[n];
	NhapMang(a, n);
	//SapXep(a, n, SoSanhLon);
	////SapXep(a, n, SoSanhNho)
	//XuatMang(a, n);

	int Max = TimPhanTuMInOrMax(a, n, SoSanhLon);
	cout << Max << "\n";
	int Min = TimPhanTuMInOrMax(a, n, SoSanhNho);
	cout << Min;
	system("pause");
	return 0;
}