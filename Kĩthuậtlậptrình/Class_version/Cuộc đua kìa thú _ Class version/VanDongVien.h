#pragma once
#include <iostream>
#include <vector>
#include <string>
#include <fstream>
const int DICH = 210;
using namespace std;


class VanDongVien
{
private:
	float thanhtich;
	int khoihanh;
	int xuatphat;
	char m_code[30];
	int m_speed;
	int m_equipment;

public:
	VanDongVien();
	~VanDongVien();
	//getter
	float get_thanhtich();
	int get_khoihanh();
	int get_xuatphat();
	char* get_code();
	int get_speed();
	int get_equipment();

	//setter
	void set_thanhtich(float);
	void set_khoihanh(int);
	void set_xuatphat(int);
	void set_code(char*);
	void set_speed(int);
	void set_equipment(int);

	//input
	friend istream& operator >> (istream&, VanDongVien &);
	//output
	friend ostream& operator << (ostream&, VanDongVien); 

	void KhoiHanhVaXuatPhat();
	void updateSpeed();
	void tinhThanhTich();
};

