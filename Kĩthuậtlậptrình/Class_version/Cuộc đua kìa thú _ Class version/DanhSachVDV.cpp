#include "DanhSachVDV.h"


DanhSachVDV::DanhSachVDV()
{
}


DanhSachVDV::~DanhSachVDV()
{
}
//getter
int DanhSachVDV::get_soVDV()
{
	return soVDV;
}
VanDongVien* DanhSachVDV::get_arr()
{
	//VanDongVien *a = arr;
	//return a;
	return arr;
}

//setter
void  DanhSachVDV::set_soVDV(int num)
{
	soVDV = num;
}
void  DanhSachVDV::set_arr(VanDongVien *s)
{
	arr = s;
}

//input and output
void  DanhSachVDV::NhapThongTin(istream& ins)
{
	ins >> this->soVDV;
	this->arr = new VanDongVien[soVDV];
	for (int i = 0; i < soVDV; ++i)
	{
		ins >> this->arr[i];
		arr[i].KhoiHanhVaXuatPhat();
		arr[i].updateSpeed();
		arr[i].tinhThanhTich();
	}
}
void DanhSachVDV::XuatThongTin(ostream& os)
{
	for (int i = 0; i < soVDV; ++i)
		os << this->arr[i];
}
void  DanhSachVDV::BangTongSap()
{
	int i = 1;
	while (i < soVDV)
	{
		VanDongVien temp = arr[i];
		int tmp = arr[i].get_thanhtich();
		int j = i;
		while (j >= 1 && tmp < arr[j - 1].get_thanhtich())
		{
			arr[j--] = arr[j - 1];
		}
		arr[j] = temp;
		i++;
	}
	//for (int i = 0; i < soVDV - 1; ++i)
	//{
	//	for (int j = i + 1; j < soVDV; ++j)
	//	{
	//		if (arr[j].get_thanhtich() < arr[i].get_thanhtich())
	//		{
	//			VanDongVien temp = arr[j];
	//			arr[j] = arr[i];
	//			arr[i] = temp;
	//		}
	//	}
	//}
}

