#pragma once
#include "VanDongVien.h"
class DanhSachVDV
{
private:
	int soVDV;
	VanDongVien *arr;
public:
	DanhSachVDV();
	~DanhSachVDV();

	//getter
	int get_soVDV();
	VanDongVien* get_arr();

	//setter
	void set_soVDV(int);
	void set_arr(VanDongVien*);

	//input and output
	void NhapThongTin(istream&);
	void XuatThongTin(ostream&);
	void BangTongSap();

	
};

