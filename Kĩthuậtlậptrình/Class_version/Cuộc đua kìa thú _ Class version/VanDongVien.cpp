#include "VanDongVien.h"


VanDongVien::VanDongVien()
{}


VanDongVien::~VanDongVien()
{}

//getter
float VanDongVien::get_thanhtich()
{
	return thanhtich;
}
int VanDongVien::get_khoihanh()
{
	return khoihanh;
}
int VanDongVien::get_xuatphat()
{
	return xuatphat;
}
char* VanDongVien::get_code()
{
	return m_code;
}
int VanDongVien::get_speed()
{
	return m_speed;
}
int VanDongVien::get_equipment()
{
	return m_equipment;
}

//setter
void VanDongVien::set_thanhtich(float num)
{
	thanhtich = num;
}
void VanDongVien::set_khoihanh(int num)
{
	khoihanh = num;
}
void VanDongVien::set_xuatphat(int num)
{
	xuatphat = num;
}
void VanDongVien::set_code(char* s)
{
	int i = 0;
	while (s[i] != '\0')
		m_code[i] = s[i++];
	m_code[i] = '\0';
}
void VanDongVien::set_speed(int num)
{
	m_speed = num;
}
void VanDongVien::set_equipment(int num)
{
	m_equipment = num;
}

void VanDongVien::KhoiHanhVaXuatPhat()
{
	if (m_code[0] == 'L')
	{
		xuatphat = 0;
		khoihanh = 9;
	}
	else if (m_code[0] == 'R')
	{
		khoihanh = 8;
		xuatphat = 0;
	}
	else if (m_code[0] == 'T')
	{
		khoihanh = 7;
		xuatphat = 75;
	}
}

void VanDongVien::updateSpeed()
{
	if (m_equipment == 1)
	{
		if (m_code[0] == 'L')
		{
			m_speed *= 2;
		}
		else if (m_code[0] == 'R')
		{
			m_speed += 30;
		}
		else
			m_speed *= 5;
	}
}
void VanDongVien::tinhThanhTich()
{
	thanhtich = (float)(DICH - xuatphat) / m_speed + khoihanh;
}
//input
istream& operator >> (istream& is, VanDongVien &s)
{
	is >> s.m_code;
	is >> s.m_speed >> s.m_equipment;
	return is;
}
//output
ostream& operator << (ostream& os, VanDongVien s)
{
	os << s.m_code << " " << s.m_speed << " " << s.m_equipment << " " << s.khoihanh << " " << s.xuatphat << " " << s.thanhtich << endl;
	return os;
}

