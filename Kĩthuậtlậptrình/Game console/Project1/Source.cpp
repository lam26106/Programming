﻿#include <iostream>
#include <Windows.h>
#include <string>
#include <ctime>
#include <fstream>
#include <vector>
#include <conio.h>
#include <iomanip>
#include <mmsystem.h>
#pragma warning (disable:4996)
using namespace std;

// Hàm thay đổi kích cỡ của khung cmd.
void resizeConsole(int width, int height)
{
	HWND console = GetConsoleWindow();
	RECT r;
	GetWindowRect(console, &r);
	MoveWindow(console, r.left, r.top, width, height, TRUE);
}

// Hàm tô màu.
void textcolor(int x)
{
	HANDLE mau;
	mau = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(mau, x);
}

// Hàm dịch chuyển con trỏ đến tọa độ x, y.
void gotoxy(int x, int y)
{
	HANDLE hConsoleOutput;
	COORD Cursor_an_Pos = { x - 1, y - 1 };
	hConsoleOutput = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleCursorPosition(hConsoleOutput, Cursor_an_Pos);
}

// Hàm xóa màn hình.
void ClearScreen()
{
	HANDLE hOut;
	COORD Position;
	hOut = GetStdHandle(STD_OUTPUT_HANDLE);
	Position.X = 0;
	Position.Y = 0;
	SetConsoleCursorPosition(hOut, Position);
}
void ColorBoard()
{
	for (int i = 0; i < 200; ++i)
	{
		textcolor(i);
		cout << "i = " << i << "\n";
	}
}


char m1[40][30];
struct Player_car
{
	int x, y;
	int pt;
};
struct AI_car
{
	int x, y;
};
struct Money
{
	int x, y;
};
struct Player
{
	string id, password;
	int point;
};
void LoginFunction(const Player& pl);
void GameStart(Player_car& player, AI_car *arr, Money *a, int &highestScore, Player& pl, vector<Player> playarr);
void Intro()
{
	ifstream FileIn("Intro.txt");
	string t;
	srand(time(NULL));
	while (!FileIn.eof())
	{
		getline(FileIn, t);
		int i = rand() % 5 + 10;
		textcolor(i);
		Sleep(100);
		cout << t;
		cout << endl;
	}
}
void Initialize_Race()
{
	for (int i = 0; i < 40; ++i)
	{
		m1[i][0] = '|';
		m1[i][29] = '|';
		//if (i % 2 == 0)
		//	m1[i][14] = '|';
		//else
		//	m1[i][14] = ' ';
		for (int j = 1; j < 29; ++j)
		{
			m1[i][j] = ' ';
		}
	}
}

bool Initialize_Car(Player_car &player)
{

	if (player.x >= 10 && player.y <= 26 && player.x <= 35 && player.y >= 2)
	{
		m1[player.x][player.y] = 'X';
		m1[player.x][player.y - 1] = '|';
		m1[player.x][player.y + 1] = '|';
		m1[player.x - 1][player.y - 1] = 'X';
		m1[player.x - 1][player.y + 1] = 'X';
		m1[player.x + 1][player.y - 1] = 'X';
		m1[player.x + 1][player.y + 1] = 'X';
		return true;
	}
	return false;
}


void Initialize_AICar(AI_car &player)
{

	m1[player.x][player.y] = '#';
	m1[player.x][player.y - 1] = '*';
	m1[player.x][player.y + 1] = '*';
	m1[player.x - 1][player.y - 1] = '*';
	m1[player.x - 1][player.y + 1] = '*';
	m1[player.x + 1][player.y - 1] = '*';
	m1[player.x + 1][player.y + 1] = '*';
}
void Initialize_Money(Money &pt)
{
	m1[pt.x][pt.y] = '$';
}
void MoveCar(Player_car &player)
{

	if (GetAsyncKeyState(VK_UP))
	{
		--player.x;
		if (Initialize_Car(player) != true)
			player.x++;
	}
	else if (GetAsyncKeyState(VK_DOWN))
	{
		player.x++;
		if (Initialize_Car(player) != true)
			player.x--;
	}
	else if (GetAsyncKeyState(VK_LEFT))
	{
		player.y--;
		if (Initialize_Car(player) != true)
			player.y++;
	}
	else if (GetAsyncKeyState(VK_RIGHT))
	{
		player.y++;
		if (Initialize_Car(player) != true)
			player.y--;
	}
	else if (GetAsyncKeyState(VK_SPACE))
	{
		while (true)
		{
			if (GetAsyncKeyState(VK_SPACE))
				break;
		}
	}
}
template <class kutycoi>
bool MoveAICarAndMoney(kutycoi &s)
{
	s.x++;

	if (s.x <= 38)
	{
		return true;
	}
	return false;

}
bool MoveMoney(Money &pt)
{
	pt.x++;
	if (pt.x > 38)
	{
		return false;
	}
	return true;
}
int abc()
{
	return rand() % 26 + 2;
}
int xyz()
{
	return rand() % 8 + 1;
}
template <class kutycoi>
void Initialize_AI(kutycoi &s)
{
	s.y = abc();
	s.x = xyz();
}
template <class kutycoi>
void CheckAI(kutycoi&s)
{

	if (MoveAICarAndMoney(s) == false)
	{
		Initialize_AI(s);
	}
}
void PrintRace(char m[][30])
{
	for (int i = 9; i < 37; ++i)
	{
		cout << "\t\t";
		for (int j = 0; j < 30; ++j)
		{
			if (j == 0 || j == 29)
			{
				textcolor(191);
				cout << m[i][j];
				textcolor(7);
			}
			else if (m[i][j] == 'X' || m[i][j] == '|' || m[i][j] == 'o')
			{
				textcolor(14);
				cout << m[i][j];
				textcolor(7);
			}
			else if (m[i][j] == '#' || m[i][j] == '*')
			{
				textcolor(144);
				cout << m[i][j];
				textcolor(7);
			}
			else if (m[i][j] == '$')
			{
				textcolor(160);
				cout << m[i][j];
				textcolor(7);
			}
			else
				cout << m[i][j];
		}
		cout << endl;
	}
}


//Sort the player data in terms of decreasing point
//and print out to the file name HighScore.
void HighScore(vector<Player> &playarr)
{
	ofstream FileOut("HighScore.txt");
	int size = playarr.size();
	int *a = new int[size];
	int *b = new int[size];
	for (int i = 0; i < size; ++i)
	{
		b[i] = playarr[i].point;
	}
	for (int i = 0; i < size; ++i)
	{
		a[i] = i;
	}

	for (int i = 1; i < size; ++i)
	{
		int j = i;
		int temp = b[i];
		while (j > 0 && temp > b[j - 1])
		{
			a[j] = a[j - 1];
			b[j] = b[j - 1];
			j--;
		}
		a[j] = i;
		b[j] = temp;
	}
	for (int i = 0; size > 2 ? i < 2 : i < size; ++i)
	{
		FileOut << playarr[a[i]].id << " " << playarr[a[i]].point;
		if (i != size - 1)
			FileOut << endl;
	}
	if (size >= 3)
	{
		FileOut << playarr[a[2]].id << " " << playarr[a[2]].point;
	}
	FileOut.close();
	delete a;
	delete b;
}
//Get the highest score
void GetHighestScore(int &highestScore)
{
	ifstream FileIn("HighScore.txt");
	string t;
	FileIn >> t;
	if (t.length() == 0)
	{
		highestScore = 0;
	}
	else
	{
		FileIn.seekg(0, SEEK_SET);
		FileIn >> t >> highestScore;
		FileIn.close();
		return;
	}
}
void GameOver(const Player& pl)
{
	gotoxy(40, 10);
	textcolor(15);
	cout << "Score: " << pl.point << endl;
	ifstream FileIn("GameOver.txt");
	string t;
	srand(time(NULL));
	while (!FileIn.eof())
	{
		getline(FileIn, t);
		int i = rand() % 5 + 10;
		textcolor(i);
		Sleep(100);
		cout << "\n\t\t";
		cout << t;

	}
}
void Input(string&t, char &key, string option[], int size)
{
	bool check = true;
	while (check == true)
	{
		t = "";
		do
		{
			key = getch();
			int a = key;
			if (key == 13)
				break;
			else if (key == 8)
			{
				t.erase(t.begin() + t.length() - 1);
				cout << "\b" << " " << "\b";
			}
			
			else if (a != -32)
			{
				t += key;
				cout << key;
			}
		} while (key != 13);
		for (int i = 0; i < size; ++i)
		{
			if (option[i] == t)
			{
				check = false;
				break;
			}
		}
		if (check == true)
		{
			for (int i = 0; i < t.length(); ++i)
				cout << "\b" << " " << "\b";
		}
	}
}

void Check_CarCrashAndMoneyGain(Player_car &player, AI_car*&arr, Money* &a, int size1, int size2, int &pt, vector<Player> &playarr, Player &pl)
{
	for (int i = 0; i < size1; ++i)
	{
		int hieudong = player.x - arr[i].x;
		int hieucot = player.y - arr[i].y;
		if (hieudong >= -2 && hieudong <= 2 && hieucot <= 2 && hieucot >= -2)
		{
			ClearScreen();
			textcolor(14);
			gotoxy(30, 30);
			cout << "\tYour score is: " << player.pt << endl << "\t\t\t\tGAME OVER ahihi" << endl;
			//cout << "\n" << playarr.size() << endl;
			ifstream FileIn("PlayerData.txt", ios::in);
			//FileIn.seekg(1, SEEK_SET);
			while (!FileIn.eof())
			{
				Player temp;
				FileIn >> temp.id >> temp.password >> temp.point;
				playarr.push_back(temp);
			}
			playarr.pop_back(); // Vi kí tu cuoi cung cua file la mot khoang trong nen khi luu vao vector phan tu cuoi cung bi loi nen phai loai
			//bo phan tu nay ra
			for (int i = 0; i < playarr.size(); ++i)
			{
				if (playarr[i].id == pl.id){
					playarr[i].point = player.pt;
					break;
				}
			}
			FileIn.close();
			//for (int i = 0; i < playarr.size(); ++i)
			//	cout << playarr[i].id << " " << playarr[i].password << " " << playarr[i].point << "\n";
			ofstream FileOut("PlayerData.txt");// OverWrite
			FileOut.seekp(SEEK_SET);
			FileOut << "\n";
			for (int i = 0; i < playarr.size(); ++i)
			{
				FileOut << playarr[i].id << " " << playarr[i].password << " " << playarr[i].point << "\n";
			}
			FileOut.close();
			HighScore(playarr);
			system("pause");
			system("cls");
			GameOver(pl);
			delete a;
			delete arr;
			playarr.clear();
			cout << "\n1. Play again";
			cout << "\n2. Exit game";
			string option[2] = { "1", "2" };
			string t = "";
			char p;
			fflush(stdin);
			Input(t, p, option, 2);
			if (t == "1")
			{
				player.x = 28;
				player.y = 21;

				player.pt = 0;
				LoginFunction(pl);
				int highestScore;
				GetHighestScore(highestScore);
				arr = new AI_car[4];
				a = new Money[7];

				srand(time(NULL));
				for (int i = 0; i < 4; ++i)
					Initialize_AI(arr[i]);
				for (int i = 0; i < 7; ++i)
					Initialize_AI(a[i]);
				GameStart(player, arr, a, highestScore, pl, playarr);
				return;
			}
			else
			{
				exit(0);
			}
		}
	}
	for (int i = 0; i < size2; ++i)
	{
		if (a[i].x == player.x && a[i].y == player.y)
		{
			pt++;
			pl.point = pt;
			Initialize_AI(a[i]);
		}
		else if (a[i].x >= player.x - 1 && a[i].x <= player.x + 1 && a[i].y >= player.y - 1 && a[i].y <= player.y + 1)
		{
			pt++;
			pl.point = pt;
			Initialize_AI(a[i]);
		}
	}
}


void HighScoreBoard(const Player& pl)
{
	ifstream FileIn("HighScore.txt");
	string id, score;
	cout << "\n\t\tId" << "\t\t\t\tScore\n";
	//FileIn.seekg(1, SEEK_SET);
	while (!FileIn.eof())
	{
		FileIn >> id;
		cout << "\t\t" << id;
		FileIn >> score;
		int w = 35 - id.length();
		cout   << setw(w) << score;
		cout << endl;
	}
	FileIn.close();
	cout << "\n\n\n\t\t\tDo you want Go Back(1) or Play Game(2): ";
	string option[2] = { "1", "2" };
	string t = "";
	char key;
	Input(t, key, option, 2);
	if (t == "1")
		LoginFunction(pl);
	else
		return;
}


void UserInfo(const Player& pl)
{
	cout << "\n\t\tID: " << pl.id;
	cout << "\n\t\tPassword: ";
	int i = 0;
	for (; i < pl.id.length() / 3; ++i)
		cout << pl.password[i];
	for (; i < pl.id.length(); ++i)
		cout << "*";
	cout << "\n\t\tScore: " << pl.point;
	cout << "\n\n\n\t\t\tDo you want Go Back(1) or Play Game(2): ";
	string option[2] = { "1", "2" };
	string t = "";
	char key;
	Input(t, key, option, 2);
	if (t == "1")
		LoginFunction(pl);
	else
		return;
}
void LoginFunction(const Player& pl)
{
	system("cls");
	cout << "\n\t\t1.User information";
	cout << "\n\t\t2.High Score";
	cout << "\n\t\t3.Play game";
	cout << "\n\t\t------------------------";
	cout << "\n\t\tChoose option: ";
	string t = "";
	char key;
	while (true)
	{
		do
		{
			key = getch();
			if (key == 13)
				break;
			else if (key == 8)
			{
				t.erase(t.begin() + t.length() - 1);
				cout << "\b" << " " << "\b";
			}
			else
			{
				t += key;
				cout << key;
			}
		} while (key != 13);
		if (t == "1" || t == "2" || t == "3")
			break;
	}
	if (t == "1")
		UserInfo(pl);
	else if (t == "2")
		HighScoreBoard(pl);
	else
		system("cls");
}
void SignUp(ifstream& FileIn, ofstream &FileOut, Player &pl);
bool LogIn(ifstream& filein, Player &pl)
{
	bool checkId = false;
	string id1, password, temp = "";
	char key;
	filein.seekg(0, SEEK_SET);
	filein >> temp;
	if (temp.length() == 0)
	{
		cout << "\nID not found. Please sign up.";
		system("pause");
		exit(0);
		return false;
	}
	cout << "\n\n\n\n\n\n\n\n\n\n\t\t\tID: ";
	while (checkId == false)
	{
		//cout << temp << endl;
		temp = "";
		do
		{
			//cout << "\nInput a char: ";
			key = getch();
			if (key == 13)
			{
				break;
			}
			else if (key == 8 && temp.length() > 0)
			{
				temp.erase(temp.begin() + temp.length() - 1);
				cout << "\b" << " " << "\b";
			}
			else if (key == 8 && temp.length() == 0){}
			else
			{
				cout << key;
				temp += key;
			}
		} while (key != 13);
		if (temp.length() <= 30 && temp.length() >= 1)
		{
			ifstream FileIn("PlayerData.txt");
			while (!FileIn.eof())
			{
				id1 = "";
				FileIn >> id1;
				if (id1 == temp)
				{
					checkId = true;
					break;
				}
				else
				{
					//int temp;
					FileIn >> id1 >> id1;
				}
				//cout << "hello";
			}
			FileIn.close();

		}
		if (checkId == false)
		{
			string k = "   User ID not valid.Do you want to sign up(1) or continue(2): ";
			cout << k;
			string option[2] = { "1", "2" };
			string t = "";
			char p;
			Input(t, p, option, 2);
			if (t == "1")
			{
				filein.close();
				system("cls");
				ifstream FileIn1("PlayerData.txt");
				ofstream FileOut1("PlayerData.txt", ios::app);
				{
					SignUp(FileIn1, FileOut1, pl);
				}
				FileIn1.close();
				FileOut1.close();
				//FileIn1.open("PlayerData.txt");
				//FileOut1.open("PlayerData.txt");
				LoginFunction(pl);
				return true;
			}
			else if (t == "2")
			{
				for (int i = 0; i < temp.length() + k.length() + 1; ++i)
				{
					cout << "\b" << " " << "\b";
				}
			}

		}
	}
	cout << endl << "\t\t\tPassword: ";
	filein.seekg(SEEK_SET);
	while (!filein.eof())
	{
		string id;
		filein >> id;
		if (id == temp)
		{
			filein >> password;
			filein >> pl.point;
			break;
		}
	}
	filein.close();
	string tempPass;
	bool checkPass = false;
	while (checkPass == false)
	{
		tempPass = "";
		do
		{
			key = getch();
			if (key == 13)
			{
				break;
			}
			else if (key == 8 && tempPass.length() > 0)
			{
				tempPass.erase(tempPass.begin() + tempPass.length() - 1);
				cout << "\b" << " " << "\b";
			}
			else if (key == 8 && tempPass.length() == 0){}
			else
			{
				cout << key;
				Sleep(100);
				cout << "\b" << "*";
				tempPass += key;
			}
		} while (true);
		if (tempPass == password)
		{
			checkPass = true;
		}
		else
		{
			for (int i = 0; i < tempPass.length(); ++i)
				cout << "\b" << " " << "\b";
		}
	}
	pl.id = id1;
	pl.password = password;
	
	LoginFunction(pl);
	return true;
}
void SignUp(ifstream& FileIn, ofstream &FileOut, Player &pl)
{
	string id, password;
	char key;
	cout << "\n\n\n\n\n\n\n\n\n\n\t\t\t\t\t\tID(1-30 characters): ";
	while (true)
	{
		id = "";
		do
		{
			key = getch();
			if (key == 13)
				break;
			else if (key == 8 && id.length() > 0)
			{
				id.erase(id.begin() + id.length() - 1);
				cout << "\b" << " " << "\b";
			}
			else if (key == 8 && id.length() == 0){}
			else
			{
				id += key;
				cout << key;
			}
		} while (true);
		if (id.length() <= 30 && id.length() >= 1)
		{
			bool check = true;
			FileIn.seekg(1, SEEK_SET);
			while (!FileIn.eof())
			{
				string t;
				FileIn >> t;
				if (t == id)
				{
					cout << "  " << "(account already existed)";
					char p = getch();
					for (int i = 0; i < id.length() + 27 ; ++i)
					{
						cout << "\b" << " " << "\b";
					}
					check = false;
					break;
				}
				else
					FileIn >> t >> t;
			}
			if (check == true){
				FileOut << id << " ";
				pl.id = id;
				break;
			}
		}
		else
		{
			for (int i = 0; i < id.length(); ++i)
				cout << "\b" << " " << "\b";
		}
	}
	cout << endl << "\t\t\t\t\t\tPassword: ";
	while (true)
	{
		password = "";
		do
		{
			key = getch();
			if (key == 13)
			{
				break;
			}
			else if (key == 8 && password.length() > 0)
			{
				password.erase(password.begin() + password.length() - 1);
				cout << "\b" << " " << "\b";
			}
			else if (key == 8 && password.length() == 0){}
			else
			{
				cout << key;
				Sleep(100);
				cout << "\b" << "*";
				password += key;
			}
		} while (true);
		if (password.length() <= 15 && password.length() >= 1)
		{
			FileOut << password << " " << 0 << "\n";
			pl.password = password;
			break;
		}
		else
		{
			for (int i = 0; i < password.length(); ++i)
				cout << "\b" << " " << "\b";
		}
	}
	pl.point = 0;
}
void Menu()
{
	cout << "\n---------------------------MENU-----------------------------------\n";
	cout << "\n1.Log In";
	cout << "\n2.Sign Up";
	cout << "\n3.Exit";
	cout << "\n--------------------------------\n";
	cout << "You choose: ";
}

void LoginProcess(ifstream &FileIn, ofstream &FileOut, Player &pl)
{
	int luachon;
	do{
		cin >> luachon;
		if (luachon != 1 && luachon != 2 && luachon != 3)
			cout << "\nWrong choice. Choose again: ";
		else
			break;
	} while (true);
	if (luachon == 1)
		LogIn(FileIn, pl);
	else if (luachon == 2)
	{
		system("cls");
		SignUp(FileIn, FileOut, pl);
		LoginFunction(pl);
		return;
	}
	else
	{
		cout << "\nSee you again";
		system("pause");
		exit(0);
	}

}
void GameStart(Player_car& player, AI_car *arr, Money *a, int &highestScore, Player& pl, vector<Player> playarr)
{
	int b = 0;

	srand(time(NULL));
	bool check = true;
	while (true)
	{

		Initialize_Race();


		Initialize_Car(player);
		for (int i = 0; i < 4; ++i)
			Initialize_AICar(arr[i]);
		for (int i = 0; i < 7; ++i)
			Initialize_Money(a[i]);


		PrintRace(m1);
		gotoxy(50, 19);
		cout << "Highest Score: ";
		if (highestScore > pl.point)
			cout << highestScore;
		else
			cout << pl.point;
		gotoxy(50, 20);
		cout << "Score: " << pl.point;

		for (int i = 0; i < 4; ++i)
			CheckAI(arr[i]);
		for (int i = 0; i < 7; ++i)
			CheckAI(a[i]);
		MoveCar(player);
		Check_CarCrashAndMoneyGain(player, arr, a, 4, 7, player.pt, playarr, pl);

		b++;
		Sleep(0);
		ClearScreen();
		if (b > 30)
			b = 0;
	}
}

int main()
{
	PlaySound(TEXT("abc.MP3"), NULL, SND_LOOP);
	vector <Player> playarr;

	Player pl;
	int highestScore;
	GetHighestScore(highestScore);
	ifstream FileIn("PlayerData.txt", ios::app);
	ofstream FileOut("PlayerData.txt", ios::app);
	Intro();
	Menu();

	LoginProcess(FileIn, FileOut, pl);
	FileIn.close();
	FileOut.close();
	system("cls");


	Player_car player;
	AI_car* arr = new AI_car[4];
	Money* a = new Money[7];
	player.x = 28;
	player.y = 21;

	player.pt = 0;
	srand(time(NULL));
	for (int i = 0; i < 4; ++i)
		Initialize_AI(arr[i]);
	for (int i = 0; i < 7; ++i)
		Initialize_AI(a[i]);

	resizeConsole(800, 600);
	GameStart(player, arr, a, highestScore, pl, playarr);

	system("pause");
	return 0;
}