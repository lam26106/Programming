﻿#include <iostream>
#include <stdlib.h>
using namespace std;
char* minusInt(char*, char *, int);
//Them mot ki tu
void addChar(char *str, int pos, char item)
{
	for (int i = strlen(str); i >= pos; --i)
	{
		str[i + 1] = str[i];
	}
	str[pos] = item;
}
//Xoa mot ki tu
void delChar(char *str, int pos)
{
	for (int i = pos; i <= strlen(str); ++i)
		str[i] = str[i + 1];
}
//Dao nguoc chuoi
void revs(char *s)
{
	for (int i = 0; i < strlen(s) / 2; ++i)
	{
		char temp = s[i];
		s[i] = s[strlen(s) - 1 - i];
		s[strlen(s) - 1 - i] = temp;
	}
}
//Cong 2 chuoi
char* addInt(char *str1, char* str2, int MAXCHAR = 50)
{
	int commonLength = (strlen(str1) >= strlen(str2) ? strlen(str1) : strlen(str2)) + 1;
	//cout << commonLength << endl;
	char *s3 = new char[commonLength + 1]; // cap phat dong con tro s3 voi so luong ky tu lon hon commonLength 1 ky tu
	char *s1 = new char[MAXCHAR];	
	strcpy(s1, str1);
	char *s2 = new char[MAXCHAR];
	strcpy(s2, str2);
	int numOfZero1 = commonLength - strlen(s1); // số chữ số 0 cần thêm vào trước s1
	int numOfZero2 = commonLength - strlen(s2); // số chữ số 0 cần thêm vào trước s2
	bool check = true;
	//cong 2 choi am
	if (s1[0] == '-' && s2[0] == '-')
	{
		char *p1 = new char[commonLength + 1];
		char *p2 = new char[commonLength + 1];
		strcpy(p1, s1);
		strcpy(p2, s2);
		delChar(p1, 0);
		delChar(p2, 0);
		s3 = addInt(p1, p2, MAXCHAR);
		addChar(s3,0,'-');
		delete[]p1;
		delete[]p2;
		return s3;
	}
	//chuoi 1 am
	else if (s1[0] == '-')
	{
		char *p1 = new char[commonLength + 1];
		p1 = strcpy(p1, s1);
		delChar(p1, 0);
		s3 = minusInt(s2, p1, MAXCHAR);
		delete[] p1;
		return s3;
	}
	//chuoi 2 am
	else if (s2[0] == '-')
	{
		char *p2 = new char[commonLength + 1];
		p2 = strcpy(p2, s2);
		delChar(s2, 0);
		s3 = minusInt(s1, p2, MAXCHAR);
		delete[] p2;
		return s3;
	}
	for (int i = 1; i <= numOfZero1; ++i)
	{
		addChar(s1, 0, '0');	// thêm chữ số 0 vào s1
	}
	for (int i = 1; i <= numOfZero2; ++i)
	{
		addChar(s2, 0, '0');  // thêm chữ số 0 vào s2
	}
	int idxMax = commonLength - 1;
	int temp = 0;	// khởi tạo biến nhớ VD: 9 + 2 = 12 => temp = 1
	//xu ly tinh tong 2 chuoi duong
	for (int i = idxMax; i >= 0; --i)
	{
		int add = s1[i] - 48 + s2[i] - 48 + temp;	// tinh tong 2 chu so va bien nho voi nhau 
		s3[i] = add % 10 + 48;
		temp = add / 10;
	}
	s3[commonLength] = '\0';
	if (s3[0] == '0')
		delChar(s3, 0);	// sau khi tính xong hết mà trước chuỗi s3 còn số 0 thì xóa đi
	delete[]s1;
	delete[]s2;
	return s3;

}

//Hieu 2 chuoi
char* minusInt(char*str1, char *str2, int MAXCHAR = 50)
{
	int commonLength = (strlen(str1) >= strlen(str2) ? strlen(str1) : strlen(str2)) + 1; // khởi tạo độ dài chung VD: s1 = 3, s2 = 15 
	//003 - 015 => commonLength = 3
	char *s3 = new char[commonLength + 1]; // khởi tạo chuỗi s3 
	char *s1 = new char[MAXCHAR];
	strcpy(s1, str1);
	char *s2 = new char[MAXCHAR];
	strcpy(s2, str2);
	char *tmp; // biến trung gian để hoán vị 2 chuỗi
	if (s1[0] == '-' && s2[0] == '-') // trường hợp cả 2 đều âm: s1 = -a, s2 = -b => s1 - s2 = b - a
									  // ta hoán vị s1 = b, s2 = a
	{
		tmp = s1;
		s1 = s2;
		s2 = tmp;
		delChar(s1, 0);
		delChar(s2, 0);
	}
	else if (s1[0] == '-' || s2[0] == '-') // trường hợp 1 trong 2 chuỗi có dấu - ở đầu VD: s1 = -a, s2 = b => s1 - s2 = -(a+b)
	{
		if (s1[0] == '-') // trường hợp chuỗi 1 có dấu -
		{
			//char *s4 = strdup(s1);	// tạo bản sao và gán cho con trỏ s4 để không làm ảnh hưởng đến chuỗi gốc s1
			delChar(s1, 0);	// xóa dấu - ở đầu
			s3 = addInt(s1, s2, MAXCHAR); // tính tổng s4, s3 rồi gán cho s3
			addChar(s3, 0, '-'); // thêm dấu - vào đằng trước
		//	delete[]s4;
			return s3;
		}
		else if (s2[0] == '-') // s1 = a, s2 = -b => s1 - s2 = a + b
		{
			//char *s4 = strdup(s2);
			delChar(s2, 0);
			s3 = addInt(s2, s1, MAXCHAR);
			//delete[]s4;
			return s3;
		}
	}
	// xử lý cho 2 chuỗi đều có dấu - hoặc đều không có dấu - (đối với TH đều có dấu - thì ở trên ta đã hoán vị chuỗi và xóa bỏ hết dấu -
	//giờ ta chỉ cần xử lý duy nhất trường hợp 2 chuỗi đều không có dấu -
	// VD s1 = 3, s2 = 5 => s1 - s2 = 03 - 05 = -2
	int numOfZero1 = commonLength - strlen(s1);
	int numOfZero2 = commonLength - strlen(s2);
	for (int i = 1; i <= numOfZero1; ++i) // thêm số 0 vào đầu chuỗi 1
	{
		addChar(s1, 0, '0');
	}
	for (int i = 1; i <= numOfZero2; ++i) // thêm số 0 vào đầu chuỗi 2
	{
		addChar(s2, 0, '0');
	}
	char *greater, *smaller; // vì khi trừ ta luôn trừ số lớn cho số bé (rồi sau đó tùy xem có nhân với -1 hay không và phải đảm bảo
	// không thay đổi chuỗi gốc s1,s2
	int check = 0; // check xem chuỗi 1 có bé hơn , lớn hơn hay bằng với chuỗi 2
	// check = 0 => bằng, check = 1 => lớn hơn, check = -1 => bé hơn
	for (int i = 0; i < commonLength; ++i)
	{
		if (s1[i] < s2[i])
		{
			greater = strdup(s2);
			smaller = strdup(s1);
			check = -1;	// chuỗi 1 bé hơn chuỗi 2
			break;
		}
		else if (s1[i] > s2[i])
		{
			greater = strdup(s1);
			smaller = strdup(s2);
			check = 1;	// chuỗi 1 to hơn chuỗi 2
			break;
		}
	}
	//cout << greater << endl << smaller << endl;
	if (check == 0)// bằng nhau thì return 0 luôn
	{
		s3[0] = '0';
		s3[1] = '\0';
		return s3;
	}
	// trừ từng đơn vị một
	int idxMax = commonLength - 1;
	int temp = 0; // biến nhớ đầu tiên = 0
	for (int i = idxMax; i >= 0; --i)
	{
		int minus = greater[i] - smaller[i] - temp; // nếu trừ các đơn vị với nhau
		if (minus < 0) // nếu minus < 0
		{
			s3[i] = (greater[i] + 10 - smaller[i] - temp) + 48; // vd: 5 - 9 <=> 5 + 10 - 6 nhớ 1
			temp = 1;
		}
		else
		{
			s3[i] = minus + 48; // nếu không thì trừ bình thường
			temp = 0; // biến nhớ bằng 0
		}
	}
	//s3[0] = '0';
	s3[commonLength] = '\0';
	//cout << s3 << endl;
	if (check == -1) // nếu chuỗi 1 < chuỗi 2 thì sau khi tính sau phải thêm dấu - đằng trc
	{
		s3[0] = '-';
		for (int i = 1;; ++i) // thêm dấu - đằng trước rồi check xem nếu đằng sau còn chữ số 0 thì del hết vd: -0123 => -123
		{
			if (s3[i] == '0') {
				delChar(s3, i);
				i--;
			}
			else
				break;
		}
	}
	for (int i = 0;; ++i) // nếu có dấu - từ đầu thì xóa tất cả số 0 cho đến khi gặp một số !=0
	{
		if (s3[i] == '0') {
			delChar(s3, i);
			i--;
		}
		else
			break;
	}
	delete[]greater;
	delete[]smaller;
	delete[]s1;
	delete[]s2;

	return s3;
}

//Nhân 2 chuỗi
char* multipleInt(char *str1, char *str2, int MAXCHAR =50)
{
	int commonLength = (strlen(str1) > strlen(str2) ? strlen(str1) : strlen(str2));
	int *s3 = new int[strlen(str1) + strlen(str2)] ;
	int length_s1 = strlen(str1);
	int length_s2 = strlen(str2);
	char *s1 = new char[commonLength + 1], *s2 = new char[commonLength + 1];
	strcpy(s1, str1);
	strcpy(s2, str2);
	bool check = false;
	if (s1[0] == '-' && s2[0] == '-')
	{
		delChar(s1, 0);
		delChar(s2, 0);
	}
	else if (s1[0] == '-')
	{
		delChar(s1, 0);
		check = true;
	}
	else if (s2[0] == '-') {
		delChar(s2, 0);
		check = true;
	}
	//cout << s1 << endl << s2 << endl;
	for (int i = 0; i < strlen(s1) + strlen(s2); ++i)
	{
		s3[i] = 0;
	}
	for (int i = strlen(s1) - 1; i >= 0; --i)
	{
		for (int j = strlen(s2) - 1; j >= 0; --j)
		{
			s3[i + j + 1] += (s1[i] - 48) * (s2[j] - 48);
		}
	}
	for (int i = strlen(s1) + strlen(s2) - 1; i >= 0; --i)
	{
		if (s3[i] >= 10)  
		{
			s3[i - 1] += s3[i] / 10;
			s3[i] %= 10;
		}
	}
	char *str = new char[strlen(s1) + strlen(s2) + 1];
	int i = 0;
	for (; i < strlen(s1) + strlen(s2); ++i)
	{
		str[i] = s3[i] + 48;
	}
	str[i] = '\0';
	for (int j = 0; j < strlen(str); ++j)
	{
		if (str[j] == '0') {
			delChar(str, 0);
			j--;
		}
		else break;
	}
	if (check == true)
		addChar(str, 0, '-');
	return str;
	
}

//2 số chia cho nhau rồi trả về 1 kí tự
//Độ dài của số num1 luôn <= số num2 để đảm bảo cho ra kết quả luôn là 1 số từ 0 - 9
char simple_divide(int num1, int num2)
{
	int resInt = 0;
	while (num1 >= num2)
	{
		resInt++;
		num1 -= num2;
	}
	char resChar = resInt + 48;
	return resChar;
}
char *divideInt(char *s1, char *s2)
{
	int dividend = 0, divisor = 0, remain = 0;
	char *res = new char[strlen(s1) + 1];//Khở tạo chuỗi chứa kết quả
	char *str1 = strdup(s1);
	char *str2 = strdup(s2);
	bool check = true; 
	// Check dùng để kiểm tra xem là phếp chia 2 số dương hay là trong phép chia có ít nhất 1 số âm(2 số âm chia cho nhau thì cũng giống với 2 số dương chia)
	if (str1[0] == '-' && str2[0] == '-')
	{
		delChar(str1, 0);
		delChar(str2, 0);
	}
	else if (str1[0] == '-') {
		delChar(str1, 0);
		check = false;
	}
	else if (str2[0] == '-') {
		delChar(str2, 0);
		check = false;
	}
	if (str2[0] == '0')
	{
		res[0] = '\0';
		cout << "Error";
		return res;
	}
	//Xu ly phep chia neu chuoi bị chia co do dai nho hon chuoi chia
	if (strlen(str1) < strlen(str2))
	{
		res[0] = '0';
		res[1] = '\0';
		for (int i = 0; i < strlen(str1); ++i)
			remain = remain * 10 + str1[i] - 48;
		if (check == false && s1[0] == '-')
			remain *= -1;
		cout << "Phan du la: " << remain << endl;
		cout << "Ket qua la:  ";
		return res;
	}
	int i = 0, idx = 0;
	//Trước tiên ta sẽ lấy ra số bị chia có độ dài bằng số chia
	for (; i < strlen(str2); ++i)
	{
		dividend = dividend * 10 + str1[i]  - 48;
	}
	//Chuyen chuỗi chia sang dạng số
	for (int i = 0; i < strlen(str2); ++i)
	{
		divisor = divisor * 10 + str2[i] - 48;
	}
	//Chia 2 chuỗi cho nhau rồi trả kết quả về chuỗi
	res[idx++] = simple_divide(dividend, divisor);
	//Tiếp tục xử lý chia như hồi tiểu học
	while (i < strlen(str1))
	{
		remain = dividend - (res[idx - 1] - 48) * divisor;
		dividend = remain * 10 + str1[i++] - 48;
		res[idx++] = simple_divide(dividend, divisor);
	}
	res[idx] = '\0';
	//Tinh số dư
	remain = dividend - (res[idx - 1] - 48) * divisor;
	cout << "Phan du la: " << remain << endl;
	cout << "Ket qua la: ";
	//Xóa tất cả số không mà ta đã thêm lúc đầu
	for (int i = 0; res[i] != '\0'; ++i)
	{
		if (res[i] == '0') {
			delChar(res, i);
			i--;
		}
		else break;
	}
	//Neu la phep chia am
	if (check == false)
	{
		addChar(res, 0, '-');
	}
	delete[]str1;
	delete[]str2;
	return res;

}
int main()
{
	const int MAXCHAR = 50;
	char str1[MAXCHAR] = "99999999999999999999999999999999999";
	char str2[MAXCHAR] = "999999999999999999999999999999999";
	cout << "Cong 2 chuoi = " << addInt(str1, str2) << endl;
	cout << "Hieu 2 chuoi = " << minusInt(str1, str2) << endl;
	cout << "Tich 2 chuoi = " << multipleInt(str1, str2) << endl;
	cout << "Thuong 2 chuoi la: " << endl; 
	cout << divideInt(str1, str2) << endl;
	system("pause");
	return 0;
}