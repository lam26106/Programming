﻿#include <iostream>
using namespace std;

//Them mot ki tu
void addChar(char *str, int pos, char item)
{
	for (int i = strlen(str); i >= pos; --i)
	{
		str[i + 1] = str[i];
	}
	str[pos] = item;
}
//Xoa mot ki tu
void delChar(char *str, int pos)
{
	for (int i = pos; i <= strlen(str); ++i)
		str[i] = str[i + 1];
}
//Dao nguoc chuoi
void revs(char *s)
{
	for (int i = 0; i < strlen(s) / 2; ++i)
	{
		char temp = s[i];
		s[i] = s[strlen(s) - 1 - i];
		s[strlen(s) - 1 - i] = temp;
	}
}
//Hieu 2 chuoi
char* minusInt(char*str1, char *str2, int MAXCHAR = 50)
{
	int commonLength = (strlen(str1) >= strlen(str2) ? strlen(str1) : strlen(str2)) + 1; // khởi tạo độ dài chung VD: s1 = 3, s2 = 15 
	//003 - 015 => commonLength = 3
	char *s3 = new char[commonLength + 1]; // khởi tạo chuỗi s3 
	char *s1 = new char[MAXCHAR];
	strcpy(s1, str1);
	char *s2 = new char[MAXCHAR];
	strcpy(s2, str2);
	char *tmp; // biến trung gian để hoán vị 2 chuỗi

	// xử lý cho 2 chuỗi đều có dấu - hoặc đều không có dấu - (đối với TH đều có dấu - thì ở trên ta đã hoán vị chuỗi và xóa bỏ hết dấu -
	//giờ ta chỉ cần xử lý duy nhất trường hợp 2 chuỗi đều không có dấu -
	// VD s1 = 3, s2 = 5 => s1 - s2 = 03 - 05 = -2
	int numOfZero1 = commonLength - strlen(s1);
	int numOfZero2 = commonLength - strlen(s2);
	for (int i = 1; i <= numOfZero1; ++i) // thêm số 0 vào đầu chuỗi 1
	{
		addChar(s1, 0, '0');
	}
	for (int i = 1; i <= numOfZero2; ++i) // thêm số 0 vào đầu chuỗi 2
	{
		addChar(s2, 0, '0');
	}
	char *greater, *smaller; // vì khi trừ ta luôn trừ số lớn cho số bé (rồi sau đó tùy xem có nhân với -1 hay không và phải đảm bảo
	// không thay đổi chuỗi gốc s1,s2
	int check = 0; // check xem chuỗi 1 có bé hơn , lớn hơn hay bằng với chuỗi 2
	// check = 0 => bằng, check = 1 => lớn hơn, check = -1 => bé hơn
	for (int i = 0; i < commonLength; ++i)
	{
		if (s1[i] < s2[i])
		{
			greater = strdup(s2);
			smaller = strdup(s1);
			check = -1;	// chuỗi 1 bé hơn chuỗi 2
			break;
		}
		else if (s1[i] > s2[i])
		{
			greater = strdup(s1);
			smaller = strdup(s2);
			check = 1;	// chuỗi 1 to hơn chuỗi 2
			break;
		}
	}
	//cout << greater << endl << smaller << endl;
	if (check == 0)// bằng nhau thì return 0 luôn
	{
		s3[0] = '0';
		s3[1] = '\0';
		return s3;
	}
	// trừ từng đơn vị một
	int idxMax = commonLength - 1;
	int temp = 0; // biến nhớ đầu tiên = 0
	for (int i = idxMax; i >= 0; --i)
	{
		int minus = greater[i] - smaller[i] - temp; // nếu trừ các đơn vị với nhau
		if (minus < 0) // nếu minus < 0
		{
			s3[i] = (greater[i] + 10 - smaller[i] - temp) + 48; // vd: 5 - 9 <=> 5 + 10 - 6 nhớ 1
			temp = 1;
		}
		else
		{
			s3[i] = minus + 48; // nếu không thì trừ bình thường
			temp = 0; // biến nhớ bằng 0
		}
	}
	//s3[0] = '0';
	s3[commonLength] = '\0';
	//cout << s3 << endl;
	if (check == -1) // nếu chuỗi 1 < chuỗi 2 thì sau khi tính sau phải thêm dấu - đằng trc
	{
		s3[0] = '-';
		for (int i = 1;; ++i) // thêm dấu - đằng trước rồi check xem nếu đằng sau còn chữ số 0 thì del hết vd: -0123 => -123
		{
			if (s3[i] == '0') {
				delChar(s3, i);
				i--;
			}
			else
				break;
		}
	}
	for (int i = 0;; ++i) // nếu có dấu - từ đầu thì xóa tất cả số 0 cho đến khi gặp một số !=0
	{
		if (s3[i] == '0') {
			delChar(s3, i);
			i--;
		}
		else
			break;
	}
	delete[]greater;
	delete[]smaller;
	delete[]s1;
	delete[]s2;

	return s3;
}
int compareString(char *s1, char *s2)
{
	for (int i = 0; i <= strlen(s1) && i <= strlen(s2); ++i)
	{
		if (s1[i] > s2[i])
			return 1;
		else if (s1[i] < s2[i])
			return -1;
	}
	return 0;
}
char* simple_multiple(char *str, char s)
{
	char *strcop = new char[strlen(str) + 1];
	strcpy(strcop, str);
	char *res = new char[strlen(strcop) + 2];
	for (int i = 0; i < strlen(strcop) + 1; ++i)
		res[i] = '0';
	res[strlen(strcop) + 1] = '\0';
	int nho = 0;
	for (int i = strlen(strcop) - 1; i >= 0; --i)
	{
		int result = (s - 48) * (strcop[i] - 48);
		res[i + 1]  = (res[i+1] -48 +(result  + nho) % 10) + 48;
		nho = result / 10;
	}
	res[0] = nho + 48;
	for (int i = 0; i < strlen(res); ++i)
	{
		if (res[i] == '0')
			delChar(res, 0);
		else
			break; 
	}
	return res;
}

char simple_divide(char*str1, char*str2)
{
	int count = 0;
	char *dividend = new char[strlen(str1) + 1];
	char *divisor = new char[strlen(str2) + 1];
	strcpy(dividend, str1);
	strcpy(divisor, str2);
	int c = compareString(dividend, divisor);
	while (c == 1 || c == 0)
	{
		dividend = minusInt(dividend, divisor);
		count++;
		c = compareString(dividend, divisor);
	}
	char result = count + 48;
	return result;
}
char* divideInt(char*str1, char *str2, const int MAXCHAR = 50)
{
	char *str1cop = new char[strlen(str1) + 1];
	char *str2cop = new char[strlen(str2) + 1];
	strcpy(str1cop, str1);
	strcpy(str2cop, str2);
	char *dividend = new char[strlen(str2cop) + 1];
	char *res = new char[strlen(str1cop) + 1];
	char *remain = new char[strlen(str1cop) + 1];
	if (compareString(str1cop, str2cop) == -1)
	{
		res[0] = '0';
		res[1] = '\0';
		remain = str1cop;
		cout << "The remain is: " << remain << endl;
		return res;
	}
	else if (compareString(str1cop, str2cop) == 0)
	{
		res[0] = '1';
		res[1] = '\0';
		remain[0] = '0';
		remain[1] = '\0';
		cout << "The remain is: " << remain << endl;
		return res;
	}
	int i = 0;
	for (; i < strlen(str2cop); ++i)
	{
		dividend[i] = str1cop[i];
	}
	dividend[i] = '\0';
	//cout << dividend << endl;
	int index = 0, indexOfremain = 0;
	//cout << simple_divide(dividend, str2cop) << endl;
	res[index++] = simple_divide(dividend, str2cop);
	remain = minusInt(dividend, simple_multiple(str2cop, res[index - 1]));
	cout << res[index - 1] << " " << remain << endl;
	for (; i < strlen(str1cop); ++i)
	{
		addChar(remain, strlen(remain), str1cop[i]);
		//cout << remain << " ";
		dividend = remain;
		res[index++] = simple_divide(dividend, str2cop);
		remain = minusInt(dividend, simple_multiple(str2cop, res[index - 1]));
		//cout << remain << endl;
	}
	//remain[]
	res[index] = '\0';
	cout << "The remain is: " << remain << endl;
	delete[] str1cop;
	delete[] str2cop;
	delete[] remain;
	return res;
}
int main()
{
	char *str1 = "999999999999999999999999999999999999";
	char *str2 = "99999";
	//cout << compareString(str1, str2) << endl;
	//char *c = minusInt(str1, str2);
	//char c = simple_divide(str1, str2);
	//cout << c;
	cout << divideInt(str1, str2);
	system("pause");
	return 0;
}