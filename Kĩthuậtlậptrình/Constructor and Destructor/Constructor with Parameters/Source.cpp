#include <iostream>
#include <string>
using namespace std;

class HocSinh
{
private:
	int DiemToan;
	string Ten;
public:
	//Default Constructor:
	HocSinh()
	{
		Ten = "\nNguyen Trung Lam";
		DiemToan = 10;
		cout << Ten << " " << DiemToan;
	}

	//Constructor with parameters:
	HocSinh(string ten, int diem)
	{
		Ten = ten;
		DiemToan = diem;
		cout << Ten << "\n" << DiemToan;
	}
};
int main()
{
	
	HocSinh s1("Nguyen Thuy Linh", 9), s2;

	system("pause");
	return 0;
}