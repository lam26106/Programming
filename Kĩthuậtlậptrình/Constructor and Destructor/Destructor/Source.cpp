#include <iostream>
#include <string>
using namespace std;

class HocSinh
{
private:
	int *DiemToan;
	string Ten;
public:
	//Default Constructor:
	void Xuat()
	{
		cout << "\n" << Ten << "\n" << *DiemToan;
	}
	HocSinh()
	{
		Ten = "\nNguyen Trung Lam";
		DiemToan = new int;
		*DiemToan = 10;
		cout << Ten << " " << *DiemToan;
	}

	//Constructor with parameters:
	HocSinh(string ten, int diem)
	{
		Ten = ten;
		DiemToan = new int;
		*DiemToan = diem;
		cout << Ten << "\n" << *DiemToan;
	}
	//copy constructor/
	// chung ta nen tao ra mot phuong thuc copy constructor nhu sau de phong truong hop trong class co bien con tro
	HocSinh(const HocSinh &x)
	{
		Ten = x.Ten;
		DiemToan = new int;
		*DiemToan = *x.DiemToan;
	}
	//Destructor
	//If in class there is no pointer variable, we just need to leave the destructor empty. However, if there is one or more pointer
	//in class, we need to creat th destructor method in which we release the memory space of pointer
	~HocSinh()
	{
		delete DiemToan;
	}
};
int main()
{

	HocSinh s1("Nguyen Thuy Linh", 9);
	HocSinh s2(s1);
	s2.Xuat();

	system("pause");
	return 0;
}