#pragma once
#include<iostream>
#include<string>
using namespace std;
class myFavoriteDataType //stimulate points in 2D 
{
private:
	int value;
	string name;
public:
	myFavoriteDataType();
	myFavoriteDataType(string, int);
	int getValue() const;
	string getName() const;

	void setValue(int _x);
	void setName(string _y);

	bool operator < (const myFavoriteDataType& s) const; 
	bool operator >= (const myFavoriteDataType& s) const;
	bool operator > (const myFavoriteDataType& s) const;
	bool operator <= (const myFavoriteDataType& s) const;
	bool operator == (const myFavoriteDataType& s) const;
	friend ostream& operator << (ostream&, const myFavoriteDataType& s);
};




