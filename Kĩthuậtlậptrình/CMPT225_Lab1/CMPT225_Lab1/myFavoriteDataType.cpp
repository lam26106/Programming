#include "myFavoriteDataType.h"

myFavoriteDataType::myFavoriteDataType()
{
	this->setName("");
	this->setValue(0);
}
myFavoriteDataType::myFavoriteDataType(string _name, int _value)
{
	this->setName(_name);
	this->setValue(_value);
}

string myFavoriteDataType::getName() const
{
	return this->name;
}

int myFavoriteDataType::getValue() const
{
	return this->value;
}


void myFavoriteDataType::setValue(int _x)
{
	this->value = _x;
}
void myFavoriteDataType::setName(string _y)
{
	this->name = _y;
}
bool myFavoriteDataType::operator < (const myFavoriteDataType& s) const
{
	return this->getValue() < s.getValue();
}
bool myFavoriteDataType::operator >= (const myFavoriteDataType& s) const
{
	return !((*this) < s);
}
bool myFavoriteDataType::operator > (const myFavoriteDataType& s) const
{
	return s < (*this);
}
bool myFavoriteDataType::operator <= (const myFavoriteDataType& s) const
{
	return !((*this) > s );
}
bool myFavoriteDataType::operator == (const myFavoriteDataType& s) const
{
	return this->getValue() == s.getValue();
}
ostream& operator << (ostream& os, const myFavoriteDataType& s)
{
	os << s.getName() << " " << s.getValue() << endl;
	return os;
}
