#pragma once
#include <iostream>
#include <string>
using namespace std;
class Diem
{
private:
	float x, y; 
public:
	// Default constructor
	Diem(void);
	Diem(float, float); //constructor with parameter
	Diem(const Diem &); //copy constructor
	//Destructor
	~Diem(void);
	void Nhap();
	void Xuat();
	void set_x(float);
	void set_y(float);
	float get_x();
	float get_y();

};

