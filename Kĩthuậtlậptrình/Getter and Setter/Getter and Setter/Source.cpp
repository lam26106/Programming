#include <iostream>
#include <string>
using namespace std;

class HocSinh
{
private:
	int *DiemToan;
	string Ten;
public:
	//Default Constructor:
	void Xuat()
	{
		cout << "\n" << Ten << "\n" << *DiemToan;
	}
	HocSinh()
	{
		Ten = "\nNguyen Trung Lam";
		DiemToan = new int;
		*DiemToan = 10;
		cout << Ten << " " << *DiemToan;
	}

	//Constructor with parameters:
	HocSinh(string ten, int diem)
	{
		Ten = ten;
		DiemToan = new int;
		*DiemToan = diem;
		cout << Ten << "\n" << *DiemToan;
	}
	//copy constructor/
	// chung ta nen tao ra mot phuong thuc copy constructor nhu sau de phong truong hop trong class co bien con tro
	HocSinh(const HocSinh &x)
	{
		Ten = x.Ten;
		DiemToan = new int;
		*DiemToan = *x.DiemToan;
	}
	//Destructor
	~HocSinh()
	{
		delete DiemToan;
	}
	string get_Ten()
	{
		return Ten;
	}
	int *get_DiemToan()
	{
		return DiemToan;
	}
	void set_Ten(string ten)
	{
		Ten = ten;
	}
	void set_Diem(int &diemToan)
	{
		DiemToan = new int;
		*DiemToan = diemToan;
	}
};
int main()
{

	HocSinh s1("Nguyen Thuy Linh", 9);
	HocSinh s2(s1);
	s2.Xuat();

	system("pause");
	return 0;
}