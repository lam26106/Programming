#include <iostream>
using namespace std;
struct PhanSo
{
	int TS, MS;
};
typedef struct PhanSo PHANSO;
void NhapPhanSo(PHANSO &ps)
{
	cout << "Nhap vao tu so: ";
	cin >> ps.TS;
	do{
		cout << "Nhap vao mau so (khac 0): ";
		cin >> ps.MS;
	} while (ps.MS == 0);
}
void XuatPhanSo(PHANSO ps)
{
	cout << "Phan so la: " << ps.TS << "/" << ps.MS;
}
int UCLN(int a, int b)
{
	//giai thuat 1
	//if (a == 0 && b != 0)
	//	return a;
	//else if (a != 0 && b == 0)
	//	return b;
	//while (a != b)
	//{
	//	if (a > b)
	//		a -= b;
	//	else
	//		b -= a;
	//}
	//return a;

	//giai thuat 2: Euclid
	if (a <= 0)
		a *= -1;
	if (b < 0)
		b *= -1;
	int num1 = a > b ? a : b;
	int num2 = a < b ? a : b;
	if (num2 == 0)
		return num1;
	else
		UCLN(num2, num1 % num2);
}
PHANSO TongPhanSo(PHANSO ps1, PHANSO ps2)
{
	PHANSO ps3;
	ps3.TS = ps1.TS * ps2.MS + ps2.TS * ps1.MS;
	ps3.MS = ps1.MS * ps2.MS;
	int ucln = UCLN(ps3.TS, ps3.MS);
	ps3.TS /= ucln;
	ps3.MS /= ucln;
	return ps3;
}
PHANSO operator + (PHANSO ps1, PHANSO ps2)
{

	PHANSO ps3;
	ps3.TS = ps1.TS * ps2.MS + ps2.TS * ps1.MS;
	ps3.MS = ps1.MS * ps2.MS;
	int ucln = UCLN(ps3.TS, ps3.MS);
	ps3.TS /= ucln;
	ps3.MS /= ucln;
	return ps3;
}
PHANSO operator - (PHANSO ps1, PHANSO ps2)
{
	PHANSO ps3;
	ps3.TS = ps1.TS * ps2.MS - ps2.TS * ps1.MS;
	ps3.MS = ps1.MS * ps2.MS;
	int ucln = UCLN(ps3.TS, ps3.MS);
	ps3.TS /= ucln;
	ps3.MS /= ucln;
	
	return ps3;
}
PHANSO operator * (PHANSO ps1, PHANSO ps2)
{
	PHANSO ps3;
	ps3.TS = ps1.TS * ps2.TS;
	ps3.MS = ps1.MS * ps2.MS;
	return ps3;
}
PHANSO operator / (PHANSO ps1, PHANSO ps2)
{
	if (ps2.TS != 0){
		PHANSO temp;
		temp.MS = ps2.TS;
		temp.TS = ps2.MS;
		return ps1 * temp;
	}
}
PHANSO operator + (PHANSO ps1, int num)
{
	PHANSO ps2;
	ps2.TS = num;
	ps2.MS = 1;
	return ps1 + ps2;
}
PHANSO operator + (int num, PHANSO ps1)
{
	PHANSO ps2;
	ps2.TS = num;
	ps2.MS = 1;
	return ps1 + ps2;
}


//Toan tu nhap phan so
istream& operator >> (istream &is, PHANSO &ps)
{
	//thuc hien cac buoc nhu trong ham NhapPhanSo
	cout << "Nhap vao tu so: ";
	is >> ps.TS;
	do{
		cout << "Nhap vao mau so (khac 0): ";
		is >> ps.MS;
	} while (ps.MS == 0);
	return is;
}
//Toan tu xuat phan so
ostream& operator << (ostream &os, PHANSO ps)
{
	//thuc hien cac buoc nhu trong ham XuatPhanSo
	os << "Phan so la: " << ps.TS << "/" << ps.MS;
	return os;
}
int main()
{
	PHANSO ps1, ps2;
	cin >> ps1 >> ps2;
	PHANSO ps3 = ps1 + ps2;
	cout << ps3;

	system("pause");
	return 0;
}