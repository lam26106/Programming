#pragma once
#include "SinhVien.h"

struct LopHoc
{
	int soSinhVien;
	char TenLop[10];
	SINHVIEN *sinhvien;
};
typedef struct LopHoc LOPHOC;
void NhapLopHoc(LOPHOC &);
void XuatLopHoc(LOPHOC);
void SapXepSinhVien_DTB(LOPHOC &, char);
void HoanVi(SINHVIEN &, SINHVIEN &);
void XuatDanhSachDuocSapXep(LOPHOC);
void TinhSoHocSinhHocLai(LOPHOC, int&, float&);
SINHVIEN* TraCuuSinhVien(LOPHOC, char*);
void UpdateThongTinSinhVien(LOPHOC &, char *);
void SapXepDanhSachLopTheoTen(LOPHOC &);

