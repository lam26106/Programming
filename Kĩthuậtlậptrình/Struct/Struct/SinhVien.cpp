#pragma warning(disable:4996)

#include "SinhVien.h"
void NhapSinhVien(SINHVIEN &sv)
{
	fflush(stdin);
	cout << "\nNhap vao ma so sinh vien: ";
	gets(sv.MaSo);
	fflush(stdin);
	cout << "\nNhap vao ten sinh vien: ";
	gets(sv.Ten);
	cout << "\nNhap thong tin cac mon hoc";
	NhapDanhSachMon(sv.danhsachmon);
}
void XuatSinhVien(SINHVIEN sv)
{
	cout << "\nMa so sinh vien la: " << sv.MaSo;
	cout << "\nTen sinh vien la: " << sv.Ten;
	cout << "\nThong tin cac mon la: ";
	XuatDanhSachMon(sv.danhsachmon);
	float diemTB = TinhDiemTrungBinh(sv);
	cout << "\nDiem TB cua sinh vien la: " << diemTB;
	char *xeploai = XepLoaiHocLuc(sv);
	cout << "\nXep loai hoc luc cua sinh vien la: " << xeploai;
}
float TinhDiemTrungBinh(SINHVIEN sv)
{
	int SoTinChi = 0;
	float TongDiem = 0;
	for (int i = 0; i < sv.danhsachmon.soMon; ++i)
	{
		SoTinChi += sv.danhsachmon.danhsach[i].tinChi;
		TongDiem += sv.danhsachmon.danhsach[i].soDiem;
	}
	return TongDiem / SoTinChi;
}char* XepLoaiHocLuc(SINHVIEN sv)
{
	float dtb = TinhDiemTrungBinh(sv);
	if (dtb < 2)
		return "Kem";
	else if (dtb < 5)
		return "Yeu";
	else if (dtb < 7)
		return "Trung Binh";
	else if (dtb < 8)
		return "Kha";
	else if (dtb < 9)
		return "Gioi";
	else
		return "Xuat cmn Sac";
}
void CapNhatSinhVien(SINHVIEN &sv)
{
		fflush(stdin);
		cout << "\nNhap lai ten sinh vien: ";
		gets(sv.Ten);
		fflush(stdin);
		cout << "\nNhap lai thong tin cac mon hoc";
		NhapDanhSachMon(sv.danhsachmon);
	
}
