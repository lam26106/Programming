#pragma once
#include "DanhSachMon.h"
struct Sinhvien
{
	char Ten[50], MaSo[9];
	DanhSachMonHoc danhsachmon;
};
typedef struct Sinhvien SINHVIEN;
void NhapSinhVien(SINHVIEN &);
void XuatSinhVien(SINHVIEN);
float TinhDiemTrungBinh(SINHVIEN);
char* XepLoaiHocLuc(SINHVIEN);
void CapNhatSinhVien(SINHVIEN &);
