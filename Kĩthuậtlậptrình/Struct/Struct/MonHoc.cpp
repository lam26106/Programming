#include "MonHoc.h"
void NhapMonHoc(MONHOC &subject)
{
	fflush(stdin);
	cout << "\nNhap ten mon hoc: ";
	gets(subject.TenMon);
	do {
		cout << "\nSo tin chi khong duoc be hon 0: ";
		cin >> subject.tinChi;
	} while (subject.tinChi < 0);
	fflush(stdin);
	do {
		cout << "\nSo diem khong duoc be hon 0 va lon hon 10: ";
		cin >> subject.soDiem;
	} while (subject.soDiem < 0 || subject.soDiem > 10);
}
void XuatMon(MONHOC subject)
{
	cout << "\nTen mon hoc la: " << subject.TenMon;
	cout << "\nSo tin chi cua mon la: " << subject.tinChi;
	cout << "\nSo diem dat duoc la: " << subject.soDiem;
}