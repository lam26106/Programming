#include <iostream>
#include<unordered_map>
using namespace std;

struct Node
{
	int data;
	Node *left, *right;
	Node(){
		left = NULL;
		right = NULL;
	}
};
struct BSTree
{
	Node* root;
	BSTree(){
		root = NULL;
	}
};
Node* newNode(int _data)
{
	Node* tmp = new Node;
	tmp->left = tmp->right = NULL;
	tmp->data = _data;
	return tmp;
	
}
//A program to check if a binary tree is BST or not

bool isBST(Node* root, Node* l = NULL, Node* r = NULL)
{
	// Base condition
	if (root == NULL)
		return true;
	cout << root->data << " ";
	// if left node exist that check it has
	// correct data or not
	if (l != NULL && root->data < l->data){
		
		return false;
	}
	

	// if right node exist that check it has
	// correct data or not
	if (r != NULL && root->data > r->data){
		return false;
	}
	cout << endl;
	// check recursively for every node.
	return isBST(root->left, l, root) &&
		isBST(root->right, root, r);
}

//Find k-th smallest element in BST (Order Statistics in BST)
//int n = 0;
void k_th_SmallestElement(Node* root, int k)
{
	static int n = 0;
	if (root != NULL){
		k_th_SmallestElement(root->left, k);
		n++;
		if (n == k){
			cout << root->data<< endl;
			return;
		}
		k_th_SmallestElement(root->right, k);
	}
}
//Check if each internal node of a BT has exactly one child
bool checkOneChild(Node* root)
{
	if (root != NULL)
	{
		if (root->left != NULL && root->right != NULL)
			return false;
		if (root->left != NULL)
			return checkOneChild(root->left);
		else
			return checkOneChild(root->right);
	}
	return true;
	
}
////Check if each internal node of a BST has exactly one child
//Input is an array of preorder traversal of BST
//e.g pre[] = {20, 10, 11, 13, 12}
bool checkOneChildOfBST(int pre[], int size)
{
	int max = pre[size - 1] > pre[ size - 2] ? pre[ size - 1] : pre[size - 2];
	int min = pre[size - 1] < pre[size - 2] ? pre[size - 1] : pre[size - 2];
	for (int i = size - 3; i >= 0; --i)
	{
		if (pre[i] < min)
			min = pre[i];
		else if (pre[i] > max)
			max = pre[i];
		else
			return false;
	}
	return true;
}
bool isSameBSTUtil(int a[], int b[], int n, int i1, int i2, int min, int max)
{
	int j, k;

	/* Search for a value satisfying the constraints of min and max in a[] and
	b[]. If the parent element is a leaf node then there must be some
	elements in a[] and b[] satisfying constraint. */
	for (j = i1; j<n; j++)
		if (a[j]>min && a[j]<max)
			break;
	for (k = i2; k<n; k++)
		if (b[k]>min && b[k]<max)
			break;
	cout << i1 << " " << j << endl;
	/* If the parent element is leaf in both arrays */
	if (j == n && k == n)
		return true;

	/* Return false if any of the following is true
	a) If the parent element is leaf in one array, but non-leaf in other.
	b) The elements satisfying constraints are not same. We either search
	for left child or right child of the parent element (decinded by min
	and max values). The child found must be same in both arrays */
	if (((j == n) ^ (k == n)) || a[j] != b[k])
		return false;
	/* Make the current child as parent and recursively check for left and right
	subtrees of it. Note that we can also pass a[k] in place of a[j] as they
	are both are same */
	return isSameBSTUtil(a, b, n, j + 1, k + 1, a[j], max) &&  // Right Subtree
		isSameBSTUtil(a, b, n, j + 1, k + 1, min, a[j]);    // Left Subtree
}

// A wrapper over isSameBSTUtil()
bool isSameBST(int a[], int b[], int n)
{
	return isSameBSTUtil(a, b, n, 0, 0, INT_MIN, INT_MAX);
}

// Driver program to test above functions
//int main()
//{
//	int a[] = { 8, 3, 6, 1, 4, 7, 10, 14, 13 };
//	int b[] = { 8, 10, 14, 3, 6, 4, 1, 7, 13 };
//	int n = sizeof(a) / sizeof(a[0]);
//	printf("%s\n", isSameBST(a, b, n) ?
//		"BSTs are same" : "BSTs not same");
//	system("pause");
//	return 0;
//}
//Check for Identical BSTs without building the trees
//bool checkIdenticalBST(int a[], int b[], int size, int l, int r, int max, int min)
//{
//	int i, j;
//	for (i = l; i < size; ++i)
//	{
//		if (a[i] < max && a[i] > min)
//			break;
//	}
//	for (j = r; j < size; ++j)
//		if (a[j] < max && a[j] > min)
//			break; 
//	return checkIdenticalBST(a, b, size, i + 1, j + 1, )
//}
int main()
{
	//Node *root = newNode(10);
	//root->left = newNode(6);
	//root->left->right = newNode(8);
	//root->left->right->right = newNode(9);
	//root->left->right->left = newNode(7);
	//if (checkOneChild(root))
	//	cout << "yes\n";
	unordered_map<int, int> temp;
	vector<int*> t;
	int* p = new int(3);
	t.push_back(p);
	t.pop_back();
	cout << *p;
	//k_th_SmallestElement(root, 3);
	
	system("pause");
	return 0;
}