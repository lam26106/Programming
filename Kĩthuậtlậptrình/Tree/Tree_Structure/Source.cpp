//#include <iostream>
//#include <vector>
//using namespace std;
//
//template<typename T>
//class gTree;
//template<typename T>
//class gTNode
//{
//private:
//	T data;
//	gTNode<T> *parent;
//	vector<gTNode<T>*> children;
//public:
//	gTNode()
//	{
//		parent = NULL;
//	}
//	gTNode(T _data, gTNode<T>* _parent)
//	{
//		data = _data;
//		parent = _parent;
//	}
//	T getData()
//	{
//		return data;
//	}
//	gTNode<T>* getParent()
//	{
//		return parent;
//	}
//	vector<gTNode<T>*>& getChildren()
//	{
//		return children;
//	}
//	void addChild(const T& _data)
//	{
//		gTNode<T>* temp = new gTNode<T>(_data, this);
//		children.push_back(temp);
//	}
//	~gTNode()
//	{
//		cout << endl << data;
//		for (int i = 0; i < children.size(); ++i)
//		{
//			delete children[i];
//			children[i] = NULL;
//		}
//		parent = NULL;
//	}
//	bool remove()
//	{
//		if (parent == NULL && children.size() > 0)
//		{
//			cout << "Can not remove the root that has children\n";
//			return 0;
//		}
//		else if (parent == NULL)
//		{
//			this->~gTNode();
//		}
//		else
//		{
//			parent->children.erase(remove(parent->children.begin(), parent->children.end(), this), parent->children.end());
//			while (!children.empty())
//			{
//				children[0]->parent = parent;
//				parent->children.push_back(children[0]);
//				children.erase(children.begin());
//			}
//			delete this;
//			return true;
//		}
//	}
//	friend class gTree<T>;
//};
//template<typename T>
//class gTree
//{
//private:
//	gTNode<T>* root;
//	void remove(gTNode<T>*  _root)
//	{
//		if (_root != NULL){
//			vector<gTNode<T>*> ch = _root->getChildren();
//			if (!ch.empty()){
//				for (int i = ch.size() - 1; i >= 0; i--)
//				{
//					remove(_root->getChildren()[i]);
//					_root->getChildren().erase(_root->getChildren().begin() + i);
//				}
//			}
//			delete _root;
//			root = NULL;
//		}
//
//	}
//public:
//	gTree() :root(NULL){}
//	bool addNode(gTNode<T>* _parent, T item)
//	{
//		if (_parent == NULL)
//		{
//			if (root == NULL)
//				root = new gTNode<T>(item, NULL);
//			else{
//				cout << "Tree already have root.\n";
//				return false;
//			}
//		}
//		else{
//			_parent->addChild(item);
//		}
//		return true;
//	}
//	gTNode<T>* getRoot()
//	{
//		return root;
//	}
//	bool isEmpty()
//	{
//		return root == NULL;
//	}
//
//	~gTree()
//	{
//		remove(root);
//	}
//};
//template <typename T>
//void preOrder(gTNode<T>* v)
//{
//	if (v == NULL)
//		return;
//	cout << v->getData(); // print element
//	vector<gTNode<T>*> ch = v->getChildren(); // list of children
//	for (int i = 0; i < ch.size(); i++) {
//		cout << " ";
//		preOrder(ch[i]);
//
//	}
//}
//int checkPrime(int n) {
//	int i;
//	int m;
//
//	if (n < 2)
//		return 0;
//
//	if (n == 2 || n == 3)
//		return 1;
//	if (n % 2 == 0 || n % 3 == 0)
//		return 0;
//
//	m = (int)sqrt(n);
//	for (i = 5; i <= m; i = i + 6)
//		if (n % i == 0 || n % (i + 2) == 0)
//			return 0;
//	return 1;
//}
//int main()
//{
//	//int a = 3;
//	////gTNode<int> *t = new gTNode<int>(a, NULL);
//	//gTree<int> test;
//	//test.addNode(NULL, a);
//	////test.getRoot()->addChild(4);
//	//preOrder(test.getRoot());
//	//test.~gTree();
//	////cout << test.getRoot()->getData();
//	if (checkPrime(23))
//		cout << "ahihihi\n";
//	system("pause");
//	return 0;
//}

#include<stdio.h>
#include<stdlib.h>
#define bool int

/* A binary tree tNode has data, pointer to left child
and a pointer to right child */
struct tNode
{
	int data;
	struct tNode* left;
	struct tNode* right;
};

/* Structure of a stack node. Linked List implementation is used for
stack. A stack node contains a pointer to tree node and a pointer to
next stack node */
struct sNode
{
	struct tNode *t;
	struct sNode *next;
};

/* Stack related functions */
void push(struct sNode** top_ref, struct tNode *t);
struct tNode *pop(struct sNode** top_ref);
bool isEmpty(struct sNode *top);

/* Iterative function for inorder tree traversal */
void inOrder(struct tNode *root)
{
	/* set current to root of binary tree */
	struct tNode *current = root;
	struct sNode *s = NULL;  /* Initialize stack s */
	bool done = 0;

	while (!done)
	{
		/* Reach the left most tNode of the current tNode */
		if (current != NULL)
		{
			/* place pointer to a tree node on the stack before traversing
			the node's left subtree */
			push(&s, current);
			current = current->left;
		}

		/* backtrack from the empty subtree and visit the tNode
		at the top of the stack; however, if the stack is empty,
		you are done */
		else
		{
			if (!isEmpty(s))
			{
				current = pop(&s);
				printf("%d ", current->data);

				/* we have visited the node and its left subtree.
				Now, it's right subtree's turn */
				current = current->right;
				if (current != NULL)
					printf("%d ", current->data);
			}
			else
				done = 1;
		}
	} /* end of while */
}

/* UTILITY FUNCTIONS */
/* Function to push an item to sNode*/
void push(struct sNode** top_ref, struct tNode *t)
{
	/* allocate tNode */
	struct sNode* new_tNode =
		(struct sNode*) malloc(sizeof(struct sNode));

	if (new_tNode == NULL)
	{
		printf("Stack Overflow \n");
		getchar();
		exit(0);
	}

	/* put in the data  */
	new_tNode->t = t;

	/* link the old list off the new tNode */
	new_tNode->next = (*top_ref);

	/* move the head to point to the new tNode */
	(*top_ref) = new_tNode;
}

/* The function returns true if stack is empty, otherwise false */
bool isEmpty(struct sNode *top)
{
	return (top == NULL) ? 1 : 0;
}

/* Function to pop an item from stack*/
struct tNode *pop(struct sNode** top_ref)
{
	struct tNode *res;
	struct sNode *top;

	/*If sNode is empty then error */
	if (isEmpty(*top_ref))
	{
		printf("Stack Underflow \n");
		getchar();
		exit(0);
	}
	else
	{
		top = *top_ref;
		res = top->t;
		*top_ref = top->next;
		free(top);
		return res;
	}
}

/* Helper function that allocates a new tNode with the
given data and NULL left and right pointers. */
struct tNode* newtNode(int data)
{
	struct tNode* tNode = (struct tNode*)
		malloc(sizeof(struct tNode));
	tNode->data = data;
	tNode->left = NULL;
	tNode->right = NULL;

	return(tNode);
}

/* Driver program to test above functions*/
int main()
{

	/* Constructed binary tree is
	1
	/   \
	2      3
	/  \
	4     5
	*/
	struct tNode *root = newtNode(1);
	root->left = newtNode(2);
	root->right = newtNode(3);
	root->left->left = newtNode(4);
	root->left->right = newtNode(5);

	inOrder(root);

	getchar();
	return 0;
}