#include <iostream>
using namespace std;


struct Node
{
	int data;
	Node *pLeft, *pRight;
};
typedef Node* Tree;
void insert(Tree &t, int DATA)
{
	if (t == nullptr)
	{
		Node* temp = new Node;
		temp->data = DATA;
		temp->pLeft = temp->pRight = nullptr;
		t = temp;
	}
	else
	{
		if (DATA < t->data)
			insert(t->pLeft, DATA);
		else if (DATA > t->data)
			insert(t->pRight, DATA);
	}
}
void createTree(Tree &t, int n, int count = 0)
{
	if (count != n)
	{
		int DATA;
		cout << "Insert an item to tree: ";
		cin >> DATA;
		insert(t, DATA);
		createTree(t, n, count + 1);
	}
}
void NLR(const Tree &t)
{
	if (t != nullptr)
	{
		cout << t->data << " "; 
		NLR(t->pLeft);
		NLR(t->pRight);
	}
}
void LNR(const Tree &t)
{
	if (t != nullptr)
	{
		LNR(t->pLeft);
		cout << t->data << " ";
		LNR(t->pRight);
	}
}
void LRN(const Tree &t)
{
	if (t != nullptr)
	{
		LRN(t->pLeft);
		LRN(t->pRight);
		cout << t->data << " ";
	}
}
void LietKeNodeCo2Con(const Tree &t)
{
	if (t != nullptr)
	{
		if (t->pLeft != nullptr & t->pRight != nullptr)
		{
			cout << t->data << " ";
		}
		LietKeNodeCo2Con(t->pLeft);
		LietKeNodeCo2Con(t->pRight);
	}
}
int DemCacNodeCo2Con(const Tree &t)
{
	if (t == nullptr)
		return 0;
	else if (t->pLeft == nullptr && t->pRight == nullptr)
		return 0;
	else if (t->pLeft == nullptr )
		return 0;
	else if (t->pLeft != nullptr )
		return 0;
	else
		return 1 + DemCacNodeCo2Con(t->pLeft) + DemCacNodeCo2Con(t->pRight);
		
		
}
void LietKeNode1Con(const Tree &t)
{
	if (t != nullptr)
	{
		if ((t->pLeft != nullptr && t->pRight == nullptr) || (t->pLeft == nullptr && t->pRight != nullptr))
		{
			cout << t->data << " ";
		}
		LietKeNode1Con(t->pLeft);
		LietKeNode1Con(t->pRight);
	}
}
int TongCacNode(const Tree &t)
{
	if (t == nullptr)
		return 0;
	if (t->pLeft == nullptr && t->pRight == nullptr)
		return t->data;
	else
		return t->data + TongCacNode(t->pLeft) + TongCacNode(t->pRight);
}
int Node_max(const Tree &t)
{
	if (t->pLeft == nullptr && t->pRight == nullptr)
		return t->data;
	else if (t->pLeft == nullptr)
	{
		int temp = Node_max(t->pRight);
		return t->data > temp ? t->data : temp;
	}
	else if (t->pRight == nullptr)
	{
		int temp = Node_max(t->pLeft);
		return t->data > temp ? t->data : temp;
	}
	else
	{
		int temp1 = Node_max(t->pLeft);
		int temp2 = Node_max(t->pRight);
		temp1 = temp1 > temp2 ? temp1 : temp2;
		return t->data > temp1 ? t->data : temp1;
	}


}
int MaxNode(Tree T) {
	if (T->pRight == NULL)
		return T->data;
	else
		MaxNode(T->pRight);
}

bool Search(const Tree& t, int x)
{
	if (t == nullptr)
		return false;
	else if (t->data == x)
		return true;
	else if (t->data < x)
		Search(t->pRight, x);
	else
		Search(t->pLeft, x);
}

//void Euler_tour(Tree root){
//	if (root == NULL)
//		return;
//
//	cout << root->data << " ";
//	if (root->pLeft){
//		Euler_tour(root->pLeft);
//		cout << root->data << " ";
//	}
//	if (root->pRight){
//		Euler_tour(root->pRight);
//		cout << root->data << " ";
//	}
//}
void Euler_tour(const Tree& t){
	if (t != nullptr)
	{
		cout << t->data << " ";
		if (t->pLeft){
			Euler_tour(t->pLeft);
			cout << t->data << " ";
		}
		if (t->pRight != nullptr){
			Euler_tour(t->pRight);
			cout << t->data << " ";
		}
	}
}
int descendants(const Tree &t, const Tree &root) 
{
	//if (t->pLeft && t->pRight)
	//	return 2 + descendants(t->pLeft) + descendants(t->pRight);
	//else if (t->pLeft)
	//	return 1 + descendants(t->pLeft);
	//else if (t->pRight)
	//	return 1 + descendants(t->pRight);
	//else
	//	return 0;
	if (t == nullptr)
		return 0;
	else if (t == root)
		return descendants(t->pLeft, root) + descendants(t->pRight, root);
	else 
		return 1 + descendants(t->pLeft, root) + descendants(t->pRight, root);


}
int main()
{
	//int count = 0;
	//Tree t = nullptr;
	//int n;
	//cin >> n;
	//createTree(t, n);
	////Euler_tour(t);
	////Euler_tour(t);
	//cout << descendants(t, t)


	int* a[2];
	system("pause");
	return 0;
}