#include <iostream>
using namespace std;


// Bai 759
//int Bai759(int n)
//{
//	int dem = 0;
//	if (n == 0)
//		return 0;
//	int temp = n;
//	while (temp != 0)
//	{
//		dem++;
//		temp /= 10;
//	}
//	//cout << dem << endl;
//	return Bai759(n / 10) + (n % 10) * pow(10.0, dem - 1 );
//}
int Bai759_v2(int n, int sum = 0)
{
	if (n == 0)
		return sum;
	return Bai759_v2(n / 10, sum * 10 + n % 10);
}
int main()
{
	cout << Bai759_v2(12345);
	system("pause");
	return 0;
}