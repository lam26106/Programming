#include <iostream>
using namespace std;

//Taho tac voi mang hoan toan bang de quy
//Nhap mang
void NhapMang(int *a, int size, int i = 0)
{
	if (i == size - 1)
	{
		cin >> a[i];
		return;
	}
	cin >> a[i];
	NhapMang(a, size, i + 1);

}
//Xuat mang
void XuatMang(int *a, int size, int i = 0)
{
	if (i == size - 1)
	{
		cout << a[i];
		return;
	}
	cout << a[i] << " ";
	XuatMang(a, size, i + 1);
}
//Tinh tong cac phan tu cua mang
int TongMang(int *a, int size)
{
	int i = size - 1;
	if (i == 0)
		return a[i];
	return TongMang(a, size - 1) + a[i];
}
//Tim Max cua mang
int FindMax(int *a, int size, int Max = INT_MIN)
{
	int i = size - 1;
	if (i == -1)
		return Max;
	if (a[i] > Max)
		Max = a[i];
	FindMax(a, size - 1, Max);
}
//Tim min
int FindMin(int *a, int size, int Min = INT_MAX)
{
	int i = size - 1;
	if (i == -1)
		return Min;
	if (a[i] < Min)
		Min = a[i];
	FindMin(a, size - 1, Min);
}
//Dem so luong cac so chan trong mang
int DemChan(int *a, int size)
{
	int i = size - 1;
	if (i == -1)
		return 0;
	if (a[i] % 2 == 0)
		return DemChan(a, size - 1) + 1;
	else
		return DemChan(a, size - 1);
}
//Sap xep mang tang dan
//insection sort
void AscendingSort(int *a, int size, int i = 1)
{
	if (i == size )
		return;
	int temp = a[i];
	int j = i;
	while (temp < a[j - 1] && j > 0)
	{
		a[j] = a[j - 1];
		j--;
	}
	a[j] = temp;
	cout << endl;
	XuatMang(a, 4);
	cout << endl;
	AscendingSort(a, size, i + 1);
}

//Thao tac tren mang 2 chieu
//Nhap mang
void NhapMang2D(int **a, int column, int row)
{
	int i = column - 1;
	int j = row - 1;
	if (j == -1)
		return;
	NhapMang2D(a, column, row - 1);
	for (int i = 0; i < column; ++i)
		cin >> a[j][i];
}
//Xuat mang 2D
void XuatMang2D(int **a, int column, int row)
{
	if ( row == 0)
		return;
	XuatMang2D(a, column, row - 1);
	for (int i = 0; i < column; ++i)
		cout << a[row - 1][i] << " ";
	cout << endl;
}
//Tong cac phan tu cua mang 2 chieu
int Tong2D(int **a, int column, int row)
{
	if (row == 0)
		return 0;
	int Tong = 0;
	for (int i = 0; i < column; ++i)
		Tong += a[row - 1][i];
	return Tong2D(a, column, row - 1) + Tong;
}
//Tim Min 2d
int TimMin2D(int **a, int column, int row, int i = 0, int Min = INT_MAX)
{
	if (i == column *row)
		return Min;
	if (a[i / column][i%column] < Min)
		Min = a[i / column][i%column];
	TimMin2D(a, column, row, i + 1, Min);

}
int main()
{
	//int a[4];
	//NhapMang(a, 4);
	//XuatMang(a, 4);
	//AscendingSort(a, 4);
	//cout << endl;
	//XuatMang(a, 4);
	
	int **a = new int *[2];
	for (int i = 0; i < 2; ++i)
		a[i] = new int[3];
	NhapMang2D(a, 3, 2);
	XuatMang2D(a, 3, 2);
	cout << TimMin2D(a, 3, 2);
	for (int i = 0; i < 2; ++i)
		delete[]a[i];
	delete[]a;
	system("pause");
	return 0;
}