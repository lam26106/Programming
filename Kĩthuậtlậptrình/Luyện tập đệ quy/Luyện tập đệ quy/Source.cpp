#include <iostream>
using namespace std;


//Tinh S = 1^2 + 2^2 + 3^2 + .... + n^2
int TongBinhPhuong(int n)
{
	if (n == 1)
		return 1;
	return n * 2 + TongBinhPhuong(n - 1);
}
//Tinh S = 1 + 1/2 + 1/3 + 1/4 + ... + 1/n
float TongPhanSo1(int n)
{
	if (n == 1)
		return 1.0;
	return 1.0 / n + TongPhanSo1(n - 1);
}

//Tinh S = 1/2 + 1/4 + .. + 1/2n
float TongPhanSo2(int n)
{
	if (n == 1)
		return 0.5;
	return 1.0 / (n * 2) + TongPhanSo2(n - 1);
}
//Tinh S = 1 + 1/3 + 1.5 + ... + 1/(2n+1)
float TongPhanSo3(int n)
{
	if (n == 0)
		return 1.0;
	return 1.0 / (2 * n + 1) + TongPhanSo3(n - 1);
}
//Tinh T(x,n) = x^n
float TongMu(int x, int n)
{
	if (n > 0){
		if (n == 0)
			return 1;
		return x * TongMu(x, n - 1);
	}
	else
	{
		if (n == 0)
			return 1;
		return 1.0 / x * TongMu(x, n + 1);
		
	}
}
//Tinh giai thua
int giaithua(int n)
{
	if (n == 0)
		return 1;
	return n * giaithua(n - 1);
}
//Tinh S = 1 + 1.2 + 1.2.3 + .... + 1.2....n
int Tong1(int n)
{
	if (n == 1)
		return 1;
	return giaithua(n) + Tong1(n - 1);
}

//Tinh S = x + x^2 + x^3 + .. + x^n
float TongMu2(int x, int n)
{
	if (n == 0)
		return x;
	return TongMu(x, n) + TongMu2(x, n - 1);
}
//Tinh S = x + x^2 / 2! + x^3/3! +...+ x^n/n!

float Tong2(int x, int n)
{
	if (n == 0)
		return x;
	return TongMu(x, n) / giaithua(n) + Tong2(x, n - 1);
}
int main()
{
	int n = 3;
	cout << TongMu(2, -3);
	system("pause");
	return 0;
}