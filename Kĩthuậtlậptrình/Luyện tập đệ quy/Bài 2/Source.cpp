#include <iostream>
using namespace std;

//Bai 748 trong file bai tap
float Bai748(int n)
{
	if (n == 1)
		return sqrt(2);
	return sqrt(2 + Bai748(n - 1));
}

//Bai 749 trong file bai tap
float Bai749(int n)
{
	if (n == 1)
		return 1;
	return sqrt(n + Bai749(n - 1));
}
// Bai 750
float Bai750(int n, int i = 1)
{
	if (i == n)
		return sqrt((float)n);
	return sqrt((float)(i + Bai750(n, i + 1)));
}
// Bai 751
//float Bai751(int n)
//{
//	if (n == 0)
//		return 1;
//	return 1.0 / (1 + Bai751(n - 1));
//}
float Bai751_v2(int n, float sum = 1)
{
	if (n == 0)
		return sum;
	return Bai751_v2(n - 1, 1.0 / (1 + sum));
}
//Bai 752
int Bai752(int n, int x = 1)
{
	if (n < 10)
		return x;
	return Bai752(n / 10, x + 1);
}
//Bai 753
int Bai753(int n, int x = 0)
{
	if (n == 0)
		return x;
	return Bai753(n / 10, x + n % 10);
}

//Bai 755
int Bai755(int n, int sum = 0)
{
	if (n == 0)
		return sum;
	if (n % 10 % 2 != 0)
		sum++;
	return Bai755(n / 10, sum);
}

//Bai 756
int Bai756(int n)
{
	if (n == 0)
		return 0;
	if (n % 10 % 2 == 0)
		return n % 10 + Bai756(n / 10);
	else
		return Bai756(n / 10);
}
int main()
{
	cout.setf(ios::fixed);
	cout.setf(ios::showpoint);
	cout.precision(10);
	cout << Bai751(3);
	system("pause");
	return 0;
}