﻿#include<iostream>
#include<set>
#include<vector>
#include<string>
#include<algorithm>
using namespace std;

vector<set<int>> printSubSet_v1(int n)
{
	vector<set<int>> a;
	set<int> b;
	if (n == 0)
	{
		a.push_back(b);
		return a;
	}
	else
	{
		vector<set<int>> a = printSubSet_v1(n - 1);
		for (int i = 0; i < a.size(); ++i)
		{
			b = a[i];
			b.insert(n);
			a.push_back(b);
		}
		return a;
	}

}
void printSubSet_v2(int P, string S, string V, int j)
{
	if (P > 0)
	{
		for (int i = j; i < P; ++i)
		{
			char tmp = V[i];
			S = S + tmp;
			V.erase(V.begin() + i);
			cout << S << endl;
			printSubSet_v2(P - 1, S, V, i);
			V.insert(V.begin() + i, tmp);
			S.erase(S.begin() + S.length() - 1);
		}
	}
}
//Print all subset version bá đạo của anh Kiên
//int n;
//bool a[100];
//
//void show(){
//	for (int i = 1; i <= n; i++)
//		if (a[i])
//			printf("%d ", i);
//	printf("\n");
//}
//
//void bt(int u){
//	if (u == n + 1) {
//		show();
//		return;
//	}
//	a[u] = 0;
//	bt(u + 1);
//	a[u] = 1;
//	bt(u + 1);
//}
//int main(){
//	cin >> n;
//	bt(1);
//	return 0;
//}
int base = 0;
void subset_v3(int n)
{
	if (base == pow(2.0, n))
	{
		return;
	}
	int k = 1;
	for (int j = 0; j < n; ++j)
	{
		int test = k & base;
		if (test != 0)
			cout << j + 1;
		k = k << 1;
	}
	cout << endl;
	base++;
	subset_v3(n);
}
vector<int> used;
void show(vector<int> a)
{
	for (int i : a)
		cout << i << " ";
	cout << endl;
}

void sub_permute(vector<int> a, int n = 0)
{
	if (n == used.size())
		show(a);
	else
	{
		for (int i = 0; i < used.size(); ++i)
		{
			if (used[i] == 1)
			{
				a[n] = i + 1;
				used[i] = 0;
				sub_permute(a, n + 1);
				used[i] = 1;
			}
		}
	}
}
int c = 0;

int n;
string g = "";
void recur(int i = 0, int j = 3, int blank = n)
{
	if (i < n && j < n*2 +1){
		recur(i + 1, j + 1);
		swap(g[i], g[blank]);
		cout << g << endl;
		c++;
		swap(i, blank);
		int k;
		for (int count = 0; count < n; count++){
			k = blank;
			while (g[k] != 'W') k++;
			swap(g[k], g[blank]);
			cout << g << endl;
			c++;
			blank = k;
		}
		swap(g[i], g[blank]);
		cout << g << endl;
		c++;
		cout << endl;

	}
}
int main()
{
	
	cin >> n;
	int i = 0;
	while (i < n)
	{
		g = g + "G";
		i++;
	}
	g = g + "B";
	i++;
	while (i <= n*2)
	{
		g = g + "W"; 
		i++;
	}
	cout << g << endl;
	recur();
	cout << c;

	system("pause");
	return 0;
}