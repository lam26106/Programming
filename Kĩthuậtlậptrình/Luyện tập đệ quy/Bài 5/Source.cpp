#include <iostream>
using namespace std;


int addInterger_v1(int x, int y)
{
	if (y == x)
		return x;
	else
		return y + addInterger_v1(x, y - 1);
}
int addInterger_v2(int x, int y, int tong = 0)
{
	if (y < x)
		return tong;
	else
		addInterger_v2(x, y - 1, tong + y);
}
void print(int n)
{
	if (n == 0)
		cout << n << " ";
	else
	{
		print(n - 1);
		cout << n << " ";
	}
}
void printOdd(int n)
{
	//if (n == 1)
	//	cout << n << " ";
	//else if (n % 2 != 0)
	//{
	//	printOdd(n - 1);
	//	cout << n << " ";
	//}
	//else
	//	printOdd(n - 1);
	if (n % 2 == 0)
		n = n - 1;
	if (n == 1)
		cout << n << " ";
	else
	{
		printOdd(n - 2);
		cout << n << " ";
	}
}
int BinarySearch(int *a, int target, int start, int last, int index = -1)
{
	int middle = (start + last) / 2;
	if (start > last)
		return index;
	if (a[middle] == target)
	{
		index = middle;
		return index;
	}
	else if (a[middle] > target)
		BinarySearch(a, target, start, middle - 1, index);
	else 
		BinarySearch(a, target, middle + 1, last, index);

}
void Merge(int *a, int start, int middle, int last)
{
	int *temp = new int[last - start + 1];
	int k = 0, i = start, j = middle + 1;
	while (i <= middle  && j <= last)
	{
		if (a[i] >= a[j])
			temp[k++] = a[j++];
		else
			temp[k++] = a[i++];
	}
	while (i <= middle)
		temp[k++] = a[i++];
	while (j <= last)
		temp[k++] = a[j++];
	for (int i = start, k = 0; i <= last; ++i, ++k)
		a[i] = temp[k];
	delete[] temp;
}
void MergeSort(int*a, int start, int last)
{
	if (start >= last)
		return;
	else
	{
		int middle = (start + last) / 2;
		MergeSort(a, start, middle);
		MergeSort(a, middle + 1, last);
		Merge(a, start, middle, last);
	}
}
void NhapMang(int *a, int size, int index = 0)
{
	if (index >= size)
		return;
	else
	{
		cout << "Nhap vao phan tu " << index + 1 << ": ";
		cin >> a[index];
		NhapMang(a, size, index + 1);
	}
}
void XuatMang(int*a, int size)
{
	if (size == 1)
		cout << a[size - 1] << " ";
	else
	{
		XuatMang(a, size - 1);
		cout << a[size - 1] << " ";
	}
}
int main()
{
	int x = 1, y = 5;
	//cout << addInterger_v2(x, y);
	//print(5);
	//printOdd(10);
	//int a[] = { 10,9,8,7,6,5,4,3,2,1};
	//cout << BinarySearch(a, 9, 0, 9);
	//MergeSort(a, 0, 9);
	//for (int i = 0; i < 10; ++i)
	//	cout << a[i] << " ";
	int a[10];
	NhapMang(a, 10);
	cout << endl;
	XuatMang(a, 10);
	system("pause");
	return 0;
}