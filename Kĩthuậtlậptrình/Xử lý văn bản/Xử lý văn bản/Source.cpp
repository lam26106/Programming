#include <iostream>
#include <fstream>
#include <cstring>
#include <Windows.h>
#include <conio.h>
#pragma warning (disable:4996)
using namespace std;

bool checkUpperChar(char* s)
{
	for (int i = 0; s[i] != '\n'; ++i)
	{
		if (s[i] < 'A' || s[i] > 'Z')
			return false;
	}
	return true;
}
void swap(int &a, int&b)
{
	int temp = a;
	a = b;
	b = temp;
}
char* AddString(char*s1, char*s2)
{
	char* s3 = new char[strlen(s1) + strlen(s2) + 2];
	int i = 0;
	for (int j = 0; j < strlen(s1); ++j)
		s3[i++] = s1[j];
	s3[i++] = ' ';
	for (int j = 0; j < strlen(s2); ++j)
		s3[i++] = s2[j];
	s3[i] = '\0';
	return s3;
}
void StringAppend(char*&s1, char c)
{
	int index = strlen(s1);
	s1[index] = c;
	s1[index + 1] = '\0';
}
void StringReduce(char*&s1)
{
	int index = strlen(s1);
	s1[index - 1] = '\0';
}
void UpdateFreOfOneGram(int *&fre, int size, char**s)
{
	for (int i = 0; i < size - 1; ++i)
	{
		if (fre[i] != -1)
			for (int j = i + 1; j < size; ++j)
			{
				if (fre[j] != -1 && strcmp(s[i], s[j]) == 0)
				{
					fre[i]++;
					fre[j] = -1;

				}
			}
	}
}
void UpdateFreOfTwoGram(int *&fre, int size, char**s)
{
	for (int i = 0; i < size - 1; ++i)
	{
		if (fre[i] != -1)
		{
			for (int j = i + 1; j < size; ++j)
			{
				if (strcmp(s[i], s[j]) == 0 && fre[j] != -1)
				{
					fre[i]++;
					fre[j] = -1;
				}
			}
		}
	}
}

void sortAndupdateOneGram(int *&fre, int &size, char**s)
{
	for (int i = 0; i < size - 1; ++i)
	{
		for (int j = i + 1; j < size; ++j)
		{
			if (fre[j] > fre[i])
			{
				swap(fre[j], fre[i]);
				char*temp = s[j];
				s[j] = s[i];
				s[i] = temp;
			}
		}
	}
	for (int j = size - 1; j >= 0; --j)
	{
		if (fre[j] == -1){
			delete[] s[j];
			size--;
		}
	}
}
void sortAndUpdateTwoGram(int*&fre, int &size, char**s)
{
	for (int i = 0; i < size - 1; ++i)
	{
		for (int j = i + 1; j < size; ++j)
		{
			if (fre[j] > fre[i])
			{
				swap(fre[j], fre[i]);
				char*temp = s[i];
				s[i] = s[j];
				s[j] = temp;
			}
		}
	}
	for (int i = size - 1; i >= 0; --i)
	{
		if (fre[i] == -1)
		{
			delete[] s[i];
			size--;
		}
	}
}
void UpdateOneGram(int*&fre, int &size, char** s)
{
	int j = size - 1;
	for (int i = size - 1; i >= 0; --i)
	{
		if (fre[i] < 3 && checkUpperChar(s[i]) == false)
		{

			swap(fre[i], fre[j]);
			char*temp = s[j];
			s[j] = s[i];
			s[i] = temp;
			delete[] s[j];
			j--;
			size--;
		}
	}
}
void UpdateTwoGram(int *&fre, int &size, char**s)
{
	int j = size - 1;
	for (int i = size - 1; i >= 0; --i)
	{
		if (fre[i] < 3 && checkUpperChar(s[i]) == false)
		{
			swap(fre[i], fre[j]);
			char*temp = s[j];
			s[j] = s[i];
			s[i] = temp;
			delete[] s[j];
			j--;
			size--;
		}
	}
}
bool myCheckSubString(char *s1, char *s2)
{
	for (int i = 0; i < strlen(s1); ++i)
	{
		if (s1[i] != s2[i])
			return false;
	}
	return true;
}
int main()
{
	int *FreOfOneGram = new int[1000];
	int *FreOfTwoGram = new int[999];

	for (int i = 0; i < 1000; ++i)
		FreOfOneGram[i] = 1;
	for (int i = 0; i < 999; ++i)
		FreOfTwoGram[i] = 1;

	char **OneGram = new char*[1000];
	char **TwoGram = new char*[999];

	for (int i = 0; i < 1000; ++i)
		OneGram[i] = new char[30];
	for (int i = 0; i < 999; ++i)
		TwoGram[i] = new char[40];

	ifstream FileIn("INPUT.txt");
	int i = 0;
	while (!FileIn.eof())
	{
		FileIn >> OneGram[i];
		++i;
	}

	int sizeOfOneGram = i - 1;
	int sizeOfTwoGram = sizeOfOneGram - 1;

	for (int j = 0; j < sizeOfTwoGram; ++j)
	{
		TwoGram[j] = AddString(OneGram[j], OneGram[j + 1]);
	}


	UpdateFreOfOneGram(FreOfOneGram, sizeOfOneGram, OneGram);
	UpdateOneGram(FreOfOneGram, sizeOfOneGram, OneGram);
	sortAndupdateOneGram(FreOfOneGram, sizeOfOneGram, OneGram);

	UpdateFreOfTwoGram(FreOfTwoGram, sizeOfTwoGram, TwoGram);
	UpdateTwoGram(FreOfTwoGram, sizeOfTwoGram, TwoGram);
	sortAndUpdateTwoGram(FreOfTwoGram, sizeOfTwoGram, TwoGram);

	//cout << endl;
	cout << "One Gram: ";
	for (int j = 0; j < sizeOfOneGram; ++j)
		cout << OneGram[j] << " ";
	cout << endl;
	cout << "Two gram: ";
	for (int j = 0; j < sizeOfTwoGram; ++j)
		cout << "(" << TwoGram[j] << ")" ;
	cout << endl;
	int point = 0;
	char key;
	char*temp = new char[100];
	char*temp1 = new char[100];
	strcpy(temp, "");
	strcpy(temp1, "");
	int track = 0;
	cout << "Type here: ";
	do
	{
		int lazada;
		int maxOflazada = 0;
		int flag = 0;

		strcpy(temp1, temp);
		key = getch();
		if (key == 13)
			break;
		if (key == 8)
		{
			cout << "\b" << " " << "\b";
			StringReduce(temp);
			//StringReduce(temp1);
			continue;
		}
		else if (GetAsyncKeyState(VK_LEFT))
		{
			cout << "\b";
			continue;
		}
		else if (GetAsyncKeyState(VK_SPACE))
		{
			char *temp3 = new char[100];
			bool check = false;
			cout << " ";
			strcpy(temp, "");
			StringAppend(temp1, ' ');
			for (int i = 0; i < sizeOfTwoGram; ++i)
			{
				if (myCheckSubString(temp1, TwoGram[i]) )
				{
					check = true;
					strcpy(temp3, TwoGram[i]);
					break;
				}
			}
			if (check == true && point % 2 == 0)
			{
				for (int j = strlen(temp1); j < strlen(temp3); ++j)
				{
					cout << temp3[j];
					StringAppend(temp, temp3[j]);
				}
				strcpy(temp1, temp);
				delete[] temp3;
				point++;
			}
			else if (check == true && point % 2 != 0)
			{
				point++;
			}
			else if (check == false && point % 2 != 0)
				point++;
			else if (check == false )
			{
				strcpy(temp1, temp);
			}
			continue;

		}

		else
		{
			cout << key;
			char a = key;
			StringAppend(temp, a);
			//StringAppend(temp1, a);
		}
		int dem = strlen(temp);
		for (int i = 0; i < sizeOfOneGram; ++i)
		{
			lazada = 0;
			if (temp[0] == OneGram[i][0])
			{
				lazada++;
				for (int j = 1; j < strlen(OneGram[i]) && j < strlen(temp); ++j)
				{
					if (temp[j] == OneGram[i][j])
						lazada++;
					else{
						lazada--;
						break;
					}
				}

				if (maxOflazada < lazada){
					maxOflazada = lazada;
					flag = i;
				}
			}
			else
				continue;
		}
		if (maxOflazada >= dem && dem < strlen(OneGram[flag]))
		{
			int step = 0;
			for (int i = dem; i < strlen(OneGram[flag]); ++i)
			{
				cout << OneGram[flag][i];
				StringAppend(temp, OneGram[flag][i]);
				//StringAppend(temp1, OneGram[flag][i]);
				step++;
			}
		}
	} while (true);
	system("pause");
	return 0;
}