#include <iostream>
#include <fstream>
#include <stdlib.h>
using namespace std;
void NhapMang(int *a, int n)
{
	for (int i = 0; i < n; i++)
	{
		cout << "Nhap vao a[" << i << "]";
		cin >> *(a + i);
	}
}
void XuatMang(int *a, int n)
{
	for (int i = 0; i < n; i++)
	{
		cout << a[i] << "    ";
	}
}
void NhapTapTin(ofstream &FileOut, int *a, int n)
{
	FileOut.open("TapTin.DAT", ios::out | ios::binary);

	FileOut.write((char*)a, n*sizeof(int));

	FileOut.close();
}
void DocFile(ifstream &FileIn, int *&a, int &n)
{
	FileIn.open("TapTin.DAT", ios::in | ios::binary);
	if (!FileIn)
	{
		cout << "File khong ton tai.Xin kiem tra lai!";
		system("pause");
		exit(0);// nam trong stdlib.h or windows.h
	}
	FileIn.seekg(0, ios::end);
	//n = (((int)FileIn.tellg() + 1) / sizeof(int));
	n = FileIn.tellg();
	cout << n << endl;
	//FileIn.seekg(0, ios::beg);
	//a = new int[n];
	//FileIn.read((char*)a, n*sizeof(int));
	delete[] a;
	FileIn.close();
}
int main()
{
	int *a;
	int n = 5;
	a = new int[n];
	ifstream FileIn;
	ofstream FileOut;
	NhapMang(a, n);
	NhapTapTin(FileOut, a, n);
	XuatMang(a, n);
	DocFile(FileIn, a, n);
	
	//ofstream FileOut;
	//int n = 5;
	//a = new int[n];
	//NhapMang(a, n);
	//NhapTapTin(FileOut, a, n);

	//delete[]a;
	system("pause");
	return 0;
}