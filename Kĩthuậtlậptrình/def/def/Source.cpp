#include <iostream>
using namespace std;

class PhanSo
{
private:
	int tu, mau;
public:
	PhanSo();
	~PhanSo();
	PhanSo(int, int);
	void Nhap();
	void Xuat();
	PhanSo operator + (PhanSo);
	PhanSo operator + (int);
	PhanSo operator += (PhanSo);
	PhanSo operator = (const PhanSo);
	int get_tu();
	int get_mau();
	void set_tu(int);
	void set_mau(int);
	friend PhanSo operator + (int, PhanSo);
	friend istream& operator >> (istream&, PhanSo &);
	friend ostream& operator << (ostream&, PhanSo);
	

};
PhanSo::PhanSo()
{
	tu = 0;
	mau = 1;
}
PhanSo::PhanSo(int x, int y)
{
	tu = x;
	mau = y;
}
PhanSo::~PhanSo()
{}

int PhanSo::get_mau()
{
	return mau;
}
int PhanSo::get_tu()
{
	return tu;
}
void PhanSo::set_mau(int a)
{
	mau = a;
}
void PhanSo::set_tu(int n)
{
	tu = n;
}
void PhanSo::Nhap()
{
	cout << "\nNhap vao tu va mau: ";
	cin >> tu >> mau;
}
void PhanSo::Xuat()
{
	cout << "\n" << tu << "/" << mau;
}
//operator overloading
PhanSo PhanSo::operator + (PhanSo ps)
{
	PhanSo tong(tu * ps.mau + mau * ps.tu, mau * ps.mau);
	return tong;
}
PhanSo PhanSo::operator + (int a)
{
	PhanSo ps(a, 1);
	return *this + ps;
}
PhanSo operator + (int a, PhanSo ps)
{
	//return ps + a;
	PhanSo tong;
	tong.tu = ps.tu + ps.mau * a;
	tong.mau = ps.mau;
	return tong;
}
PhanSo PhanSo::operator += (PhanSo ps)
{
	*this = *this + ps;
	return *this;
}
PhanSo PhanSo::operator = (const PhanSo ps)
{
	tu = ps.tu;
	mau = ps.mau;
	return *this;
}
// Toan tu nhap va xuat
istream& operator >> (istream& is, PhanSo &ps)
{
	is >> ps.tu >> ps.mau;
	return is;
}
ostream& operator << (ostream& os, PhanSo ps)
{
	os << ps.tu << "/" << ps.mau;
	return os;
}
int main()
{
	//PhanSo ps1(3, 4), ps2(5, 6);
	//int a = 3;
	//ps1.Xuat();
	//ps2.Xuat();
	//PhanSo Tong = ps1 + a;
	//Tong.Xuat();

	PhanSo p1, p2;
	cin >> p1;
	p2 = p1;
	cout << p1 << p2;

	
	system("pause");
	return 0;
}