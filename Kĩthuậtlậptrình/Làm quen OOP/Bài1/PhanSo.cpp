#include "PhanSo.h"

int PhanSo::GCD(int ts, int ms)
{
	if (ts < 0)
	{
		ts *= -1;
	}
	if (ms < 0)
	{
		ms *= -1;
	}
	int num1 = ts > ms ? ts : ms;
	int num2 = ts < ms ? ts : ms;
	return num2 == 0 ? num1 : GCD(num2, num1%num2);
}
void PhanSo::Nhap()
{
	cout << "\nNhap TS: ";
	cin >> TS;
	do{
		cout << "\nMS phai khac 0: ";
		cout << "\nNhap MS: ";
		cin >> MS;
	} while (MS == 0);
	int gcd = GCD(TS, MS);
	TS /= gcd;
	MS /= gcd;
}
void PhanSo::Xuat()
{
	cout << TS << "/" << MS;
	if (Check() == 1)
		cout << "Phan so bang 0";
	else if (Check() == 0)
		cout << "Phan so be hon 0";

}
int PhanSo::Check()
{
	if (TS == 0)
		return 1;
	if (float(TS) / MS < 0)
		return 0;
	return -1;
}