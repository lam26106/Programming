#include <iostream>
#include <cstring>
#include <ctime>
#pragma warning(disable:4996)
const int secondInAYear = 21600000;
using namespace std;

 struct item
{
	char *key = nullptr;
	char *value = nullptr;
	int expiry;
	int inputDay, inputMonth, inputYear, inputHour, inputMin, inputSecond;
	int Second_by_year;
};
typedef struct item ITEM;
void copyItem(ITEM& s1, ITEM &s2)
{
	if (s1.key != nullptr)
		delete s1.key;
	if (s1.value != nullptr)
		delete s1.value;
	s1.key = new char;
	s1.value = new char;
	s1.key = strdup(s2.key);
	s1.value = strdup(s2.value);
	s1.expiry = s2.expiry;
	s1.inputDay = s2.inputDay;
	s1.inputHour = s2.inputHour;
	s1.inputMin = s2.inputMin;
	s1.inputMonth = s2.inputMonth;
	s1.inputSecond = s2.inputSecond;
	s1.inputYear = s2.inputYear;
	s1.Second_by_year = s2.Second_by_year;
}
void addItemToList(ITEM *&list, ITEM &temp, int &size)
{
	if (list == nullptr)
	{
		list = new ITEM[1];
		
		copyItem(list[0], temp);
	
		size = 1;
		return;
	}
	ITEM* temp2 = new ITEM[size + 1];
	for (int i = 0; i < size; ++i)
	{
		copyItem(temp2[i], list[i]);
	}
	copyItem(temp2[size], temp);
	delete list;
	list = temp2;
	size++;
}
int computeSecond(int mon, int day, int hour, int min, int sec);
void storeData(ITEM *&list, int &size)
{
	ITEM temp;
	temp.key = new char;
	temp.value = new char;
	fflush(stdin);
	printf("\nEnter your key: ");
	gets(temp.key);
	fflush(stdin);
	printf("\nEnter your variable: ");
	gets(temp.value);
	fflush(stdin);
	do
	{
		printf("\nEnter expire time: ");
		scanf("%d", &temp.expiry);
	} while (temp.expiry > 2592000);
	time_t t = time(0);   // get time now
	struct tm * now = localtime(&t);
	temp.inputDay = now->tm_mday;
	temp.inputYear = now->tm_year + 1900;
	temp.inputMonth = now->tm_mon + 1;
	temp.inputHour = now->tm_hour;
	temp.inputMin = now->tm_min;
	temp.inputSecond = now->tm_sec;
	temp.Second_by_year = computeSecond(temp.inputMonth, temp.inputDay, temp.inputHour, temp.inputMin, temp.inputSecond);
	printf("\nYour input time is: %d/%d/%d. %d:%d:%d    %d", temp.inputYear, temp.inputMonth, temp.inputDay, temp.inputHour, temp.inputMin, temp.inputSecond, temp.Second_by_year);
	addItemToList(list, temp, size);

}
bool checkExpiridate(ITEM*&, ITEM &, int &);
void retrieveData(ITEM*&list, int &size, char*keyInput)
{
	for (int i = 0; i < size; ++i)
	{
		if (strcmp(list[i].key, keyInput) == 0 && checkExpiridate(list, list[i], size) == true)
		{
			printf("\nYour data is: %s", list[i].value);
			return;
		}
	}
	printf("\nItem not found");
}
void deleteItem(ITEM*&list, int &size, char*keyInput)
{
	for (int i = 0; i < size; ++i)
	{
		if (strcmp(list[i].key, keyInput) == 0)
		{
			for (int j = i; j < size - 1; ++j)
			{
				copyItem(list[j], list[j + 1]);
			}
			ITEM *temp = new ITEM[size - 1];
			for (int i = 0; i < size - 1; ++i)
			{
				copyItem(temp[i], list[i]);
				delete list;
				list = temp;
			}
			size--;
			printf("\nDelete success");
			return;
		}
	}
	printf("\nItem not found");
}
int computeSecond(int mon, int day, int hour, int min, int sec)
{
	int currentsecond = 0;
	for (int i = 1; i < mon; ++i)
	{
		if (i == 2)
			currentsecond += 86400 * 29;
		else if ((i <= 7 && i % 2 != 0) || (i > 7 && i % 2 == 0))
		{
			currentsecond += 86400 * 31;
		}
		else if ((i <= 7 && i % 2 == 0) || (i > 7 && i % 2 != 0))
		{
			currentsecond += 86400 * 30;
		}
	}
	currentsecond += 86400 * (day - 1) + 3600 * hour + 60 * min + sec;
	return currentsecond;
}
bool checkExpiridate(ITEM*&list, ITEM &item, int &size)
{
	time_t t = time(0);   // get time now
	struct tm * now = localtime(&t);


	int currentsecond = computeSecond(now->tm_mon + 1, now->tm_mday, now->tm_hour, now->tm_min, now->tm_sec);
	int inputsecond = item.Second_by_year;
	if (now->tm_year + 1900 == item.inputYear)
	{
		if (currentsecond - inputsecond > item.expiry)
		{
			deleteItem(list, size, item.key);
			return false;
		}
	}
	else if (now->tm_year + 1900 > item.inputYear)
	{
		currentsecond += secondInAYear;
		if (currentsecond - inputsecond > item.expiry)
		{
			deleteItem(list, size, item.key);
			return false;
		}
	}
	cout << currentsecond << " " << inputsecond;
	return true;
}
int main()
{
	ITEM*list = nullptr;
	int size = 0;
	printf("\n------------Memcache-----------\n");
	printf("\n1. Store data");
	printf("\n2. Retrieve item");
	printf("\n3. Delete item");
	printf("\n4. Flush all existing items");
	printf("\n5. Exit program\n\n");
	int choice;
	do
	{
		printf("\nEnter your choice: ");
		scanf("%d", &choice);
		if (choice == 1)
		{
			storeData(list, size);
		}
		else if (choice == 2)
		{
			char*keyInput = new char;
			fflush(stdin);
			printf("Enter you key: ");
			gets(keyInput);
			retrieveData(list, size, keyInput);
		}
		else if (choice == 3)
		{
			char*keyInput = new char;
			fflush(stdin);
			printf("Enter you key: ");
			gets(keyInput);
			deleteItem(list, size, keyInput);
		}

	} while (choice != 5);
	
	system("pause");
	return 0;
}