#include "ToaDo.h"

int main()
{
	ToaDo d1, d2;
	d1.Nhap();
	d2.Nhap();
	d1.XuatToaDo(cout);
	d2.XuatToaDo(cout);
	d1.TinhKhoangCach(d2);
	d1.XuatKhoangCach(d2, cout);
	system("pause");
	return 0;
}