#pragma once
#include <iostream>
#include <string>
using namespace std;
class ToaDo
{
private:
	double x, y;
public:
	void Nhap();
	void XuatToaDo(ostream& outs);
	void XuatKhoangCach(ToaDo, ostream&);
	float TinhKhoangCach(ToaDo);
};

