#include "ToaDo.h"


void ToaDo::Nhap()
{
	cout << "\nNhap vao hoanh do: ";
	cin >> x;
	cout << "\nNhap vao tung do: ";
	cin >> y;
}
void ToaDo::XuatToaDo(ostream &outs)
{

	outs << "\nToa do cua diem vua nhap la: (" << x << ", " << y << ")";
}
float ToaDo::TinhKhoangCach(ToaDo d2)
{
	float kc = sqrt((this->x - d2.x) * (this->x - d2.x) + (this->y - d2.y)*(this->y - d2.y));
	return kc;
}
void ToaDo::XuatKhoangCach(ToaDo d2, osstream &outs)
{
	float kc = this -> TinhKhoangCach(d2);
	outs << "Khoang cach cua 2 diem la: " << kc;
}

