#include <iostream>
#include <fstream>
#include <string>
using namespace std;

int main()
{
	int n;
	cin >> n;
	ofstream FileOut("multiplication.txt");
	for (int i = 1; i <= n; ++i)
	{
		for (int j = 1; j <= n; ++j)
		{
			FileOut << i*j;
		}
		FileOut << endl;
	}
	system("pause");
	return 0;
}