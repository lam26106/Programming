#include <iostream>
#include <fstream>
#include <string>
using namespace std;

int main()
{
	ifstream FileIn; // khai bao tap tin de doc
	ofstream FileOut;// khai bao tap tin de ghi
	FileIn.open("INPUT.txt", ios_base::in);
	if (!FileIn)
	{
		cout << "Khong tim thay tap tin";
		system("pause");
		return 0;
	}
	//
	string ten;
	int ngay, thang, nam;
	float toan, ly, hoa;
	getline(FileIn, ten, '-');
	ten.erase(ten.begin() + ten.length() - 1);

	FileIn >> ngay;
	FileIn.seekg(1, SEEK_CUR);
	FileIn >> thang;
	FileIn.seekg(1, SEEK_CUR);
	FileIn >> nam;
	FileIn.seekg(3, SEEK_CUR);
	FileIn >> toan;
	FileIn.seekg(3, SEEK_CUR);
	FileIn >> ly;
	FileIn.seekg(3, SEEK_CUR);
	FileIn >> hoa;
	FileIn.close();
	FileOut.close();
	cout << ten << " " << ngay << "/" << thang << "/" << nam << endl;
	cout << toan << " " << ly << " " << hoa;
	system("pause");
	return 0;
}