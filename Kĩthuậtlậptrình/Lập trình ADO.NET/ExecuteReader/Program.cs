﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExecuteReader
{
    class Program
    {
        static void Main(string[] args)
        {
            //B1: Connect to Database
            SqlConnection connect = new SqlConnection(@"Data Source=.\SQLEXPRESS;Initial Catalog=QuanLyHS;Integrated Security=True");

            try
            {
                connect.Open();
                string sql1 = @"Select COUNT(*)
                                from HocSinh hs
                                where hs.MaMon = 'CTDL'";


                SqlCommand cmd = new SqlCommand(sql1, connect);

                Console.WriteLine("Cau lenh dang chay la: {0} ", cmd.CommandText);

                //Thực thi câu lẹnh
                string k = cmd.ExecuteScalar().ToString();
                Console.WriteLine("So luong hs tham gia lop CTDL la: " + k);

            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                connect.Close();
                Console.WriteLine("Ngat ket noi");
            }
            Console.ReadKey();
        }
    }
}
