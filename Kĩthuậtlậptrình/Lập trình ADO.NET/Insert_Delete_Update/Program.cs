﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace Insert_Delete_Update
{
    class Program
    {
        static void Main(string[] args)
        {
            SqlConnection connect = new SqlConnection(@"Data Source=.\SQLEXPRESS;Initial Catalog=QuanLyHS;Integrated Security=True");

            //string insertQuery = @"Insert into HocSinh (MaSoHS, HoTen, MaLop, MaMon, Diem) Values ('004', N'Nguyễn Thùy Linh', '12C', 'CTDL', 10)";
            string deleteQuery = @"Delete From HocSinh where HocSinh.HoTen = N'Nguyễn Thùy Linh' And HocSinh.MaMon = 'KTLT' ";
            string updateQuery = @"Update HocSinh Set HocSinh.HoTen = N'Lâm đẹp zai vl' where HocSinh.HoTen = N'Nguyễn Trung Lâm'";

            string MaSoHS, HoTen, MaLop, MaMon, Diem;
            Console.WriteLine("Nhap vao ma so hoc sinh: ");
            MaSoHS = Console.ReadLine();
            Console.WriteLine("Nhap vao ho ten: ");
            HoTen = Console.ReadLine();
            Console.WriteLine("Nhap vao ma lop: ");
            MaLop = Console.ReadLine();
            Console.WriteLine("Nhap vao ma mon: ");
            MaMon = Console.ReadLine();
            Console.WriteLine("Nhap vao diem");
            Diem = Console.ReadLine();

            string insertQuery = @"Insert into HocSinh (MaSoHS, HoTen, MaLop, MaMon, Diem) Values ('"+MaSoHS+"', N'"+HoTen+"', '"+MaLop+"', '"+MaMon+"', "+Diem+")";
            SqlCommand cmdInsert = new SqlCommand(insertQuery, connect);
            SqlCommand cmdUpdate = new SqlCommand(updateQuery, connect);
            SqlCommand cmdDelete = new SqlCommand(deleteQuery, connect);
            try{

                connect.Open();
                Console.WriteLine("\nDa insert");
                cmdInsert.ExecuteNonQuery();

                Console.WriteLine("\nDa update");
                cmdUpdate.ExecuteNonQuery();

                Console.WriteLine("\nDa delete");
                cmdDelete.ExecuteNonQuery();
                
            }
            catch(SqlException e)
            {
                Console.WriteLine(e.ToString());
            }
            finally{
                connect.Close();

            }
            Console.ReadKey();
        }
    }
}
