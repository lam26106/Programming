﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ứng_dụng_ADO.NET_trong_winform
{
    class DataProvider
    {
        private string connect;
        private SqlConnection SqlConnect;
        public DataProvider()
        {
            connect = null;
        }
        public DataProvider(string con)
        {
            connect = con;
            SqlConnect = new SqlConnection(con);
        }
      
        public string CONNECT
        {
            get { return connect; }
            set { connect = value; }
        }
        public SqlConnection SQLCONNECT
        {
            get { return SqlConnect;}
            set { SqlConnect = value;}
        }
        
        public DataTable LoadDatabase(string command)
        {
            DataTable dt = new DataTable();
            SQLCONNECT = new SqlConnection(CONNECT);
            SqlCommand cmd = new SqlCommand(command, SQLCONNECT);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            return dt;
        }
        public int Modify(string command)
        {
            SQLCONNECT = new SqlConnection(CONNECT);
            if (SQLCONNECT.State == ConnectionState.Closed)
            {
                SQLCONNECT.Open();
            }
            SqlCommand cmd = new SqlCommand(command, SQLCONNECT);
            int modifiedRows = cmd.ExecuteNonQuery();
            SQLCONNECT.Close();
            return modifiedRows;
        }
       
    }
}
