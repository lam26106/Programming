﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ứng_dụng_ADO.NET_trong_winform
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            string command = "Select *from HocSinh";
            DataProvider dp = new DataProvider(@"Data Source=.\SQLEXPRESS;Initial Catalog=QuanLyHS;Integrated Security=True");
            DataTable  dt = dp.LoadDatabase(command);
            dgv1.DataSource = dt;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            string command = @"Delete Top (1) from HocSinh";
            DataProvider dp = new DataProvider(@"Data Source=.\SQLEXPRESS;Initial Catalog=QuanLyHS;Integrated Security=True");
            dp.Modify(command);
        }

        private void btnShow_Click(object sender, EventArgs e)
        {
            
            if(dgv1.SelectedCells.Count == 0)
            {
                MessageBox.Show("Ban chua chon dong nao       ");
            }
            else
            {
                MessageBox.Show(dgv1.CurrentRow.Index.ToString() + " " + dgv1.CurrentCell.ColumnIndex.ToString());
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            string command = "Select * From LopHoc";

            DataProvider dp = new DataProvider(@"Data Source=.\SQLEXPRESS;Initial Catalog=QuanLyHS;Integrated Security=True");
            cmb1.DataSource = dp.LoadDatabase(command);

            cmb1.DisplayMember = "TenLop";
            cmb1.ValueMember = "MaLop";

            string command1 = "Select * From MonHoc";
            dgv1.DataSource = dp.LoadDatabase(command1);

        }

        private void btnConfirm_Click(object sender, EventArgs e)
        {
            string str = "";
            foreach (DataGridViewRow row in dgv1.Rows)
            {
                if (row.Cells[0].Value != null)
                {
                    str += row.Cells[2].Value.ToString()+ " voi " + row.Cells[3].Value.ToString() + " tin chir\n";
                }
            }
            if (str == "")
            {
                MessageBox.Show("Ban quen chua chon mon cmnr        ");
            }
            else
            {
                MessageBox.Show("Ban chon cac mon la: \n" + str);
            }
        }
    }
}
