﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lập_trình_ADO.NET
{
    class Program
    {
        static void Main(string[] args)
        {
            //B1: Connect to Database
            SqlConnection connect = new SqlConnection(@"Data Source=.\SQLEXPRESS;Initial Catalog=QuanLyHS;Integrated Security=True");

            string querySelect = "Select * from HocSinh";



            try
            {
                connect.Open();
                SqlCommand cmd = new SqlCommand(querySelect, connect);
                SqlDataReader reader = cmd.ExecuteReader();
                int dem = 1;
                reader.Read();
                Console.WriteLine("\n" + reader.Depth);
                //while(reader.Read())
                //{
                //    Console.Write("\nDu lieu dong thu " + dem++ + " la\n");
                Console.Write("\nMa so: " + reader[0]); /// reader["MaSo"] // reader là dòng - reader[0] là cột mã số trên dòng tương ứng.
                Console.Write("\nHo ten: " + reader[1]); // reader["HoTen"]
                Console.Write("\nMa lop: " + reader[2]); // reader["MaLop"]
                Console.Write("\nMa mon hoc: " + reader[3]); // reader["MaMonHoc"]
                Console.Write("\nDiem so: " + reader[4]); // reader["DiemSo"]
                //}
               

            }
            catch(SqlException e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                connect.Close();
                Console.WriteLine("Ngat ket noi");
            }
            Console.ReadKey();
        }
    }
}
