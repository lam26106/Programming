#include <iostream>
#include <stdio.h>
#include <string>
#include <fstream>
#pragma warning (disable:4996)
using namespace std;

void MoFile(ifstream &FileIn)
{
	FileIn.open("INPUT.txt", ios_base::in);
	if (!FileIn)
	{
		printf("Khong tim thay tap tin");
		system("pause");
		exit(0);
	}
}
struct Lophoc
{
	int soHocSinh;
	float *diemhs;
};
typedef struct Lophoc LOPHOC;
float HighestMark(LOPHOC lh)
{
	float highest = lh.diemhs[0];
	for (int i = 1; i < lh.soHocSinh; ++i)
		if (lh.diemhs[i] > highest)
			highest = lh.diemhs[i];
	return highest;
}
int main()
{

	ifstream FileIn;
	MoFile(FileIn);

	LOPHOC lh;
	FileIn >> lh.soHocSinh;
	lh.diemhs = new float[lh.soHocSinh];
	for (int i = 0; i < lh.soHocSinh; ++i)
		FileIn >> lh.diemhs[i];

	
	FileIn.close();


	ofstream FileOut;
	FileOut.open("OUTPUT.txt", ios_base::out);
	float highest = HighestMark(lh);
	FileOut << highest;

	FileOut.close();

	system("pause");
	return 0;
}