#include <iostream>
#include <stdio.h>
#include <fstream>
#include <string>
#pragma warning (disable:4996)
using namespace std;

struct HocSinh
{
	string name;
	float diemToan, diemLy, diemHoa, dtb;
	char *xeploai;
};
typedef struct HocSinh HOCSINH;
char *XepLoaiHocLuc(float dtb)
{
	if (dtb < 5)
		return "Yeu";
	else if (dtb < 7)
		return "Kha";
	else if (dtb < 8)
		return "Tien Tien";
	else
		return "Gioi";
}
void MoFile(ifstream &FileIn)
{
	FileIn.open("INPUT.txt", ios_base::in);
	if (!FileIn){
		cout << "Khong tim thay tap tin ";
		system("pause");
		exit(0);
	}
}
int main()
{
	//B1: Mo file
	ifstream FileIn;
	MoFile(FileIn);
	HOCSINH hs;
	getline(FileIn, hs.name);
	FileIn >> hs.diemToan >> hs.diemLy >> hs.diemHoa;

	FileIn.close();

	hs.dtb = (hs.diemToan + hs.diemLy + hs.diemHoa) / 3;
	hs.xeploai = XepLoaiHocLuc(hs.dtb);
	ofstream FileOut;
	FileOut.open("OUTPUT.txt", ios_base::out);
	FileOut << hs.name << endl << hs.dtb << " " << hs.xeploai;

	FileOut.close();

	
	system("pause");
	return 0;
}