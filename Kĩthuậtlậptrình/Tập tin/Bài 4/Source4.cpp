#include <iostream>
#include <stdio.h>
#include <string>
#include <fstream>
#pragma warning (disable:4996)
using namespace std;

void MoFile(ifstream &FileIn)
{
	FileIn.open("INPUT.txt", ios_base::in);
	if (!FileIn)
	{
		printf("Khong tim thay tap tin");
		system("pause");
		exit(0);
	}
}
int main()
{
	ifstream FileIn;
	MoFile(FileIn);
	int Max, num;
	FileIn >> Max;
	while (!FileIn.eof()) //Lap cho den khi nao scan het cac ky tu trong file txt
	{
		FileIn >> num;
		if (Max < num)
			Max = num;
	}
	FileIn.close();

	ofstream FileOut;
	FileOut.open("OUTPUT.txt", ios_base::out);
	FileOut << Max;

	FileOut.close();
	system("pause");
	return 0;
}