#include <iostream>
#include <stdio.h>
#include <fstream>
#pragma warning (disable:4996)
using namespace std;

int main()
{
	////Cu phap ben C
	//FILE *fileIn; // khai bao con tro File
	////cho con tro tro toi vung nho chua file Input
	//fileIn = fopen("INPUT.txt", "r");
	//if (!fileIn)
	//	cout << "Khong tim thay tap tin INPUT.txt";
	//// doc du lieu tu tap tin 
	//int a, b;
	//fscanf(fileIn, "%d%d", &a, &b);

	//// dong tap tin
	//fclose(fileIn);

	////check xem co doc du lieu dung hay k
	//cout << a << " " << b;

	////tim Max cua a va b
	//int max = a > b ? a : b;

	////ghi du lieu vao file
	////B1: mo file
	//FILE *fileOut;
	//fileOut = fopen("OUTPUT.txt", "w");

	////B2: Ghi file
	//fprintf(fileOut, "%d", max);

	////B3: dong file
	//fclose(fileOut);



	// Lam ben C++
	ifstream FileIn; // khai bao  FileIn 
	FileIn.open("INPUT.txt", ios_base::in);
	if (!FileIn)
		cout << "Khong tim thay tap tin INPUT.txt";
	// doc du lieu tu tap tin 
	int a, b;
	FileIn >> a >> b;

	// dong tap tin
	FileIn.close();

	//check xem co doc du lieu dung hay k
	cout << a << " " << b;

	//tim Max cua a va b
	int max = a > b ? a : b;

	//ghi du lieu vao file
	//B1: mo file
	ofstream FileOut;
	FileOut.open("OUTPUT.txt", ios_base::out);

	//B2: Ghi file
	FileOut << max;

	//B3: dong file
	FileOut.close();
	system("pause");
	return 0;
}