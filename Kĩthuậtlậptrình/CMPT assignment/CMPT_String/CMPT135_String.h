#pragma once
#include <iostream>
using namespace std;

class CMPT135_String
{
private:
	int length; //This will be the length of the string without counting the NULL character
	char *buffer; //This buffer will actually have length + 1 characters including the NULL char
public:
	//Very useful Static member function
	static int getC_StringLength(const char *charArray);
	//Constructors
	CMPT135_String();
	CMPT135_String(const char &c);
	CMPT135_String(const char *buffer);//*buffer is a null terminated char array
	CMPT135_String(const CMPT135_String &s);
	//Destructor
	~CMPT135_String();
	//Assignment operators
	CMPT135_String& operator = (const CMPT135_String &s);
	CMPT135_String& operator = (const char &c);
	CMPT135_String& operator = (const char* buffer); //*buffer is a null terminated char array
	//Getter member functions
	int getLength() const;
	char& operator[](const int &index) const;
	//Setter member functions
	void append(const CMPT135_String &s);
	void append(const char &c);
	void append(const char *buffer);
	void setBuffer(const char *buffer);
	//Other member functions
	int findCharIndex(const char &c) const;
	int reverseFindCharIndex(const char &c) const;
	CMPT135_String getSubString(const int &startIndex, const int &length) const; 
	bool isSubString(const CMPT135_String &s) const;
	bool isSubString(const char *buffer) const;
	void reverse();
	friend void removeOneChar(int index, CMPT135_String &s); // a supporting function
	void removeChar(const char &c);
	void replace(const char &c1, const char &c2);
	//Operator member functions
	CMPT135_String operator + (const CMPT135_String &s) const;
	CMPT135_String operator + (const char *buffer) const; //*buffer is a null terminated char array
	CMPT135_String operator + (const char &c) const;
	void operator += (const CMPT135_String &s);
	void operator += (const char &c);
	void operator += (const char* buffer); //*buffer is a null char terminated char array
	bool operator == (const CMPT135_String &s) const;
	bool operator == (const char *buffer) const; //*buffer is a null char terminated char array
	bool operator != (const CMPT135_String &s) const;
	bool operator != (const char *buffer) const; //*buffer is a null char terminated char array
	bool operator < (const CMPT135_String &s) const;
	bool operator < (const char *buffer) const; //*buffer is a null char terminated char array
	bool operator > (const CMPT135_String &s) const;
	bool operator > (const char *buffer) const; //*buffer is a null char terminated char array
	bool operator >= (const CMPT135_String &s) const;
	bool operator >= (const char *buffer) const; //*buffer is a null char terminated char array
	bool operator <= (const CMPT135_String &s) const;
	bool operator <= (const char *buffer) const; //*buffer is a null char terminated char array
	//Friend Functions (for operators)
	friend CMPT135_String operator + (const char *buffer, const CMPT135_String &s); //*buffer is a
	//null terminated char array
	friend CMPT135_String operator + (const char &c, const CMPT135_String &s);
	friend bool operator == (const char *buffer, const CMPT135_String &s); //*buffer is a null
	//terminated char array
	friend bool operator != (const char *buffer, const CMPT135_String &s); //*buffer is a null
	//terminated char array
	friend bool operator < (const char *buffer, const CMPT135_String &s); //*buffer is a null
	//terminated char array
	friend bool operator > (const char *buffer, const CMPT135_String &s); //*buffer is a null
	//terminated char array
	friend bool operator <= (const char *buffer, const CMPT135_String &s); //*buffer is a null
	//terminated char array
	friend bool operator >= (const char *buffer, const CMPT135_String &s); //*buffer is a null
	//terminated char array
	friend ostream& operator << (ostream &outputStream, const CMPT135_String &s);
	friend istream& operator >> (istream &inputStream, CMPT135_String &s);
};