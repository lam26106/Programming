#include "CMPT135_String.h"


int CMPT135_String::getC_StringLength(const char *charArray)
{
	//This function returns the length of the charArray
	//It does not include the null char at the end of charArray
	int len = 0;
	for (int i = 0; charArray[i] != '\0'; i++)
		len++;
	return len;
}

CMPT135_String::CMPT135_String()
{
	this->buffer = new char [1]; 
	this->buffer[0] = '\0';
	length = 0;
}

CMPT135_String::CMPT135_String(const char &c)
{
	length = 1;
	this->buffer = new char[2];
	this->buffer[0] = c;
	this->buffer[1] = '\0';
}
CMPT135_String::CMPT135_String(const char *buffer)
{
	this->length = CMPT135_String::getC_StringLength(buffer);
	this->buffer = new char[this->getLength() + 1];
	for (int i = 0; i < this->getLength(); i++)
		this->buffer[i] = buffer[i];
	this->buffer[this->length] = '\0';
}
CMPT135_String::CMPT135_String(const CMPT135_String &s)
{
	this->length = CMPT135_String::getC_StringLength(s.buffer);
	this->buffer = new char[this->length + 1];
	for (int i = 0; i < this->length; ++i)
	{
		this->buffer[i] = s.buffer[i];
	}
	this->buffer[length] = '\0';
}

CMPT135_String::~CMPT135_String()
{
	delete[]this->buffer;
	this->buffer = new char[1];
	this->buffer[0] = '\0';
	length = 0;
	length = 0;
}
CMPT135_String& CMPT135_String::operator = (const CMPT135_String &s)
{
	this->length = s.length;
	int len = CMPT135_String::getC_StringLength(s.buffer);
	delete[] this->buffer;
	this->buffer = new char[len + 1];
	for (int i = 0; i < len; ++i)
	{
		this->buffer[i] = s.buffer[i];
	}
	this->buffer[len] = '\0';
	return *this;
}
CMPT135_String& CMPT135_String::operator = (const char &c)
{
	this->length = 1;
	delete[] this->buffer;
	this->buffer = new char[this->length + 1];
	this->buffer[0] = c;
	this->buffer[1] = '\0';
	return *this;
}
CMPT135_String& CMPT135_String::operator = (const char* buffer)
{
	this->length = CMPT135_String::getC_StringLength(buffer); 
	delete[] this->buffer;
	this->buffer = new char[this->length + 1];
	for (int i = 0; i < this->length; i++)
		this->buffer[i] = buffer[i];
	this->buffer[this->length] = '\0';
	return *this;

}
int CMPT135_String::getLength() const
{
	int len = this->length;
	return len;
}
char& CMPT135_String::operator[ ](const int &index) const
{
	return this->buffer[index];
}

void CMPT135_String::append(const CMPT135_String &s)
{
	this->length += CMPT135_String::getC_StringLength(s.buffer);
	char *temp = new char[this->length + 1];
	int idx = 0;
	while (this->buffer[idx] != '\0')
	{
		temp[idx] = this->buffer[idx];
		idx++;
	}
	int j = 0;
	while (s.buffer[j] != '\0')
	{
		temp[idx++] = s.buffer[j++];
	}
	temp[idx] = '\0';
	delete[]this->buffer;
	this->buffer = temp;
}

void CMPT135_String::append(const char &c)
{
	this->length += 1;
	char *temp = new char[this->length + 1];
	int idx = 0;
	while (this->buffer[idx] != '\0')
	{
		temp[idx] = this->buffer[idx++];
	}
	temp[idx++] = c;
	temp[idx] = '\0';
	delete[]this->buffer;
	this->buffer = temp;
}

void CMPT135_String::append(const char *buffer)
{
	this->length = this->length + CMPT135_String::getC_StringLength(buffer);
	char *temp = new char[this->length + 1];
	int idx = 0;
	while (this->buffer[idx] != '\0')
	{
		temp[idx] = this->buffer[idx];
		idx++;
	}
	int j = 0;
	while (buffer[j] != '\0')
	{
		temp[idx++] = buffer[j++];
	}
	temp[idx] = '\0';
	delete[]this->buffer;
	this->buffer = temp;

}
ostream& operator << (ostream &outputStream, const CMPT135_String &s)
{
	for (int i = 0; i < s.getLength(); i++)
		outputStream << s[i];
	return outputStream;
}

void CMPT135_String::setBuffer(const char *buffer)
{
	delete[]this->buffer;
	this->length = CMPT135_String::getC_StringLength(buffer);
	this->buffer = new char[this->getLength() + 1];
	for (int i = 0; i < this->length + 1; ++i)
	{
		this->buffer[i] = buffer[i];
	}
}

int CMPT135_String::findCharIndex(const char &c) const
{
	for (int i = 0; this->buffer[i]!= '\0'; ++i)
	{
		if (this->buffer[i] == c)
			return i;
	}
	return -1;
}

int CMPT135_String::reverseFindCharIndex(const char &c) const
{
	for (int i = this->length - 1; i >= 0; --i)
	{
		if (this->buffer[i] == c)
			return i;
	}
	return -1;
}

CMPT135_String CMPT135_String::getSubString(const int &startIndex, const int &length) const
{
	CMPT135_String s;
	s.buffer = new char[length + 1];
	int idx1 = 0;
	int idx2 = startIndex;
	while (idx1 < length && this->buffer[idx2] != '\0')
	{
		s.buffer[idx1++] = this->buffer[idx2++];
	}
	s.buffer[idx1] = '\0';
	s.length = idx1;
	return s;
}

bool CMPT135_String::isSubString(const CMPT135_String &s) const
{
	if (s.buffer[0] == '\0')
		return true;
	for (int i = 0; this->buffer[i] != '\0'; ++i)
	{
		int idx = 0;
		if (this->buffer[i] == s.buffer[idx++])
		{
			bool check = true;
			for (int j = i + 1; j <= this->getLength() && idx < s.getLength(); ++j, ++idx)
			{
				if (this->buffer[j] != s.buffer[idx])
				{
					check = false;
				}
			}
			if (check == true)
				return check;
		}
	}
	return false;
}

bool CMPT135_String::isSubString(const char *buffer) const
{
	if (buffer[0] == '\0')
		return true;
	for (int i = 0; this->buffer[i] != '\0'; ++i)
	{
		int idx = 0;
		if (this->buffer[i] == buffer[idx++])
		{
			bool check = true;
			for (int j = i + 1; j <= this->getLength() && idx < CMPT135_String::getC_StringLength(buffer); ++j, ++idx)
			{
				if (this->buffer[j] != buffer[idx])
				{
					check = false;
					break;
				}
			}
			if (check == true)
				return true;
		}
	}
	return false;
}

void CMPT135_String::reverse()
{
	for (int i = 0; i < this->length / 2; ++i)
	{
		char temp = this->buffer[i];
		this->buffer[i] = this->buffer[this->length - 1 - i];
		this->buffer[this->length - 1 - i] = temp;
	}
}

void removeOneChar(int index, CMPT135_String &s)
{
	for (int i = index; i < s.length; ++i)
	{
		s.buffer[i] = s.buffer[i + 1];
	}
	s.length -= 1;
}
void CMPT135_String::removeChar(const char &c)
{
	for (int i = 0; i < this->length; ++i)
	{
		if (this->buffer[i] == c)
		{
			removeOneChar(i, *this);
			i--;
		}
	}
}
void CMPT135_String::replace(const char &c1, const char &c2)
{
	for (int i = 0; this->buffer[i] != '\0'; ++i)
	{
		if (this->buffer[i] == c1)
		{
			this->buffer[i] = c2;
		}
	}
}

CMPT135_String CMPT135_String::operator + (const CMPT135_String &s) const
{
	CMPT135_String sumChar;
	sumChar.buffer = new char[this->getLength() + s.getLength() + 1];
	int idx = 0;
	while (this->buffer[idx] != '\0')
	{
		sumChar.buffer[idx] = this->buffer[idx++];
	}
	int i = 0;
	while (i <= s.getLength())
	{
		sumChar.buffer[idx++] = s.buffer[i++];
	}
	sumChar.length = CMPT135_String::getC_StringLength(sumChar.buffer);
	return sumChar;
}

CMPT135_String CMPT135_String::operator + (const char *buffer) const
{
	CMPT135_String sumChar;
	int bufferLen = CMPT135_String::getC_StringLength(buffer);
	sumChar.buffer = new char[this->getLength() + bufferLen + 1];
	int idx = 0;
	while (this->buffer[idx] != '\0')
	{
		sumChar.buffer[idx] = this->buffer[idx++];
	}
	int i = 0;
	while (i <= bufferLen )
	{
		sumChar.buffer[idx++] = buffer[i++];
	}
	sumChar.length = CMPT135_String::getC_StringLength(sumChar.buffer);

	return sumChar;
}

CMPT135_String CMPT135_String::operator + (const char &c) const
{
	CMPT135_String sumChar;
	sumChar.buffer = new char[this->getLength() + 2];
	for (int i = 0; this->buffer[i] != '\0'; ++i)
		sumChar.buffer[i] = this->buffer[i];
	sumChar.buffer[this->getLength()] = c;
	sumChar.buffer[this->getLength() + 1] = '\0';
	sumChar.length = CMPT135_String::getC_StringLength(sumChar.buffer);
	return sumChar;
}

void CMPT135_String::operator += (const CMPT135_String &s)
{
	*this = *this + s;
}

void CMPT135_String::operator += (const char *buffer)
{
	*this = *this + buffer;
}

void  CMPT135_String::operator += (const char &c)
{
	*this = *this + c;
}

bool CMPT135_String::operator == (const CMPT135_String &s) const
{
	if (this->getLength() != s.getLength())
		return false;
	for (int i = 0; i <= this->getLength(); ++i)
	{
		if (this->buffer[i] != s.buffer[i])
			return false;
	}
	return true;
}

bool CMPT135_String::operator == (const char *buffer) const
{
	if (this->getLength() != CMPT135_String::getC_StringLength(buffer))
		return false;
	for (int i = 0; i <= this->getLength(); ++i)
	{
		if (this->buffer[i] != buffer[i])
			return false;
	}
	return true;
}
bool CMPT135_String::operator != (const CMPT135_String &s) const
{
	if (*this == s)
		return false;
	return true;
}

bool CMPT135_String::operator != (const char *buffer) const
{
	if (*this == buffer)
		return false;
	return true;
}

bool CMPT135_String::operator < (const CMPT135_String &s) const
{
	for (int i = 0; i <= this->getLength() && i <= s.getLength(); ++i)
	{
		if (this->buffer[i] < s.buffer[i])
			return true;
		else if (this->buffer[i] > s.buffer[i])
			return false;
	}

}

bool CMPT135_String::operator < (const char *buffer) const
{
	for (int i = 0; i <= this->getLength() && i <= CMPT135_String::getC_StringLength(buffer); ++i)
	{
		if (this->buffer[i] < buffer[i])
			return true;
		else if (this->buffer[i] > buffer[i])
			return false;
	}
	return false;
}

bool CMPT135_String::operator > (const CMPT135_String &s) const
{
	//for (int i = 0; i <= this->getLength() && i <= s.getLength(); ++i)
	//{
	//	if (this->buffer[i] > s.buffer[i])
	//		return true;
	//	else if (this->buffer[i] < s.buffer[i])
	//		return false;
	//}
	//return false;
	return (*this < s || *this == s) ? false : true;
}

bool CMPT135_String::operator > (const char *buffer) const
{
	//for (int i = 0; i <= this->getLength() && i <= CMPT135_String::getC_StringLength(buffer); ++i)
	//{
	//	if (this->buffer[i] > buffer[i])
	//		return true;
	//	else if (this->buffer[i] < buffer[i])
	//		return false;
	//}
	//return false;
	return (*this < buffer || *this == buffer) ? false : true;
}

bool CMPT135_String::operator >= (const CMPT135_String &s) const
{
	//for (int i = 0; i <= this->getLength() && i <= s.getLength(); ++i)
	//{
	//	if (this->buffer[i] > s.buffer[i])
	//		return true;
	//	else if (this->buffer[i] < s.buffer[i])
	//		return false;
	//}
	//return true;
	return (*this < s) ? false : true;
}
bool CMPT135_String::operator >= (const char *buffer) const
{
	//for (int i = 0; i <= this->getLength() && i <= CMPT135_String::getC_StringLength(buffer); ++i)
	//{
	//	if (this->buffer[i] > buffer[i])
	//		return true;
	//	else if (this->buffer[i] < buffer[i])
	//		return false;
	//}
	//return true;
	return (*this < buffer) ? false : true;
}

bool CMPT135_String::operator <= (const CMPT135_String &s) const
{
	//for (int i = 0; i <= this->getLength() && i <= s.getLength(); ++i)
	//{
	//	if (this->buffer[i] < s.buffer[i])
	//		return true;
	//	else if (this->buffer[i] > s.buffer[i])
	//		return false;
	//}
	//return true;
	return (*this > s) ? false : true;
}

bool CMPT135_String::operator <= (const char *buffer) const
{
	//for (int i = 0; i <= this->getLength() && i <= CMPT135_String::getC_StringLength(buffer); ++i)
	//{
	//	if (this->buffer[i] < buffer[i])
	//		return true;
	//	else if (this->buffer[i] > buffer[i])
	//		return false;
	//}
	//return true;
	return (*this > buffer) ? false : true;
}
//
CMPT135_String operator + (const char *buffer, const CMPT135_String &s)
{
	CMPT135_String result;
	int length = CMPT135_String::getC_StringLength(buffer);
	result.buffer = new char[length + s.getLength() + 1];
	int idx = 0;
	while (idx < length)
	{
		result.buffer[idx] = buffer[idx++];
	}
	int i = 0;
	while (i <= s.getLength())
	{
		result.buffer[idx++] = s.buffer[i++];
	}
	result.length = CMPT135_String::getC_StringLength(result.buffer);
	return result;
}

CMPT135_String operator + (const char &c, const CMPT135_String &s)
{
	CMPT135_String result;
	result.buffer = new char[s.getLength() + 2];
	result.buffer[0] = c;
	int idx = 1, i = 0;
	while (i <= s.getLength())
	{
		result.buffer[idx++] = s.buffer[i++];
	}
	result.length = CMPT135_String::getC_StringLength(result.buffer);
	return result;
}

bool operator == (const char *buffer, const CMPT135_String &s)
{
	//if (s == buffer)
	//	return true;
	//return false;
	return (s == buffer);
}

bool operator != (const char *buffer, const CMPT135_String &s)
{
	//if (s != buffer)
	//	return true;
	//return false;
	return (s != buffer);
}

bool operator < (const char *buffer, const CMPT135_String &s)
{
	//if (s > buffer)
	//	return true;
	//return false;
	return (s > buffer);
}
bool operator >(const char *buffer, const CMPT135_String &s)
{
	//if (s < buffer)
	//	return true;
	//return false;
	return (s < buffer);
}
bool operator <= (const char *buffer, const CMPT135_String &s)
{
	//if (s >= buffer)
	//	return true;
	//return false;
	return (s >= buffer);
}
bool operator >= (const char *buffer, const CMPT135_String &s)
{
	//if (s <= buffer)
	//	return true;
	//return false;
	return (s <= buffer);
}

istream& operator >> (istream &inputStream, CMPT135_String &s)
{
	/*This function is designed to read ANY length input string from the user. In order to achieve this, we
	will read characters until end of line character ('\n') is read. The trouble will be where to store the
	unknown length input. For this, we will create a char array (named buffer1) of length 100. The hope is,
	the input won't exceed 100 chars. However if it does, we will copy the 100 chars to a temp char array,
	make the buffer1 array of twice its current length, copy the characters in the temp array to buffer1 and
	continue reading; applying same principle if buffer1 gets filled again. Finally, we will copy the exact
	characters read to another buffer (named buffer2) and use that to set the buffer of CMPT135_String object
	parameter. Finally, we will do clean up of unnecessary dynamic arrays*/
	int bufferCurrentLength = 100;
	char *buffer1 = new char[bufferCurrentLength];
	int length = 0;
	char c;
	while (1)
	{
		inputStream.get(c);
		if (c == '\n')
			break;
		buffer1[length++] = c;
		//Now, check in case buffer is filled
		if (length == bufferCurrentLength)
		{
			//Copy current buffer to temp array
			char *temp = new char[2 * bufferCurrentLength];
			for (int i = 0; i < bufferCurrentLength; i++)
				temp[i] = buffer1[i];
			//Delete current buffer
			delete[] buffer1;
			//Point buffer1 to temp
			buffer1 = temp;
			//Adjust the size of the current buffer
			bufferCurrentLength *= 2;
		}
	}
	//Create a buffer of exact size for the characters read
	char *buffer2 = new char[length + 1];
	//Copy chars in current buffer to the exact size buffer
	for (int i = 0; i < length; i++)
		buffer2[i] = buffer1[i];
	//Append the a NULL character to the exact size buffer
	buffer2[length] = '\0';
	/*
	Set the exact size buffer as the buffer of the CMPT135_String object being read. The set buffer
	function will take care of adjusting the length of the object. Also the setBuffer function will first
	clean the current buffer for us.
	*/
	s.setBuffer(buffer2);
	//Clean up the current buffer and the exact size buffer
	delete[] buffer1; 
	delete[] buffer2;
	//Return the input stream
	return inputStream;
}