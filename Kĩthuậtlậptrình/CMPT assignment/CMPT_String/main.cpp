#include "CMPT135_String.h"

int main()
{
	//Test getC_StringLength static member function
	char *charArray = "This is interesting."; //Length must be 20
	cout << "The length of charArray is " << CMPT135_String::getC_StringLength(charArray) << endl;
	//Test default constructor
	CMPT135_String s1;
	cout << "s1 is " << s1 << ", length = " << s1.getLength() << endl;
	//Test non-default constructor
	CMPT135_String s2(charArray);
	cout << "s2 is " << s2 << ", length = " << s2.getLength() << endl;
	//Test copy constructor
	CMPT135_String s3 = s2;
	cout << "s3 is " << s3 << ", length = " << s3.getLength() << endl;
	//Test destructor
	s3.~CMPT135_String();
	cout << "After destruction, s3 is " << s3 << ", length = " << s3.getLength() << endl;
	//
	//Test assignment operators
	CMPT135_String s4;
	s4 = s2; //length of s4 must be 20
	cout << "s4 assigned s2. s4 is " << s4 << ", length = " << s4.getLength() << endl;
	s4 = 'Y';
	cout << "s4 assigned 'Y'. s4 is " << s4 << ", length = " << s4.getLength() << endl;
	s4 = "So far so good!"; //length of s4 must be 15
	cout << "s4 assigned \"So far so good!\". s4 is " << s4 << ", length = " << s4.getLength() <<
		endl;

	//Test chain assignment
	CMPT135_String t1, t2, t3;
	t1 = t2 = t3 = s4;
	cout << "After assigning t1 = t2 = t3 = s4, we get:" << endl;
	cout << "\tt1 is " << t1 << endl;
	cout << "\tt2 is " << t2 << endl;
	cout << "\tt3 is " << t3 << endl;
	//Test getLength. Already used (tested) above.
	//Test operator [ ]
	cout << "s4 printed using indexing operator is ";
	for (int i = 0; i < s4.getLength(); i++)
		cout << s4[i];
	cout << endl;
	s4[3] = 'F';
	s4[7] = 'S';
	s4[10] = 'G';
	cout << "s4 capitalized. s4 is " << s4 << ", length = " << s4.getLength() << endl;
	//Test append member functions
	s3.append(s4);
	cout << "s3 appened by s4. s3 is now " << s3 << ", length = " << s3.getLength() << endl;
	s1.append("I was empty before I got appended"); //length must be 33
	cout << "s1 got appened. s1 is now " << s1 << ", length = " << s1.getLength() << endl;
	s1.append('.'); //length must be 34
	cout << "s1 appened by '.'. s1 is now " << s1 << ", length = " << s1.getLength() << endl;
	//Test setBuffer member function
	s1.setBuffer("I am changing a lot."); //length must be 20
	cout << "s1 got modified. s1 is now " << s1 << ", length = " << s1.getLength() << endl;
	//Test findCharIndex
	char c = 'a';
	cout << "The first index of " << c << " in " << s1 << " is " << s1.findCharIndex(c) << endl;
	c = 'A';
	cout << "The first index of " << c << " in " << s1 << " is " << s1.findCharIndex(c) << endl;
	//Test reverseFindCharIndex
	c = 'a';
	cout << "The last index of " << c << " in " << s1 << " is " << s1.reverseFindCharIndex(c) <<
		endl;
	c = 'A';
	cout << "The last index of " << c << " in " << s1 << " is " << s1.reverseFindCharIndex(c) <<
		endl;
	//Test getSubString
	cout << "Substring of s2 starting from index 8 having length 11 is \"" << s2.getSubString(8, 11)
		<< "\"" << endl;
	//Test isSubString member functions
	CMPT135_String sb = "So";
	if (s3.isSubString(sb))
		cout << sb << " is a substring of " << s3 << endl;
	else
		cout << sb << " is NOT a substring of " << s3 << endl;
	sb = "so";
	if (s3.isSubString(sb))
		cout << sb << " is a substring of " << s3 << endl;
	else
		cout << sb << " is NOT a substring of " << s3 << endl;
	if (s3.isSubString(""))
		cout << "" << " is a substring of " << s3 << endl;
	else
		cout << "" << " is NOT a substring of " << s3 << endl;
	if (s3.isSubString("oF"))
		cout << "oF" << " is a substring of " << s3 << endl;
	else
		cout << "oF" << " is NOT a substring of " << s3 << endl;
	//Test reverse
	s1.reverse();
	cout << "The reverse of s1 is " << s1 << endl;
	s1.reverse();
	cout << "Reversing s1 again, we get " << s1 << endl;
	//Test removeChar
	s4.removeChar(' '); //Rwemove the spaces
	cout << "After removing spaces, s4 is " << s4 << endl;
	//Test replace
	s1.replace('.', '!');
	cout << "After replacing . by ! in s1, s1 is now " << s1 << endl;

	//Test operator + member functions
	cout << "s1 + s2 is " << s1 + s2 << endl;
	cout << "s1 + \" HUH\" is " << s1 + " HUH" << endl;
	cout << "s4 + '!' is " << s4 + '!' << endl;
	//Test += operators
	CMPT135_String s5;
	s5 +=  CMPT135_String("Yonas ");
	cout << "s5 is " << s5 << endl;
	s5 += "T";
	cout << "s5 is " << s5 << endl;
	s5 += ". ";
	cout << "s5 is " << s5 << endl;
	s5 = s5 +"Weldeselassie";
	cout << "s5 is " << s5 << endl;
	//Test operator == member functions
	cout << "Using == operator: ";
	if (s3 == s4)
		cout << s3 << " and " << s4 << " are equal" << endl;
	else
		cout << s3 << " and " << s4 << " are NOT equal" << endl;
	cout << "Using == operator: ";
	if (s3 == "So Far So Good!")
		cout << s3 << " and " << "\"So Far So Good!\"" << " are equal" << endl;
	else
		cout << s3 << " and " << "\"So Far So Good!\"" << " are NOT equal" << endl;
	//Test operator != member functions
	cout << "Using != operator: ";
	if (s3 != s4)
		cout << s3 << " and " << s4 << " are NOT equal" << endl;
	else
		cout << s3 << " and " << s4 << " are equal" << endl;
	cout << "Using != operator: ";
	if (s3 != "So Far So Good!")
		cout << s3 << " and " << "\"So Far So Good!\"" << " are NOT equal" << endl;
	else
		cout << s3 << " and " << "\"So Far So Good!\"" << " are equal" << endl;
	//Test operator < member operators
	cout << "Using < operator: ";
	if (s3 < s4)
		cout << s3 << " is less than " << s4 << endl;
	else
		cout << s3 << " is NOT less than " << s4 << endl;
	cout << "Using < operator: ";
	if (s3 < "So Far So Good!")
		cout << s3 << " is less than " << "\"So Far So Good!\"" << endl;
	else
		cout << s3 << " is NOT less than " << "\"So Far So Good!\"" << endl;
	//Test operator > member operators
	cout << "Using > operator: ";
	if (s4 > s3)
		cout << s4 << " is greater than " << s3 << endl;
	else
		cout << s4 << " is NOT greater than " << s3 << endl;
	cout << "Using > operator: ";
	if (s3 > "So Far So Good!")
		cout << s3 << " is greater than " << "\"So Far So Good!\"" << endl;
	else
		cout << s3 << " is NOT greater than " << "\"So Far So Good!\"" << endl;
	//Test operator >= member operators
	cout << "Using >= operator: ";
	if (s4 >= s3)
		cout << s4 << " is greater than or equal to " << s3 << endl;
	else
		cout << s4 << " is NOT greater than or equal to " << s3 << endl;
	cout << "Using >= operator: ";
	if (s3 >= "So Far So Good!")
		cout << s3 << " is greater than or equal to " << "\"So Far So Good!\"" << endl;
	else
		cout << s3 << " is NOT greater than or equal to " << "\"So Far So Good!\"" << endl;
	//Test operator <= member operators
	cout << "Using <= operator: ";
	if (s3 <= s4)
		cout << s3 << " is less than or equal to " << s4 << endl;
	else
		cout << s3 << " is NOT less than or equal to " << s4 << endl;
	cout << "Using <= operator: ";
	if (s3 <= "So Far So Good!")
		cout << s3 << " is less than or equal to " << "\"So Far So Good!\"" << endl;
	else
		cout << s3 << " is NOT less than or equal to " << "\"So Far So Good!\"" << endl;

	cout << "\"HUH \" + " << s1 << " is " << "HUH " + s1 << endl;
	cout << "! + " << s1 << " is " << '!' + s1 << endl;
	//Test operator == friend function
	cout << "Using == friend function: ";
	if ("Yonas T. Weldeselassie" == s5)
		cout << "\"Yonas T. Weldeselassie\" is equal to " << s5 << endl;
	else
		cout << "\"Yonas T. Weldeselassie\" is NOT equal to " << s5 << endl;
	//Test operator != friend function
	cout << "Using != friend function: ";
	if ("Yonas T. Weldeselassie" != s5)
		cout << "\"Yonas T. Weldeselassie\" is NOT equal to " << s5 << endl;
	else
		cout << "\"Yonas T. Weldeselassie\" is equal to " << s5 << endl;
	//Test operator < friend function
	cout << "Using < friend function: ";
	if ("Yonas Weldeselassie" < s5)
		cout << "\"Yonas Weldeselassie\" is less than " << s5 << endl;
	else
		cout << "\"Yonas Weldeselassie\" is NOT less than " << s5 << endl;
	//Test operator > friend function
	cout << "Using > friend function: ";
	if ("Yonas Weldeselassie" > s5)
		cout << "\"Yonas Weldeselassie\" is greater than " << s5 << endl;
	else
		cout << "\"Yonas Weldeselassie\" is NOT greater than " << s5 << endl;
	//Test operator <= friend function
	cout << "Using <= friend function: ";
	if ("Yonas Weldeselassie" <= s5)
		cout << "\"Yonas Weldeselassie\" is less than or equal to " << s5 << endl;
	else
		cout << "\"Yonas Weldeselassie\" is NOT less than or equal to " << s5 << endl;
	//Test operator >= friend function
	cout << "Using >= friend function: ";
	if ("Yonas Weldeselassie" >= s5)
		cout << "\"Yonas Weldeselassie\" is greater than or equal to " << s5 << endl;
	else
		cout << "\"Yonas Weldeselassie\" is NOT greater than or equal to " << s5 << endl;
	//Test operator << friend function: Already tested above.
	//Test operator >> friend function: Already tested above.
	CMPT135_String s6;
	cout << "Enter a CMPT135_String object. Just type the characters: ";
	cin >> s6;
	cout << "You entered the CMPT135_String object \"" << s6 << "\"" << endl;
	cout << endl << "This completes the test program..." << endl << endl;


	system("pause");
	return 0;
}