﻿#pragma once
#include<iostream>
#include<string>
#include<vector>
#include<algorithm>
#include <conio.h>
#include <unordered_map>
#pragma warning(disable:4996)
using namespace std;


const int SIZE = 4;
int determinant(double a[][SIZE], int size)
{
	int idx = 0;
	for (int i = 1; i < size; ++i)
	{
		for (int k = i; k < size; ++k)
		{
			if (a[k][idx] != 0)
			{
				double div = a[k][idx] / a[i - 1][idx];
				for (int j = idx; j < size; ++j)
				{
					a[k][j] = a[k][j] - div * a[i - 1][j];
				}
			}
		}
		idx++;
	}
	double det = 1;
	for (int i = 0; i < size; ++i)
		det *= a[i][i];
	return det;
}
vector<int> twoSum(vector<int>& nums, int target) {
	int max1 = nums[0];
	int max2 = nums[0];
	int min = nums[0];
	for (int i : nums){
		if (max1 < i)
			max1 = i;
		if (min > i)
			min = i;

	}
	for (int i : nums)
	{
		if (max2 < i && i != max1)
			max2 = i;
	}
	int size = max1 + max2 > max1 - min ? max1 + max2 + 1 : max1 - min + 1; 
	vector<int> tmp(size, min - 1);
	vector<int> res;
	for (int i = 0; i < nums.size(); ++i)
	{

		if (tmp[nums[i] - min] != min - 1)
		{
			res.push_back(tmp[nums[i] - min] - (target - nums[i]));
			res.push_back(i);
			for (int i : res)
				cout << i << " ";
			return res;
		}
		else
		{
			if (target - nums[i] >= min)
				tmp[target - nums[i] - min] = nums[i] + i;
		}
	}
}
string integerVectorToString(vector<int> list, int length = -1) {
	if (length == -1) {
		length = list.size();
	}

	string result;
	for (int index = 0; index < length; index++) {
		int number = list[index];
		result += to_string(number) + ", ";
	}
	return result.substr(0, result.length() - 2);
}
//int main() {
//	//vector<int> test = { -1, -2, -3, -4, -5 };
//	//vector<int> res = twoSum(test, -8);
//	////string out = integerVectorToString(res);
//	////cout << out << endl;
//	char* t = "hello ";
//	printf(t);
//	system("pause");
//	return 0;
//}
#include <iostream>
using namespace std;

int my_strlen(char*tmp)
{
	int length = 0, i = 0;
	while (tmp[i++] != '\0')
		length++;
	return length;
}
int getTime(char* tmp)
{
	int t = 0, i = 0;
	int length = my_strlen(tmp);
	while (i < length && tmp[i] != ':')
		t = t * 10 + (tmp[i++] - '0');
	return t;
}

class Test
{
private:
	int n;
public:
	Test(int k)
	{
		n = k;
	}
	Test()
	{}
	void tset(int j)
	{
		n = j;
	}
	int tget() const
	{
		return n;
	}
	Test& operator = (const Test& t)
	{
		Test temp;
		this->n = t.tget();
		temp.n = t.tget();
		//return temp;
		return *this;
	}
	Test operator + (const Test& t)
	{
		Test a = t.tget() + this->n;
		return a;
	}
	Test& operator += (const Test& t)
	{
		n = t.tget() + n;
		return *this;
	}
	int operator [] (int i)
	{
		
		return 3;
	}
};

#include <vector>
void abc(vector<int> a)
{
	a.push_back(1);
}
class TEST{
private:
	int _a, _b;
public:
	void abc();
	TEST() {}
	TEST(int a, int b){
		_a = a;
		_b = b;
	}
	int operator () (int i, int j) const
	{
		return 3;
	}
	friend ostream& operator << (ostream& os, const TEST& a)
	{
		os << "ahihiih\n";
		return os;
	}
};
void TEST::abc(){
	cout << "ahihihi\n";
}
int* ttt(int a)
{
	return &a;
}
void print(int a)
{
	if (a < 10)
		cout << a;
	else{
		print(a / 10);
		cout << a % 10;
	}
}
#include <stdio.h>
#include <limits.h>

// Number of vertices in the graph
#define V 9

// A utility function to find the vertex with minimum key value, from
// the set of vertices not yet included in MST
int minKey(int key[], bool mstSet[])
{
	// Initialize min value
	int min = INT_MAX, min_index;

	for (int v = 0; v < V; v++)
		if (mstSet[v] == false && key[v] < min)
			min = key[v], min_index = v;

	return min_index;
}

// A utility function to print the constructed MST stored in parent[]
void printMST(int parent[], int n, int graph[V][V])
{
	printf("Edge   Weight\n");
	for (int i = 1; i < V; i++)
		printf("%d - %d    %d \n", parent[i], i, graph[i][parent[i]]);
}

// Function to construct and print MST for a graph represented using adjacency
// matrix representation
void primMST(int graph[V][V])
{
	int parent[V]; // Array to store constructed MST
	int key[V];   // Key values used to pick minimum weight edge in cut
	bool mstSet[V];  // To represent set of vertices not yet included in MST

	// Initialize all keys as INFINITE
	for (int i = 0; i < V; i++)
		key[i] = INT_MAX, mstSet[i] = false;

	// Always include first 1st vertex in MST.
	key[0] = 0;     // Make key 0 so that this vertex is picked as first vertex
	parent[0] = -1; // First node is always root of MST 

	// The MST will have V vertices
	for (int count = 0; count < V - 1; count++)
	{
		// Pick the minimum key vertex from the set of vertices
		// not yet included in MST
		int u = minKey(key, mstSet);

		// Add the picked vertex to the MST Set
		mstSet[u] = true;
		cout << parent[u] << " " << u << endl;
		// Update key value and parent index of the adjacent vertices of
		// the picked vertex. Consider only those vertices which are not yet
		// included in MST
		for (int v = 0; v < V; v++)

			// graph[u][v] is non zero only for adjacent vertices of m
			// mstSet[v] is false for vertices not yet included in MST
			// Update the key only if graph[u][v] is smaller than key[v]
			if (graph[u][v] && mstSet[v] == false && graph[u][v] <  key[v])
				parent[v] = u, key[v] = graph[u][v];
	}

	// print the constructed MST
	//printMST(parent, V, graph);
}

//int _test(long long int n)
//{
//	if (n < 10)
//		return n;
//	long long int i = 10, j = 10, res;
//	while (i < n){
//		res = j;
//		int tmp = (int)log10((double)res) + 1;
//		i += tmp;
//		j++;
//	}
//	if (i == n){
//		return (double)n / (int)pow(10.0, (int)log10((double)j));
//	}
//	j--;
//	i--;
//	while (i != n){
//		j /= 10;
//		i--;
//	}
//	return j % 10;
//}
int _test(long long int n){
	if (n < 10)
		return n;
	if (n == 10)
		return 1;
	int bound = 1;
	long long int ii = 10, i = 10, j = 9, t = 1;
	do{
		ii = i;
		t++;
		j *= 10;
		bound *= 10;
		i = i + t * j;
	} while (i < n);
	if (n == i)
		return 1;
	if (t == 2)
	{
		int tmp = (n - 10) / 2 + 10;
		if (n % 2 == 0)
			return (tmp / 10) % 10;
		return tmp % 10;
	}
	else{
		long long int tmp = (n - ii) / t;
		tmp = bound + tmp;
		long long int tmp2 = ii + tmp * t;
		t = tmp2 + t - 1;
		tmp /= pow(10.0, (double)tmp2 + t - 1 - n);
		return tmp % 10;
	}
}
// driver program to test above function
int main()
{
	/* Let us create the following graph
	2    3
	(0)--(1)--(2)
	|   / \   |
	6| 8/   \5 |7
	| /     \ |
	(3)-------(4)
	9          */
	//int graph[V][V] = { { 0,	10,	12,	0,	0,	0,	0,	0,	0, },
	//{10,	0,	9,	8,	0,	0,	0,	0,	0},
	//	{12,	9,	6,	0,	3,	1,	0,	0,	0},
	//	{0,	8,	0,	6,	7,	100,	8,	5,	0},
	//	{0,	0,	3,	7,	0,	3,	0,	0,	0},
	//	{0,	0,	1,	100,	3,	0,	0,	6,	0},
	//	{0,	0,	0,	8,	0,	0,	0,	9,	2},
	//	{0,	0,	0,	5,	0,	6,	9,	0,	11},
	//	{0,	0,	0,	0,	0,	0,	2,	11,	0}
	//};

	// Print the solution
	//primMST(graph);
	long long int n;
	cin >> n;
	cout << _test(n);
	system("pause");
	return 0;
}