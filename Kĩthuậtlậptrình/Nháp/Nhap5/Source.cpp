#include <vector>
#include <iostream>
#include <fstream>
#include <algorithm>
#include<string>
using namespace std;

const int max_applications_num = 1000;

//ToDo  //Declare global variables
vector< vector<string> > applicationList;

void Tokenize(string line, vector<string> & tokens, string delimiters = "\t "){
	string token = "";
	string OneCharString = " ";
	for (int i = 0; i<line.size(); i++)
		if (find(delimiters.begin(), delimiters.end(), line[i]) != delimiters.end()) // line[i] is one of the delimiter characters
		{
			if (token != "")
				tokens.push_back(token);
			token = "";
		}
		else
		{
			OneCharString[0] = line[i];
			token += OneCharString;
		}

	if (token != "")
		tokens.push_back(token);
}

void SaveApplication(const vector<string>& tokens){

	string authors = tokens[1];
	string title = tokens[2];
	string venue = tokens[3];
	int year = atoi(tokens[4].c_str());
	string presentation = tokens[5];
	vector<string> temp = tokens;
	temp.erase(temp.begin());
	applicationList.push_back(temp);
	//ToDo
}

void remove_application(int pos){

	//ToDo
	applicationList.erase(applicationList.begin() + pos);
}


void sort(){

	//ToDo
	int i = 1;
	while (i < applicationList.size())
	{
		int j = i;
		vector<string> temp = applicationList[i];
		while (j >= 1 && atoi(applicationList[j - 1][3].c_str()) > atoi(applicationList[j][3].c_str()))
		{
			applicationList[j] = applicationList[j - 1];
			j--;
		}
		applicationList[j] = temp;
		i++;
	}
}

void print(){
	//ToDo
	cout << "hello" << endl;
	for (int i = 0; i < applicationList.size(); ++i)
	{
		vector<string> temp = applicationList[i];
		cout << temp[0] << "\t" << temp[1] << "\t" << temp[2] << "\t" << temp[3] << "\t" << temp[4] << endl;
	}
	cout << endl;
}

void ExecuteCommands(){
	ifstream inf;
	inf.open("commands.txt");

	string line;
	while (getline(inf, line).good()){
		vector<string> tokens;
		Tokenize(line, tokens, "\t ");
		if (tokens.size() == 0)
			continue;

		if (tokens[0].compare("save_application") == 0)
			SaveApplication(tokens);

		else if (tokens[0].compare("remove_application") == 0)
			remove_application(atoi(tokens[1].c_str()));

		else if (tokens[0].compare("sort") == 0)
			sort();

		else if (tokens[0].compare("print") == 0)
			print();
	}

	inf.close();
}

int main(){

	ExecuteCommands();
	system("pause");
	return 0;
}

