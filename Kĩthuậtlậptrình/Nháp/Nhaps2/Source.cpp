﻿#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#pragma warning (disable:4996)
# define MAX 300
using namespace std;
void nhap(char s[])
{
	printf("\n\t NHAP SO CAN TINH VAO DAY :");
	gets(s);
}

void xoakytu(char s[], int &n, int vitri)
{
	for (int i = vitri + 1; i < n; i++)
	{
		s[i - 1] = s[i];
	}
	n--;

}

void chuanhoachuoi(char s[])
{
	int dodai = strlen(s);
	for (int i = 0; i < dodai; i++)
	{
		if (s[i] == ' ')
		{
			xoakytu(s, dodai, i);
			i--;
		}
	}
	s[dodai] = '\0';
}

void themso(char s[], int vitri, char phantuthem)
{
	int n = strlen(s);
	for (int i = n - 1; i >= vitri; i--)
	{
		s[i + 1] = s[i];
	}
	s[vitri] = phantuthem;
	s[n + 1] = '\0';
}

void chuanhoadodai(char s1[], char s2[])
{
	int dodais1 = strlen(s1);
	int dodais2 = strlen(s2);
	if (dodais1 > dodais2)
		for (int i = 1; i <= dodais1 - dodais2; i++)
		{
			themso(s2, 0, '0');
		}
	else if (dodais1 < dodais2)
		for (int i = 1; i <= dodais2 - dodais1; i++)
		{
			themso(s1, 0, '0');
		}
}

char* tinhtong(char s1[], char s2[])
{
	chuanhoachuoi(s1);
	chuanhoachuoi(s2);
	chuanhoadodai(s1, s2);
	int n = strlen(s1);
	int nho = 0;
	char ketqua[MAX];
	int luu = 0;
	for (int i = n - 1; i >= 0; i--)
	{
		int sothu1 = s1[i] - 48;//doi ky tu sang so
		int sothu2 = s2[i] - 48;
		int tong = sothu1 + sothu2 + nho;//tong cong cac so 
		nho = tong / 10;// lay phan nguyen neu lon hon 10
		tong %= 10;// lay phan du 
		ketqua[luu++] = tong + 48;//doi so sang ky tu va luu vao mang
	}
	if (nho != 0)
	{
		ketqua[luu] = nho + 48;
	}
	ketqua[luu++] = '\0';
	strrev(ketqua); //dao chuoi lai
	return ketqua;
}


char* tinhhieuphu(char s1[], char s2[])
{
	chuanhoachuoi(s1);
	chuanhoachuoi(s2);
	chuanhoadodai(s1, s2);
	int n = strlen(s1);
	char ketqua[MAX];
	int nho = 0;
	int luu = 0;
	for (int i = n - 1; i >= 0; i--)
	{
		int sothu1 = s1[i] - 48;
		int sothu2 = s2[i] - 48;
		sothu1 = sothu1 - nho;
		nho = 0;
		if (i == 0)
		{
			if (sothu1 == 0)
			{
				break;
			}
		}
		int tong = 0;
		if (sothu1 >= sothu2)
		{
			tong = sothu1 - nho - sothu2;
		}
		else
		{
			tong = (10 + sothu1) - sothu2;
			nho++;
		}
		ketqua[luu++] = tong + 48;
	}
	ketqua[luu] = '\0';
	strrev(ketqua);
	return ketqua;
}

char* tinhhieuchinh(char s1[], char s2[])
{
	char ketqua[MAX];
	chuanhoachuoi(s1);
	chuanhoachuoi(s2);
	int dodai1 = strlen(s1);
	int dodai2 = strlen(s2);
	if (dodai1 > dodai2)
	{
		strcpy(ketqua, tinhhieuphu(s1, s2));
	}
	else if (dodai1 < dodai2)
	{
		strcpy(ketqua, tinhhieuphu(s2, s1));
		themso(ketqua, 0, '-');
	}
	else
	{
		int sothu1 = 0, sothu2 = 0;

		for (int i = 0; i < dodai1; i++)
		{
			sothu1 = s1[i] - 48;
			sothu2 = s2[i] - 48;
			break;
		}
		if (sothu1 > sothu2)
		{
			strcpy(ketqua, tinhhieuphu(s1, s2));
		}
		else
		{
			strcpy(ketqua, tinhhieuphu(s2, s1));
			themso(ketqua, 0, '-');
		}
	}
	return ketqua;
}
bool checkPrime(int n)
{
	if (n == 2)
		return true;
	if (n % 2 == 0)
		return false;
	if (n < 2)
		return false;
	else
	{
		for (int j = 3; j <= sqrt((double)n); j += 2)
			if (n % j == 0)
				return false;
	}
	return true;

}
void stringappend(string &s, int n)
{
	if (n < 10)
	{
		char k = n + 48;
		s = s + k;
		return;
	}
	string temp;
	while (n > 0)
	{
		char k = n % 10 + 48;
		temp = temp + k;
		n /= 10;
	}
	for (int j = temp.length() - 1; j >= 0; --j)
	{
		s += temp[j];
	}
}
std::string primeClimb(int n) {
	string s;
	if (checkPrime(n) == true)
	{
		stringappend(s, n);
		return s;
	}
	int length = sqrt((double)n);
	int *a = new int[length + 1];
	int i = 0;
	for (int j = 2; i <= length;)
	{
		if (checkPrime(j))
		{
			a[i] = j;
			++i;
		}
		if (j % 2 == 0)
			j++;
		else
			j += 2;
	}
	i = 0;
	while (i <= length && n >= 1)
	{
		if (n % a[i] == 0)
		{
			stringappend(s, a[i]);
			n /= a[i];
		}
		else
			i++;
	}
	return s;
}
int countTwoPrimes(int n, int m) {
	int a[1000];
	int i = 0;
	for (int j = 2; i < m;)
	{
		if (checkPrime(j))
		{
			a[i] = j;
			++i;
		}
		if (j % 2 == 0)
			j++;
		else
			j += 2;
	}
	int kq = a[n - 1] + a[m - 1];
	return kq;
}
void insertion_sort(int a[], int size)
{
	int i = 0;
	while (i < size)
	{
		int temp = a[i];
		int index = i;
		while (temp < a[index - 1] && index > 0)
		{
			a[index] = a[index - 1];
			index--;
		}
		a[index] = temp;
		i++;
	}
}
int a[8] = { 0, -1, 2, 3, 3, 5, 6, 0 };
void Chen(int &n, int x) {
	if (n == 0 || x >= a[n]) {
		a[n + 1] = x;
		n++;
	}
	else {
		a[n + 1] = a[n];
		Chen(--n, x);
		n++;
	}
}
int i, j;
void LinearFib(int k)
{
	if (k <= 1)
	{
		i = j = k; 
		return;
	}
	else
	{
		
		LinearFib(k - 1);
		int k = j;
		j = i;
		i += k;
		
	}
}
void Permutation(string sequence, string set)
{
	if (set.length() == 0)
		cout << sequence << endl;
	else{
		for (int i = 0; i < set.length(); ++i)
		{
			char e = set[i];
			set.erase(set.begin() + i);
			sequence += e;
			Permutation(sequence, set);
			set.insert(set.begin() + i, e);
			sequence.erase(sequence.length() - 1);
		}
	}
}
int convert1(string temp, int rest = 0, int i = 0)
{
	if (i == temp.length()){
		return rest;
	}
	else
	{
		rest = rest * 10 + (temp[i] - 48);
		return convert1(temp, rest, i + 1);
	}
}
void subset(string temp, string test, int j = 0)
{
	if (test.length() == 0)
		return;
	else
	{
		for (int i = j; i < test.length(); ++i, j++)
		{
			char c = test[i];
			test.erase(test.begin() + i);
			temp += c;
			cout << temp << endl;
			subset(temp, test, j);
			test.insert(test.begin() + i, c);
			temp.erase(temp.length() - 1);
		}
	}
}
int checkPalindrome(string test, int i, int j)
{
	int ii = i;
	int jj = j;
	for (; ii > 0 && jj < test.length() - 1; ii--, ++jj)
	{
		if (test[ii - 1] != test[jj + 1])
			break;
	}
	return jj - ii + 1;
	
}
int palindrome(string test)
{
	int longest = 1;
	for (int i = 1; i < test.length(); ++i)
	{
		if (test[i - 1] == test[i + 1])
			longest = max(longest, checkPalindrome(test, i, i));
		if (test[i] == test[i - 1])
			longest = max(longest, checkPalindrome(test, i - 1, i));
	}
	return longest;

}
bool check1(string t)
{
	int count = 0;
	for (int i = 0; i < t.length(); ++i)
	{
		if (t[i] == '1')
			count++;
	}
	return count == t.length();
}
void nextString(string &s)
{
	for (int i = s.length() - 1; i >= 0; --i)
	{
		if (s[i] == '0')
		{
			s[i] = '1';
			for (int j = i + 1; j < s.length(); ++j)
				s[j] = '0';
			break;
		}

	}
}
void showString(string t)
{

	for (int i = 0; i < t.length(); ++i)
		cout << t[i] << " ";
	cout << endl;
}
void showArray(int *a, int k)
{
	for (int i = 0; i < k; ++i)
		cout << a[i] << " ";
	cout << endl;
}
int main()
{
	int n, k;
	cin >> n >> k;
	int *a = new int[k];
	for (int i = 0; i < k; ++i)
		a[i] = i + 1;
	while (true){
		showArray(a, k);
		int i = k - 1;
		while (i >= 0 && a[i] == n - k + i + 1) i--;
		if (i >= 0)
		{
			a[i]++;
			for (int j = i + 1; j < k; ++j)
				a[j] = a[j - 1] + 1;
		}
		else
			break;
	}
	

	system("pause");
	return 0;
}