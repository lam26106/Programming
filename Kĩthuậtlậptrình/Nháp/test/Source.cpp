﻿#include <iostream>
using namespace std;

class PrimeNumber
{
public:
	PrimeNumber();
	PrimeNumber(int);
	int getNumber();
	friend PrimeNumber operator ++ (PrimeNumber&);
	friend PrimeNumber operator -- (PrimeNumber&);
	friend ostream& operator << (ostream&, const PrimeNumber&);

	bool operator > (const PrimeNumber&);
private:
	int number;
};

bool isPrime(int num)
{
	if (num == 2)
		return true;
	else if (num < 2)
		return false;
	else
	{
		for (int i = 2; i <= num / 2; i++)
		{
			if (num % i == 0)
				return false;
		}
		return true;
	}
}
PrimeNumber::PrimeNumber()
{
	number = 2;
}
PrimeNumber::PrimeNumber(int initNumber)
{
	if (isPrime(initNumber))
		number = initNumber;
	else
		cout << ":)";
}
int PrimeNumber::getNumber()
{
	return number;
}
PrimeNumber operator ++ (PrimeNumber& num)
{
	int found = num.number + 1;
	while (isPrime(found) == 0)
		found++;
	num = PrimeNumber(found);
	return num;
}
PrimeNumber operator -- (PrimeNumber& num)
{
	int found;
	if (num.number == 2)
		return num;
	else
		found = num.number - 1;
	while (isPrime(found) == 0)
		found--;
	num = PrimeNumber(found);
	return num;
}
ostream& operator << (ostream& outs, const PrimeNumber& num)
{
	outs << num.number;
	return outs;
}

bool PrimeNumber::operator > (const PrimeNumber& num)
{
	PrimeNumber N = num;
	return (number > N.getNumber());
}


int main()
{
	int a = 3, b = 5;
	a = ++++b;
	cout << b;
	system("pause");
	return 0;
}