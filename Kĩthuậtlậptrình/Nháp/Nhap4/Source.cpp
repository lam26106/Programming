#include <iostream>
#include <cstring>
using namespace std;

int my_strlen(char*str)
{
	int count = 0;
	for (int i = 0; str[i] != '\0'; ++i)
		count++;
	return count;
}
void addChar(char*&str, char item, int index)
{
	int t = strlen(str);
	char *result = new char[t + 2];
	int indexMax = t;
	for (int i = indexMax; i >= index; --i)
	{
		result[i + 1] = str[i];
	}
	result[index] = item;
	for (int i = 0; i < index; ++i)
		result[i] = str[i];
	str = result;
}
void deleteChar(char *&str, int index)
{
	int t = strlen(str);
	char *temp = new char[t];
	int indexMax = t;
	for (int i = index; i <= indexMax; ++i)
		temp[i] = str[i + 1];
	for (int i = 0; i < index; ++i)
		temp[i] = str[i];
	str = temp;

}
char* addOne(char *str)
{
	int t = strlen(str);
	char *s = "1";
	int commonlength = t > 1 ? t + 1 : 2;
	char *result = new char[commonlength + 1];
	int numOfzero1 = commonlength - 1;
	int numOfzero2 = commonlength - strlen(str);
	for (int i = 0; i < numOfzero1; ++i)
		addChar(s, '0', 0);
	for (int i = 0; i < numOfzero2; ++i)
		addChar(str, '0', 0);
	int nho = 0;
	for (int i = commonlength - 1; i >= 0; --i)
	{
		int kq = str[i] - 48 + s[i] - 48 + nho;
		result[i] = kq % 10 + 48;
		nho = kq / 10;
	}
	result[commonlength] = '\0';
	for (int i = 0;; ++i)
	{
		if (result[i] == '0')
		{
			deleteChar(result, i);
			i--;
		}
		else
			break;
	}
	return result;

}
bool check(char *str)
{
	int t = strlen(str);
	char*temp = new char[t + 1];
	int j = t - 1;
	for (int i = 0; i < t; ++i)
	{
		temp[i] = str[j--];
	}
	temp[strlen(str)] = '\0';
	if (strcmp(temp, str) == 0)
		return true;
	return false;
}
#define LENGTH(a) (sizeof(a)/sizeof(a[0]))

bool check(int* des, int* src, int x, int size1, int size2)
{
	for (int i = 0; i < size2 || i < size1; i++)
	{
		if (src[x] == des[i]) return false; //with another array
		if (src[x] == src[i] && i != x) return false; //within array
	}
	return true;
}
void print(int* src, int* des, int size1, int size2)
{
	for (int i = 0; i < size1; i++)
	{
		if (check(des, src, i, size1, size2))
		{
			std::cout << src[i] << " ";
		}
	}
}
int main() {
	int a[] = { 3, 5, 7, 4, 3 };
	int b[] = { 2 ,5, 6, 12, 8, 7, 5 };
	int n1 = LENGTH(a);
	int n2 = LENGTH(b);

	print(a, b, n1, n2);
	print(b, a, n2, n1);
	system("pause");
	return 0;
}
//int main()
//{
//	int t;
//	cin >> t;
//	char **main = new char*[t];
//	for (int i = 0; i < t; ++i)
//	{
//		main[i] = new char[1000000];
//		cin >> main[i];
//	}
//	cout << endl;
//	for (int i = 0; i < t; ++i){
//		for (int j = 1;; ++j)
//		{
//			char *temp = addOne(main[i]);
//			main[i] = temp;
//			if (check(main[i]) == true)
//			{
//				cout << main[i];
//				break;
//			}
//		}
//		cout << endl;
//	}
//	system("pause");
//	return 0;
//}