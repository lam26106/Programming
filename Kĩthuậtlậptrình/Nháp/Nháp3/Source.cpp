#include <iostream>
#include <math.h>
#include <vector>
#include <string>
#include <stdlib.h>
using namespace std;

int max(int a, int b)
{
	return a > b ? a : b;
}
int expand(const string& s, int left, int right) {
	// given a palindrome substring: s[left..right], we expand [left, right] to
	// find the maximum palindrome substring with center [left..right].
	// returns: length of maximum palindrome substring with center [left..right].

	while (left > 0 && right + 1 < s.length() && s[left - 1] == s[right + 1]) {
		left--;
		right++;
	}
	return right - left + 1;
}

int max_palindrome_substring(string s) {
	// Given string s, find its longest palindrome substring.
	// Returns: length of longest palindrome substring.
	int result = 0;  // our result

	for (int center = 0; center < s.length(); center++) {
		// odd-length substrings
		result = max(result, expand(s, center, center));

		// even-length substrings
		if (center + 1 < s.length() && s[center] == s[center + 1]) {
			result = max(result, expand(s, center, center + 1));
		}
	}
	return result;
}
void addChar(char*&str, char item, int index)
{
	char *result = new char[strlen(str) + 2];
	int indexMax = strlen(str);
	for (int i = indexMax; i >= index; --i)
	{
		result[i + 1] = str[i];
	}
	result[index] = item;
	for (int i = 0; i < index; ++i)
		result[i] = str[i];
	//delete[]str;
	str = result;
	//cout << str << endl;
}
void deleteChar(char *&str, int index)
{
	char *temp = new char[strlen(str)];
	int indexMax = strlen(str);
	for (int i = index; i <= indexMax; ++i)
		temp[i] = str[i + 1];
	for (int i = 0; i < index; ++i)
		temp[i] = str[i];
	//delete[]str;
	str = temp;
	
}
char* addOne(char *str)
{
	char *s = "1";
	int commonlength = strlen(str) > 1 ? (strlen(str) + 1 ): 2;
	char *result = new char[commonlength + 1];
	int numOfzero1 = commonlength - 1;
	int numOfzero2 = commonlength - strlen(str);
	for (int i = 0; i < numOfzero1; ++i)
		addChar(s, '0', 0);
	for (int i = 0; i < numOfzero2; ++i)
		addChar(str, '0', 0);
	int nho = 0;
	for (int i = commonlength - 1; i >= 0; --i)
	{
		int kq = str[i] - 48 + s[i] - 48 + nho;
		result[i] = kq % 10 + 48;
		nho = kq / 10;
	}
	result[commonlength] = '\0';
	for (int i = 0;; ++i)
	{
		if (result[i] == '0')
		{
			deleteChar(result, i);
			i--;
		}
		else
			break;
	}
	return result;

}
bool check(char *str)
{
	char*temp = new char[strlen(str) + 1];
	int j = strlen(str) - 1;
	for (int i = 0; i < strlen(str); ++i)
	{
		temp[i] = str[j--];
	}
	temp[strlen(str)] = '\0';
	if (strcmp(temp, str) == 0)
		return true;
	return false;
}
#define N 4

void print_matrix(char *A, int n) {
	int i, j;
	char val;
	for (i = 0; i < N; i++) {
		for (j = 0; j < N; j++) {
			val = *((char*)(A + i*n + j));
			printf("\t%2x", val & 0xff);
		}
		putchar('\n');
	}
	putchar('\n');
}
//void print_matrix(char A[][N], int n)
//{
//	for (int i = 0; i < N; ++i)
//	{
//		for (int j = 0; j < N; ++j)
//			cout << (int)A[i][j] << "\t";
//		cout << endl;
//	}
//	cout << "\n\n";
//}
void squareMatrix_multipliation(char a[][N], char b[][N], char c[][N], int n)
{
	for (int i = 0; i < n; ++i)
	{
		for (int j = 0; j < n; ++j)
		{
			int test = 0;
			c[i][j] = 0;
			for (int k = 0; k < n; ++k)
			{
				int bb = a[i][k];
				int cc = b[k][j];
				c[i][j] = c[i][j] + a[i][k] * b[k][j];
				test = test + bb*cc;
				
			}
			//cout << (int)c[i][j] << "\t";
			cout << test << "\t";
			//c[i][j] = c[i][j] % 17;
			//test = test % 17;
			//if (c[i][j] < 0)
			//	c[i][j] += 17;
			//if (test < 0)
			//	test += 17;
			
		}
		cout << endl;
	}
	cout << "\n\n";
}  
//int main()
//{
//
//	char A[N][N] = { 5, 11, 10, 3,
//		10, 6, 2, 1,
//		6, 3, 14, 13,
//		8, 5, 2, 0 };
//
//	char B[N][N] = { 10, 12, 14, 5,
//		12, 10, 1, 1,
//		12, 0, 12, 10,
//		12, 0, 1, 10 };
//
//	char C[N][N];
//	squareMatrix_multipliation(A, B, C, N);
//	print_matrix(*A, N);
//	print_matrix(*B, N);
//	print_matrix(*C, N);
//	system("pause");
//	return 0;
//
//}
//vector<tree> test;
vector<string> a;
int cc = 0;
void printGarden()
{
	for (int i = 0; i < 10; ++i)
		cout << a[i] << endl;
}
bool cleanGarden_v2(int i, int j)
{
	//printGarden();
	//cout << "\t" << cc << endl << endl;
	if (i < 0 || j < 0 || i >= 10 || j >= 12 || a[i][j] == '.' || a[i][j] == '1')
	{
		return false;
	}
	else if (a[i][j] == 'W')
	{
		a[i][j] = '1';
		bool check = cleanGarden_v2(i - 1, j - 1) || cleanGarden_v2(i - 1, j) || cleanGarden_v2(i - 1, j + 1) ||
			cleanGarden_v2(i, j - 1) || cleanGarden_v2(i, j + 1) || cleanGarden_v2(i + 1, j - 1) ||
			cleanGarden_v2(i + 1, j) || cleanGarden_v2(i + 1, j + 1);
		return check; 

	}
}
#define TEST for(int i = 0; i < 10; ++i) cout << "i";
//int main()
//{
//	//a = {{"W...W...WW."},
//	//	{".WWW.....WWW"},
//	//	{"....WW...WW."},
//	//	{".........WW."},
//	//	{"......WW.W.."},
//	//	{"..W......W.."},
//	//	{".W.W.....W.."},
//	//	{"W.W.W....WW."},
//	//	{".W.W......W."},
//	//	{"..W.......W."} };
//	//cleanGarden_v2(0, 8);
//	//for (int i = 0; i < 10; ++i)
//	//	for (int j = 0; j < 12; ++j)
//	//	{
//	//		if (a[i][j] == 'W')
//	//		{
//	//			bool check = cleanGarden_v2(i, j);
//	//			if (check == false){
//	//				cc++;
//	//				cout << i << " " << j << endl;
//	//			}
//	//			else
//	//			{
//	//				//cout << i << " " << j << endl;
//	//			}
//	//				
//	//		}
//	//	}
//	//cleanGarden_v2(0, 0);
//	//printGarden();
//	//cout << cc << endl;
//	TEST;
//	system("pause");
//	return 0;
//}
void mystery2(int n){
	for (int i = 0; i<n; i++)
		cout << i << endl;
	if (n>1)
		mystery2(n / 2);
}void mys(int n)
{
	for (int i = n; i >= 1; i/=2)
		for (int j = 0; j < i; j++)
			cout << j << endl;
}
void mystery4(int n){
	cout << n << endl;
	if (n>1){
		mystery4(n / 2);
		mystery4(n / 2);
	}
}void mys2(int n)
{
	for (int i = n; i > 1; i /= 2)
	{
		for (int j = i; j >= 1; j /= 2) cout << j << endl;
		for (int j = i; j >= 1; j /= 2) cout << j << endl;
	}
}
int *new_42(void) {
	int x;
	int *ret;

	x = 42;
	ret = &x;

	return ret;
} 
vector<int> aa(3, 0);
int main()
{
	char *k = "-1";
	int temp = atoi(k);
	cout << temp;
	system("pause");
	return 0;
}
