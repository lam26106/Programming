#include <iostream>
#include <Windows.h>
#include <iomanip>
using namespace std;



int main()
{
	int hour(0), minute(0), second(0);
	int check = 0;
	while (true && check == 0)
	{
		second %= 60;
		second++;
		minute += second / 60;
		minute %= 60;
		hour += minute / 60;
		hour %= 24;
		cout << setfill('0') << setw(2) << hour << ":";
		cout << setfill('0') << setw(2) << minute << ":";
		cout << setfill('0') << setw(2) << second;


		Sleep(1000);
		system("cls");
	}
	system("pause");
	return 0;
}