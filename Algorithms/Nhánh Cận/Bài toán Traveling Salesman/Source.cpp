﻿#include<iostream>
#include <vector>


using namespace std;

const int maxN = 100;
const int maxE = 10000;
const int maxC = maxN * maxE;
int **cost;
vector<int> bestWay;
vector<int> x; // Mảng chứa các thành phố lần lượt đi qua, thành phố đầu tiên và tp cuối cùng đều là 1
vector<int> Free;
vector<int> T; // T[i] la chi phi đi từ x[1] -> x[2] -> ... ->x[i]
int minCost = maxC;

void TravelingSalesMan(int n, int index = 2)
{
	for (int j = 2; j < n + 1; ++j)
	{
		if (Free[j] == 0)
		{
			x[index] = j;
			T[index] = T[index - 1] + cost[x[index - 1]][j];
			if (T[index] < minCost)
			{
				if (index < n)
				{
					Free[j] = 1;
					TravelingSalesMan(n, index + 1);
					Free[j] = 0;
				}
				else
				{
					if (T[index] + cost[x[index]][1] < minCost)
					{
						bestWay = x;
						minCost = T[index] + cost[x[index]][1];
					}
				}
			}
		}
	}
}
int main()
{
	int n, m;
	cin >> n >> m;
	cost = new int*[n + 1];
	for (int i = 0; i < n + 1; ++i)
		cost[i] = new int[n + 1];
	for (int i = 0; i < n + 1; ++i)
		for (int j = 0; j < n + 1; ++j)
		{
			if (i == j) cost[i][j] = 0;
			else
				cost[i][j] = maxC;
		}
	for (int k = 1; k <= m; ++k)
	{
		int i, j, c;
		cin >> i >> j >> c;
		cost[i][j] = cost[j][i] = c;
	}
	Free.resize(n + 1, 0);
	Free[0] = 1;
	x.resize(n + 1);
	x[1] = 1; 
	T.resize(n + 1);
	T[1] = 0;
	TravelingSalesMan(n);
	for (int i = 1; i < bestWay.size(); ++i)
		cout << bestWay[i] << " ";
	cout << "1" << endl << "Cost: " << minCost;
	system("pause");
	return 0;
}