﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuickSort
{
    class Program
    {
        public static void QuickSort(List<int> array)
        {
            if (array.Count >= 2)
            {
                List<int> L = new List<int>();
                List<int> E = new List<int>();
                List<int> R = new List<int>();
                int pivot = array[0];
                foreach (var item in array)
                {
                    if (item < pivot)
                        L.Add(item);
                    else if (item > pivot)
                        R.Add(item);
                    else
                        E.Add(item);
                }
                QuickSort(L);
                QuickSort(R);
                int index = 0;
                foreach (var item in L)
                    array[index++] = item;
                foreach (var item in E)
                    array[index++] = item;
                foreach (var item in R)
                    array[index++] = item;
                
            }

        }
        static void Main(string[] args)
        {
            List<int> a = new List<int>{ 3, 5, 2, 9, 4, 6, 1 };
            QuickSort(a);
            foreach (var item in a)
            {
                Console.Write(item + "  ");
            }
            Console.ReadKey();
        }
    }
}
