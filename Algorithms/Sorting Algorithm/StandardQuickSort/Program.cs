﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StandardQuickSort
{
    class Program
    {
        public static void mySwap(ref int a, ref int b)
        {
            int temp = a;
            a = b;
            b = temp;
        }
        public static void QuickSort(int[] a, int left, int right)
        {
            if (left +1 < right)
            {
                int pivot = a[right];
                int j = 0;
                for (int i = 0; i <= right; ++i)
                {
                    if (a[i] < pivot)
                    {
                        mySwap(ref a[i], ref a[j++]);
                    }
                }
                mySwap(ref a[right], ref a[j]);
                QuickSort(a, left, j - 1);
                QuickSort(a, j + 1, right);
            }
        }

        static void Main(string[] args)
        {
            int[] a = { 3, 5, 1, 2, 9, 7, 10, 14 };
            QuickSort(a, 0, 7);
            for (int i = 0; i < 8; ++i )
            {
                Console.Write(a[i] + " ");
            }
            Console.ReadKey();
        }
    }
}
