﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Merge_Sort
{
    class Program
    {
        public static void Merge(int[]array, int left, int middle, int right)
        {
            int[] tmp = new int[right - left + 1];
            int i = left, j = middle + 1;
            int index = 0;
            while(i <= middle && j <= right)
            {
                if (array[i] < array[j])
                    tmp[index++] = array[i++];
                else
                    tmp[index++] = array[j++];
            }
            while (i <= middle)
                tmp[index++] = array[i++];
            while (j <= right)
                tmp[index++] = array[j++];
            for (i = left, index = 0; i <= right; ++i, ++index)
                array[i] = tmp[index];

        }
        public static void MergeSort(int[]array, int left, int right)
        {
            if (left < right)
            {
                int middle = (left + right) / 2;
                MergeSort(array, left, middle);
                MergeSort(array, middle + 1, right);
                Merge(array, left, middle, right);
            }
        }
        static void Main(string[] args)
        {
            int[] a = { 3, 5, 2, 9, 4, 6, 1 };
            MergeSort(a, 0, 6);
            foreach (var item in a)
                Console.Write(item + " ");
            Console.ReadKey();
        }
    }
}
