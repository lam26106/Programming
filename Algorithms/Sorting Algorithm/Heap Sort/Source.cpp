#include<iostream>
using namespace std;

void _swap(int *a, int *b)
{
	int c = *a;
	*a = *b;
	*b = c;
}
void heapify(int *a, int last, int begin)
{
	int ii = begin;
	int left = begin * 2;
	int right = begin * 2 + 1;
	if (left < last && a[left] < a[ii])
		ii = left;
	if (right < last && a[right] < a[ii])
		ii = right;
	if (ii != begin)
	{
		_swap(&a[ii], &a[begin]);
		heapify(a, last, ii);
	}
}
void heapSort(int *a, int size)
{
	for (int i = size / 2 - 1; i >= 0; --i)
		heapify(a, size, i);
	for (int i = size - 1; i >= 0; --i)
	{
		swap(a[0], a[i]);
		heapify(a, i, 0);
	}
}
int main()
{
	int a[] = { 1, 2, 3, 4, 5, 6 };
	heapSort(a, 6);
	for (int i = 0; i < 6; ++i) cout << a[i] << " ";
	cout << endl;
	system("pause");
	return 0;
}