#include <iostream>
#include <iomanip>
using namespace std;

int ii[] = { -2, -1, +1, +2, +2, +1, -1, -2 };
int jj[] = { +1, +2, +2, +1, -1, -2, -2, -1 };
int matrix[8][8];

void show()
{
	for (int i = 0; i < 8; ++i){
		for (int j = 0; j < 8; ++j) cout << setw(3) << matrix[i][j] << " ";
		cout << endl;
	}
	cout << endl;
}
bool check = false;
void MaDiTuan(int i, int j,int count = 2)
{
	if (check == false){
		if (count > 64)
			check = true, show();
		else{
			for (int k = 0; k < 8; ++k)
			{
				int _i = i + ii[k];
				int _j = j + jj[k];
				if (_i >= 0 && _i < 8 && _j >= 0 && _j < 8)
				{
					if (matrix[_i][_j] == 0){
						matrix[_i][_j] = count;
						MaDiTuan(_i, _j, count + 1);
						matrix[_i][_j] = 0;
					}
				}
			}
		}
	}
	
}
int main()
{
	int i = 7;
	int j = 3;
	for (int i = 0; i < 8; ++i)
		for (int j = 0; j < 8; ++j)
			matrix[i][j] = 0;
	matrix[i][j] = 1;
	MaDiTuan(i, j);
	system("pause");
	return 0;
}