﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KiTuLapTrong2Xau
{
    class Program
    {
        public static int min = 1000;
        public static int[] flag;
        public static string[] input;
        public static string[] process;
        public static int[] DupCount;
        public static int N;
        public static void print(string[] tmp)
        {
            for (int i = 0; i < tmp.GetLength(0); ++i)
            {
                Console.WriteLine(tmp[i]);
            }
            Console.WriteLine();
        }
        public static int duplicate(string tmp1, string tmp2)
        {
            int count = 0;
            for (int i = 0; i < tmp1.Length; ++i)
                if (tmp2.Contains(tmp1[i]) == true)
                    count++;
            return count;
        }
        public static void BackTrack(int index)
        {
            if (index == N)
            {
                if (DupCount[index - 1] < min)
                    min = DupCount[index - 1];
            }
            else
            {
                for (int i = 0; i < N; ++i)
                {

                    if (flag[i] == 0)
                    {
                        int tmp = DupCount[index - 1] + duplicate(process[index - 1], input[i]);
                        if (tmp < min)
                        {
                            process[index] = input[i];
                            DupCount[index] = tmp;
                            flag[i] = 1;
                            BackTrack(index + 1);
                            flag[i] = 0;
                        }
                    }

                }
            }
        }
        static void Main(string[] args)
        {
            N = int.Parse(Console.ReadLine());
            flag = new int[N];
            input = new string[N];
            process = new string[N];
            DupCount = new int[N];
            for (int i = 0; i < N; ++i)
            {
                input[i] = Console.ReadLine();
            }
            for (int i = 0; i < N; ++i)
            {
                flag[i] = 0;
            }
            DupCount[0] = 0;
            for (int i = 0; i < N; ++i)
            {
                process[0] = input[i];
                flag[i] = 1;
                BackTrack(1);
                flag[i] = 0;
            }

            Console.WriteLine(min.ToString());
            Console.ReadKey();
        }
    }
}
