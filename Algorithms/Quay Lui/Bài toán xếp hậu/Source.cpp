﻿#include<iostream>
#include<vector>
using namespace std;

//
//vector<int> x(n);//x[i] = j <=> con hậu thứ i đặt ở hàng i cột j
//vector<int> a(n, 1);//a[i] = true <=> cột i chưa có con hậu nào
//vector<int>	b(2 * n - 1, 1); //b[i+j] == true <=> đường chéo DB-TN chưa có con hậu nào
//vector<int> c(2 * n - 1, 1);//c[i - j + n - 1] == true <=> đường chéo TB-DN chưa có con hậu nào
int n;
vector<int> x;
vector<int> a;
vector<int> b;
vector<int> c;
void show(vector<int> a)
{
	for (int i = 0; i < a.size(); ++i)
	{
		cout << "(" << i << ", " << a[i] << ")  ";
	}
	cout << endl;
}

void XepHau(int i)
{
	for (int k = 0; k < n; ++k)
	{
		if (a[k] == 1 && b[i + k] == 1 && c[i - k + n - 1] == 1)
		{
			x[i] = k;
			if (i == x.size() - 1)
				show(x);
			else
			{
				a[k] = b[i + k] = c[i - k + n - 1] = 0;
				XepHau(i + 1);
				a[k] = b[i + k] = c[i - k + n - 1] = 1;
			}
		}
	}
}
int main()
{
	cin >> n;
	x.resize(n);
	a.resize(n, 1);
	b.resize(2 * n - 1, 1);
	c.resize(2 * n - 1, 1);
	XepHau(0);
	system("pause");
	return 0;
}