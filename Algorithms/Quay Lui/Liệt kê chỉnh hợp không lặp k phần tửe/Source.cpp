#include <iostream>
#include <vector>
using namespace std;

void show(vector<int> a)
{
	for (int i : a)
		cout << i << " ";
	cout << endl;
}
void QuayLui(vector<int> a, vector<int> flag, int index = 0)
{
	//flag[index] = 0;
	for (int i = 1; i < flag.size(); ++i)
	{
		if (flag[i] == 1 ){
			a[index] = i;
			flag[i] = 0;
			if (index == a.size() - 1)
			{
				show(a);
				return;
			}
			else
			{
				QuayLui(a, flag, index + 1);
				flag[i] = 1;
			}
		}
	}
}
int main()
{
	int n, k;
	cin >> n >> k;
	vector<int> a(k);
	vector<int> flag;
	for (int i = 0; i <= n; ++i)
		flag.push_back(1);
	QuayLui(a, flag);
	system("pause");
	return 0;
}