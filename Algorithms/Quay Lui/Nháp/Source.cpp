﻿#include <iostream>
#include <iomanip>

using namespace std;

// Input
int n = 8;
int x, y;

// Output
int BanCo[8][8];

int dx[] = { -2, -1, +1, +2, +2, +1, -1, -2 };
int dy[] = { +1, +2, +2, +1, -1, -2, -2, -1 };

void Xuat()
{
	for (int i = 0; i<n; i++)
	{
		for (int j = 0; j<n; j++)
		{
			cout << setw(3);
			cout << BanCo[i][j] << " ";
		}
		cout << endl;
	}
	cout << endl;
}

// Tim 1 trong 8 o xung quanh cua (x,y) de dat Ma
void MaDiTuan(int x, int y, int buoc)
{
	if (buoc > n*n)
		Xuat();
	else
	{
		// Xet 8 o xung quanh
		for (int k = 0; k<8; k++)
		{
			int xx = x + dx[k];
			int yy = y + dy[k];

			// Nằm trong bang nxn11111111111111111111
			if (xx >= 0 && yy >= 0 && xx<n && yy<n)
				if (BanCo[xx][yy] == 0) // Chua có Mã
				{
					BanCo[xx][yy] = buoc;
					//Xuat();
					MaDiTuan(xx, yy, buoc + 1);
					BanCo[xx][yy] = 0;
				}
		}
	}
}
#include <stdio.h>
#include <conio.h>
#pragma warning(disable:4996)
long long dem = 0;
void TRY(int d, int k, int n)
{
	if (k == n) { dem++; return; }
	for (int t = 0; t <= 3; t++)
		if (d == 3 && t == 3) continue;
		else if (d == 2 && t == 2) TRY(d + 1, k + 1, n);
		else if (d == 1 && t == 1) TRY(d + 1, k + 1, n);
		else if (t == 0) TRY(1, k + 1, n);
		else TRY(0, k + 1, n);
}
int main()
{
	int n;
	scanf("%d", &n);
	TRY(0, 0, n);
	printf("%lld", dem);
	system("pause");
	return 0;
}