#include<iostream>
#include<vector>
using namespace std;


int n;
vector<int> queen;
vector<int> column;
vector<int> diagonal1;
vector<int> diagonal2;
void show(vector<int> a)
{
	for (int i = 0; i < a.size(); ++i)
	{
		cout << "(" << i << ", " << a[i] << ")  ";
	}
	cout << endl;
}
void Queen(int i)
{
	if (i == queen.size())
	{
		show(queen);
	}
	else{
		for (int j = 0; j < n; ++j)
		{
			if (column[j] == 1 && diagonal1[j + i] == 1 && diagonal2[i - j + n - 1] == 1){
				queen[i] = j;
				column[j] = diagonal1[j + i] = diagonal2[i - j + n - 1] = 0;
				Queen(i + 1);
				column[j] = diagonal1[j + i] = diagonal2[i - j + n - 1] = 1;
			}

		}
	}
}
int main()
{
	cin >> n;
	queen.resize(n);
	column.resize(n, 1);
	diagonal1.resize(2 * n - 1, 1); //diagonal[i + j] = true => there's no queen on the first diagonal
	diagonal2.resize(2 * n - 1, 1); //diagonal[i - j + n -1] = true => there's no queen on the second diagonal
	Queen(0);
	system("pause");
	return 0;
}