﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lucky_numbers
{
 
    class Program
    {
        public static string n;
        public static int num;
        public static List<int> numOf4;
        public static List<int> numOf7;
        public static List<int> numbers;
        public static String res;
        public static bool check = false;
        public static void LuckyNumber()
        {
            if (numbers[0] == 0 && numbers[1] == 0 && check == false)
            {
                if (int.Parse(res) >= num)
                {
                    check = true;
                    Console.WriteLine(res);
                    
                }
            }
            else if (check == false)
            {
                for (int i = 0; i <= 1; i++)
                {
                    if (numbers[i] > 0 && i == 0)
                    {
                        res = res + "4";
                        numbers[i]--;
                        LuckyNumber();
                        numbers[i]++;
                        res = res.Remove(res.Length - 1, 1);
                    }
                    else if (numbers[i] > 0 && i == 1)
                    {
                        res = res + "7";
                        numbers[i]--;
                        LuckyNumber();
                        numbers[i]++;
                        res = res.Remove(res.Length - 1, 1);
                    }

                }
            }
        }
        static void Main(string[] args)
        {

            n = Console.ReadLine();
            num = int.Parse(n);
            numbers = new List<int>(2);
            int turn;
            for (turn = 1; turn * 2 <= n.Length + 2; turn++)
            {
                numbers.Clear();
                numbers.Add(turn);
                numbers.Add(turn);
                res = "";
                LuckyNumber();
            }

 
            Console.ReadKey();

        }
    }
}
