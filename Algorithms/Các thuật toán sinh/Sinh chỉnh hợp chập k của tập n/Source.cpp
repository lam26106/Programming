#include <iostream>
using namespace std;

void showString(int* t, int size)
{
	for (int i = 0; i < size; ++i)
		cout << t[i] << " ";
	cout << endl;
}
bool check(int* a, int size, int n)
{
	int count = 0;
	for (int i = 0; i < size; ++i)
		if (a[i] == n - 1)
			count++;
	return count == size;
}

int main()
{
	int n, k;
	cin >> n >> k;
	int *a = new int[k];
	for (int i = 0; i < k; ++i)
		a[i] = 0;
	showString(a, k);
	while (!check(a, k, n))
	{
		for (int i = k - 1; i >= 0; --i)
		{
			if (a[i] != n - 1)
			{
				a[i]++;
				for (int j = i + 1; j < k; ++j)
					a[j] = 0;
				break;
			}
		}
		showString(a, k);
	}
	system("pause");
	return 0;
}