#include <iostream>
using namespace std;

void showString(int* t, int size)
{
	for (int i = 0; i < size; ++i)
		cout << t[i] << " ";
	cout << endl;
}

void my_swap(int&a, int&b)
{
	int temp = a;
	a = b;
	b = temp;
}
int main()
{
	int n;
	cin >> n;
	int*a = new int[n];
	for (int i = 0; i < n; ++i)
		a[i] = i + 1;
	while (true)
	{
		showString(a, n);
		int i = n - 1;
		while (i > 0 && a[i - 1] > a[i]) i--;
		if (i > 0)
		{
			for (int j = n - 1; j >= i; --j)
			{
				if (a[j] > a[i - 1])
				{
					my_swap(a[j], a[i - 1]);
					break;
				}
			}

			for (int j = i, k = n - 1; j < k; ++j, --k)
			{
				my_swap(a[j], a[k]);
			}
		}
		else
			break;
	}
	system("pause");
	return 0;
}