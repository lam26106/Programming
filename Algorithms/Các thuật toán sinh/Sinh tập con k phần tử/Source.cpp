﻿#include <iostream>
using namespace std;


//Cho tập S gồm n phần tử: 1,2,3....n
//Tìm các tập con k phần tử của tập S
//Cận trên của từng phần tử trong tập con x với k phần tử sẽ là: x[i] = n - k + i + 1(i là chỉ số của mảng)
//Cận dưới của từng phần tử trong tập con x với k phần tử sẽ là: x[i] = x[i-1] + 1
//Cấu hình ban đầu: 123...k
//Cấu hình cuối cùng: n - k + i + 1, n - k + i + 2,...., n
void showString(int* t, int size)
{
	for (int i = 0; i < size; ++i)
		cout << t[i] << " ";
	cout << endl;
}
int main()
{
	int n , k;
	cin >> n >> k;
	int *a = new int[k];
	for (int i = 1; i <= k; ++i)
		a[i - 1] = i - 1;
	while (true){
		showString(a, k);
		int i = k - 1;
		while (i >= 0 && a[i] == n - k + i ) i--;
		if (i >= 0)
		{
			a[i]++;
			for (int j = i + 1; j < k; ++j)
				a[j] = a[j - 1] + 1;
		}
		else
			break;
	}
	system("pause");
	return 0;
}