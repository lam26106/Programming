#include <iostream>
using namespace std;

void my_swap(int *c, int *d)
{
	int *temp = c;
	c = d;
	d = c;
}
int main()
{
	int a = 3, b = 2;
	my_swap(&a, &b);
	cout << a << " " << b;
	system("pause");
	return 0;
}