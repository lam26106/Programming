﻿#include <iostream>
#include<string>
#include<vector>
using namespace std;


//Nhập vào 1 danh sách n tên người. Liệt kê tất cả các cách chọn ra đúng k người trong số n người đó
void bai1()
{

	int n, k;
	cin >> n >> k;
	vector<string> arr;
	for (int i = 1; i <= n; ++i)
	{
		string temp;
		cin >> temp;
		arr.push_back(temp);
	}
	int*a = new int[k];
	for (int i = 0; i < k; ++i)
		a[i] = i;
	while (true)
	{
		for (int i = 0; i < k; ++i)
			cout << arr[a[i]] << " ";
		cout << endl;
		int i = k - 1;
		while (i >= 0 && a[i] == n - k + i) i--;
		if (i >= 0)
		{
			a[i]++;
			for (int j = i + 1; j < k; ++j)
				a[j] = a[j - 1] + 1;
		}
		else
		{
			for (int i = 0; i < k; ++i)
				cout << arr[a[i]] << " ";
			break;
		}
	}
}
//Liệt kê các tập con của một tập hợp {0, 1,2,3,....,n - 1}. Có thể dùng phương pháp sinh nhị phân nhưng ở đây sẽ làm theo phương pháp
//dụa trên bài sinh tập con k phần tử
void bai2()
{
	int n;
	cin >> n;
	int *a;
	for (int k = 1; k <= n; ++k)
	{
		a = new int[k];
		for (int i = 0; i < k; ++i)
			a[i] = i;
		while (true)
		{
			for (int i = 0; i < k; ++i)
				cout << a[i] << " ";
			cout << endl;
			int i = k - 1;
			while (i >= 0 && a[i] == n - k + i) i--;
			if (i >= 0)
			{
				a[i]++;
				for (int j = i + 1; j < k; ++j)
					a[j] = a[j - 1] + 1;
			}
			else
			{
				delete[] a;
				break;
			}
		}
	}
}
int main()
{
	bai2();
	system("pause");
	return 0;
}