#include <iostream>
using namespace std;

void showString(string t)
{
	for (char c : t)
		cout << c << " ";
	cout << endl;
}
bool check(string t)
{
	int count = 0;
	for (char c : t)
		if (c == '1')
			count++;
	return count == t.length();
}
void nextString(string &t)
{
	for (int i = t.length() - 1; i >= 0; --i)
	{
		if (t[i] == '0')
		{
			t[i] = '1';
			for (int j = i + 1; j < t.length(); ++j)
				t[j] = '0';
			break;
		}
	}
}
int main()
{
	int n;
	cin >> n;
	string t;
	for (int i = 0; i < n; ++i)
		t += '0';
	showString(t);
	while (!check(t))
	{
		nextString(t);
		showString(t);
	}
	system("pause");
	return 0;
}